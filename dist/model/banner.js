"use strict";
module.exports = function (sequelize, dataTypes) {
    const model = sequelize.define("banner", {
        id: {
            type: dataTypes.STRING,
            primaryKey: true,
            defaultValue: dataTypes.UUIDV4,
            comment: "bannerID",
            field: "id",
        },
        title: {
            type: dataTypes.STRING,
            field: "title",
            comment: "banner标题"
        },
        type: {
            type: dataTypes.INTEGER,
            field: "type",
            comment: "banner分类"
        },
        author: {
            type: dataTypes.STRING,
            field: "author",
            comment: "作者"
        },
        imageUrl: {
            type: dataTypes.STRING,
            field: "image_url",
            comment: "图片路径"
        },
        skipLink: {
            type: dataTypes.STRING,
            field: "skip_link",
            comment: "banner跳转链接"
        },
        htmlPageNumber: {
            type: dataTypes.STRING,
            field: "html_page_number",
            comment: "用户名称"
        },
        createdAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "created_at",
            comment: "创建时间"
        },
        updateAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "updated_at",
            comment: "更新时间时间"
        },
        isDeleted: {
            type: dataTypes.BOOLEAN,
            defaultValue: 0,
            field: "is_deleted",
            comment: "是否删除"
        }
    }, {
        timestamps: false,
        tableName: "banner"
    });
    return model;
};

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGVsL2Jhbm5lci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsTUFBTSxDQUFDLE9BQU8sR0FBRyxVQUFVLFNBQWMsRUFBRSxTQUFjO0lBQ3JELE1BQU0sS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO1FBQ3JDLEVBQUUsRUFBRTtZQUNBLElBQUksRUFBRSxTQUFTLENBQUMsTUFBTTtZQUN0QixVQUFVLEVBQUUsSUFBSTtZQUNoQixZQUFZLEVBQUUsU0FBUyxDQUFDLE1BQU07WUFDOUIsT0FBTyxFQUFFLFVBQVU7WUFDbkIsS0FBSyxFQUFFLElBQUk7U0FDZDtRQUNELEtBQUssRUFBRTtZQUNILElBQUksRUFBRSxTQUFTLENBQUMsTUFBTTtZQUN0QixLQUFLLEVBQUUsT0FBTztZQUNkLE9BQU8sRUFBRSxVQUFVO1NBQ3RCO1FBQ0QsSUFBSSxFQUFFO1lBQ0YsSUFBSSxFQUFFLFNBQVMsQ0FBQyxPQUFPO1lBQ3ZCLEtBQUssRUFBRSxNQUFNO1lBQ2IsT0FBTyxFQUFFLFVBQVU7U0FDdEI7UUFDRCxNQUFNLEVBQUU7WUFDSixJQUFJLEVBQUUsU0FBUyxDQUFDLE1BQU07WUFDdEIsS0FBSyxFQUFFLFFBQVE7WUFDZixPQUFPLEVBQUUsSUFBSTtTQUNoQjtRQUNELFFBQVEsRUFBRTtZQUNOLElBQUksRUFBRSxTQUFTLENBQUMsTUFBTTtZQUN0QixLQUFLLEVBQUUsV0FBVztZQUNsQixPQUFPLEVBQUUsTUFBTTtTQUNsQjtRQUNELFFBQVEsRUFBRTtZQUNOLElBQUksRUFBRSxTQUFTLENBQUMsTUFBTTtZQUN0QixLQUFLLEVBQUUsV0FBVztZQUNsQixPQUFPLEVBQUUsWUFBWTtTQUN4QjtRQUNELGNBQWMsRUFBRTtZQUNaLElBQUksRUFBRSxTQUFTLENBQUMsTUFBTTtZQUN0QixLQUFLLEVBQUUsa0JBQWtCO1lBQ3pCLE9BQU8sRUFBRSxNQUFNO1NBQ2xCO1FBQ0QsU0FBUyxFQUFFO1lBQ1AsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFJO1lBQ3BCLFlBQVksRUFBRSxJQUFJLElBQUksRUFBRTtZQUN4QixLQUFLLEVBQUUsWUFBWTtZQUNuQixPQUFPLEVBQUUsTUFBTTtTQUNsQjtRQUNELFFBQVEsRUFBRTtZQUNOLElBQUksRUFBRSxTQUFTLENBQUMsSUFBSTtZQUNwQixZQUFZLEVBQUUsSUFBSSxJQUFJLEVBQUU7WUFDeEIsS0FBSyxFQUFFLFlBQVk7WUFDbkIsT0FBTyxFQUFFLFFBQVE7U0FDcEI7UUFDRCxTQUFTLEVBQUU7WUFDUCxJQUFJLEVBQUUsU0FBUyxDQUFDLE9BQU87WUFDdkIsWUFBWSxFQUFFLENBQUM7WUFDZixLQUFLLEVBQUUsWUFBWTtZQUNuQixPQUFPLEVBQUUsTUFBTTtTQUNsQjtLQUNKLEVBQUU7UUFDSyxVQUFVLEVBQUUsS0FBSztRQUNqQixTQUFTLEVBQUUsUUFBUTtLQUN0QixDQUFDLENBQUM7SUFDUCxPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDLENBQUMiLCJmaWxlIjoibW9kZWwvYmFubmVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoc2VxdWVsaXplOiBhbnksIGRhdGFUeXBlczogYW55KSB7XG4gICAgY29uc3QgbW9kZWwgPSBzZXF1ZWxpemUuZGVmaW5lKFwiYmFubmVyXCIsIHtcbiAgICAgICAgaWQ6IHtcbiAgICAgICAgICAgIHR5cGU6IGRhdGFUeXBlcy5TVFJJTkcsXG4gICAgICAgICAgICBwcmltYXJ5S2V5OiB0cnVlLFxuICAgICAgICAgICAgZGVmYXVsdFZhbHVlOiBkYXRhVHlwZXMuVVVJRFY0LFxuICAgICAgICAgICAgY29tbWVudDogXCJiYW5uZXJJRFwiLFxuICAgICAgICAgICAgZmllbGQ6IFwiaWRcIixcbiAgICAgICAgfSxcbiAgICAgICAgdGl0bGU6IHtcbiAgICAgICAgICAgIHR5cGU6IGRhdGFUeXBlcy5TVFJJTkcsXG4gICAgICAgICAgICBmaWVsZDogXCJ0aXRsZVwiLFxuICAgICAgICAgICAgY29tbWVudDogXCJiYW5uZXLmoIfpophcIlxuICAgICAgICB9LFxuICAgICAgICB0eXBlOiB7XG4gICAgICAgICAgICB0eXBlOiBkYXRhVHlwZXMuSU5URUdFUixcbiAgICAgICAgICAgIGZpZWxkOiBcInR5cGVcIixcbiAgICAgICAgICAgIGNvbW1lbnQ6IFwiYmFubmVy5YiG57G7XCJcbiAgICAgICAgfSxcbiAgICAgICAgYXV0aG9yOiB7XG4gICAgICAgICAgICB0eXBlOiBkYXRhVHlwZXMuU1RSSU5HLFxuICAgICAgICAgICAgZmllbGQ6IFwiYXV0aG9yXCIsXG4gICAgICAgICAgICBjb21tZW50OiBcIuS9nOiAhVwiXG4gICAgICAgIH0sXG4gICAgICAgIGltYWdlVXJsOiB7XG4gICAgICAgICAgICB0eXBlOiBkYXRhVHlwZXMuU1RSSU5HLFxuICAgICAgICAgICAgZmllbGQ6IFwiaW1hZ2VfdXJsXCIsXG4gICAgICAgICAgICBjb21tZW50OiBcIuWbvueJh+i3r+W+hFwiXG4gICAgICAgIH0sXG4gICAgICAgIHNraXBMaW5rOiB7XG4gICAgICAgICAgICB0eXBlOiBkYXRhVHlwZXMuU1RSSU5HLFxuICAgICAgICAgICAgZmllbGQ6IFwic2tpcF9saW5rXCIsXG4gICAgICAgICAgICBjb21tZW50OiBcImJhbm5lcui3s+i9rOmTvuaOpVwiXG4gICAgICAgIH0sXG4gICAgICAgIGh0bWxQYWdlTnVtYmVyOiB7XG4gICAgICAgICAgICB0eXBlOiBkYXRhVHlwZXMuU1RSSU5HLFxuICAgICAgICAgICAgZmllbGQ6IFwiaHRtbF9wYWdlX251bWJlclwiLFxuICAgICAgICAgICAgY29tbWVudDogXCLnlKjmiLflkI3np7BcIlxuICAgICAgICB9LFxuICAgICAgICBjcmVhdGVkQXQ6IHtcbiAgICAgICAgICAgIHR5cGU6IGRhdGFUeXBlcy5EQVRFLFxuICAgICAgICAgICAgZGVmYXVsdFZhbHVlOiBuZXcgRGF0ZSgpLFxuICAgICAgICAgICAgZmllbGQ6IFwiY3JlYXRlZF9hdFwiLFxuICAgICAgICAgICAgY29tbWVudDogXCLliJvlu7rml7bpl7RcIlxuICAgICAgICB9LFxuICAgICAgICB1cGRhdGVBdDoge1xuICAgICAgICAgICAgdHlwZTogZGF0YVR5cGVzLkRBVEUsXG4gICAgICAgICAgICBkZWZhdWx0VmFsdWU6IG5ldyBEYXRlKCksXG4gICAgICAgICAgICBmaWVsZDogXCJ1cGRhdGVkX2F0XCIsXG4gICAgICAgICAgICBjb21tZW50OiBcIuabtOaWsOaXtumXtOaXtumXtFwiXG4gICAgICAgIH0sXG4gICAgICAgIGlzRGVsZXRlZDoge1xuICAgICAgICAgICAgdHlwZTogZGF0YVR5cGVzLkJPT0xFQU4sXG4gICAgICAgICAgICBkZWZhdWx0VmFsdWU6IDAsXG4gICAgICAgICAgICBmaWVsZDogXCJpc19kZWxldGVkXCIsXG4gICAgICAgICAgICBjb21tZW50OiBcIuaYr+WQpuWIoOmZpFwiXG4gICAgICAgIH1cbiAgICB9LCB7XG4gICAgICAgICAgICB0aW1lc3RhbXBzOiBmYWxzZSxcbiAgICAgICAgICAgIHRhYmxlTmFtZTogXCJiYW5uZXJcIlxuICAgICAgICB9KTtcbiAgICByZXR1cm4gbW9kZWw7XG59OyJdfQ==
