"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const base_response_1 = require("../../base/base-response");
const userservice_1 = require("../user/userservice");
class UserController {
    static login(request, response, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const username = request.body["username"];
            const password = request.body["password"];
            console.log("username:" + username);
            console.log("password:" + password);
            try {
                const userService = new userservice_1.UserService();
                const user = yield userService.findOneByUserName(username);
                console.log("user", user);
                if (username === "tongtang" && password === "13764078944") {
                    response.json(new base_response_1.ResponseSuccess({}));
                }
            }
            catch (err) {
                response.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
}
exports.UserController = UserController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVudGl0eS91c2VyL3VzZXJjb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUNBLDREQUFzRztBQUN0RyxxREFBa0Q7QUFDbEQsTUFBYSxjQUFjO0lBQ2hCLE1BQU0sQ0FBTyxLQUFLLENBQUMsT0FBWSxFQUFFLFFBQTBCLEVBQUUsSUFBMEI7O1lBQzFGLE1BQU0sUUFBUSxHQUFXLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDbEQsTUFBTSxRQUFRLEdBQVcsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsQ0FBQztZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsQ0FBQztZQUNwQyxJQUFJO2dCQUNBLE1BQU0sV0FBVyxHQUFRLElBQUkseUJBQVcsRUFBRSxDQUFDO2dCQUMzQyxNQUFNLElBQUksR0FBUyxNQUFNLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDakUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQzFCLElBQUksUUFBUSxLQUFLLFVBQVUsSUFBSSxRQUFRLEtBQUssYUFBYSxFQUFFO29CQUN2RCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUMxQzthQUNKO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNyRCxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7Q0FFSjtBQW5CRCx3Q0FtQkMiLCJmaWxlIjoiZW50aXR5L3VzZXIvdXNlcmNvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gXCJleHByZXNzXCI7XG5pbXBvcnQgeyBSZXNwb25zZVN1Y2Nlc3MsIFJlc3BvbnNlUGFyYW1zRXJyb3IsIFJlc3BvbnNlU2VydmljZUVycm9yIH0gZnJvbSBcIi4uLy4uL2Jhc2UvYmFzZS1yZXNwb25zZVwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vdXNlci91c2Vyc2VydmljZVwiO1xuZXhwb3J0IGNsYXNzIFVzZXJDb250cm9sbGVyIHtcbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGxvZ2luKHJlcXVlc3Q6IGFueSwgcmVzcG9uc2U6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IHVzZXJuYW1lOiBzdHJpbmcgPSByZXF1ZXN0LmJvZHlbXCJ1c2VybmFtZVwiXTtcbiAgICAgICAgY29uc3QgcGFzc3dvcmQ6IHN0cmluZyA9IHJlcXVlc3QuYm9keVtcInBhc3N3b3JkXCJdO1xuICAgICAgICBjb25zb2xlLmxvZyhcInVzZXJuYW1lOlwiICsgdXNlcm5hbWUpO1xuICAgICAgICBjb25zb2xlLmxvZyhcInBhc3N3b3JkOlwiICsgcGFzc3dvcmQpO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgdXNlclNlcnZpY2U6IGFueSA9IG5ldyBVc2VyU2VydmljZSgpO1xuICAgICAgICAgICAgY29uc3QgdXNlcjogYW55ID0gIGF3YWl0IHVzZXJTZXJ2aWNlLmZpbmRPbmVCeVVzZXJOYW1lKHVzZXJuYW1lKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidXNlclwiLCB1c2VyKTtcbiAgICAgICAgICAgIGlmICh1c2VybmFtZSA9PT0gXCJ0b25ndGFuZ1wiICYmIHBhc3N3b3JkID09PSBcIjEzNzY0MDc4OTQ0XCIpIHtcbiAgICAgICAgICAgICAgICByZXNwb25zZS5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3Moe30pKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXNwb25zZS5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIubWVzc2FnZSkpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG59Il19
