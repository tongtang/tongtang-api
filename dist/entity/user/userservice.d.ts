export declare class UserService {
    private data;
    private model;
    constructor(obj?: any);
    findOne(id: string): any;
    findOneByUserName(username: string): Promise<any>;
}
