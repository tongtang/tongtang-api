"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const sequelize = require("sequelize");
let _ = require("lodash");
class UserService {
    constructor(obj = null) {
        if (obj)
            this.data = _.clone(obj);
        this.model = sequelize.models.user;
    }
    findOne(id) {
        return this.model.findOne({
            attributes: ["username", "password", ["create_time", "createTime"]],
            where: { id: id }
        });
    }
    findOneByUserName(username) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("this", this.model);
            return yield this.model.findOne({
                attributes: ["username", "password", ["create_time", "createTime"]],
                where: { id: username }
            });
        });
    }
}
exports.UserService = UserService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVudGl0eS91c2VyL3VzZXJzZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUN2QyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7QUFFMUIsTUFBYSxXQUFXO0lBSXRCLFlBQVksTUFBVyxJQUFJO1FBQ3pCLElBQUksR0FBRztZQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ3JDLENBQUM7SUFFRCxPQUFPLENBQUMsRUFBVTtRQUNoQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBQ3hCLFVBQVUsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDbkUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRTtTQUNsQixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUssaUJBQWlCLENBQUMsUUFBZ0I7O1lBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxPQUFPLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7Z0JBQzlCLFVBQVUsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ25FLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUU7YUFDeEIsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztLQUFBO0NBQ0Y7QUF2QkQsa0NBdUJDIiwiZmlsZSI6ImVudGl0eS91c2VyL3VzZXJzZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3Qgc2VxdWVsaXplID0gcmVxdWlyZShcInNlcXVlbGl6ZVwiKTtcbmxldCBfID0gcmVxdWlyZShcImxvZGFzaFwiKTtcblxuZXhwb3J0IGNsYXNzIFVzZXJTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBkYXRhOiBhbnk7XG4gIHByaXZhdGUgbW9kZWw6IGFueTtcblxuICBjb25zdHJ1Y3RvcihvYmo6IGFueSA9IG51bGwpIHtcbiAgICBpZiAob2JqKSB0aGlzLmRhdGEgPSBfLmNsb25lKG9iaik7XG4gICAgdGhpcy5tb2RlbCA9IHNlcXVlbGl6ZS5tb2RlbHMudXNlcjtcbiAgfVxuXG4gIGZpbmRPbmUoaWQ6IHN0cmluZykge1xuICAgIHJldHVybiB0aGlzLm1vZGVsLmZpbmRPbmUoe1xuICAgICAgYXR0cmlidXRlczogW1widXNlcm5hbWVcIiwgXCJwYXNzd29yZFwiLCBbXCJjcmVhdGVfdGltZVwiLCBcImNyZWF0ZVRpbWVcIl1dLFxuICAgICAgd2hlcmU6IHsgaWQ6IGlkIH1cbiAgICB9KTtcbiAgfVxuXG4gIGFzeW5jIGZpbmRPbmVCeVVzZXJOYW1lKHVzZXJuYW1lOiBzdHJpbmcpIHtcbiAgICBjb25zb2xlLmxvZyhcInRoaXNcIiwgdGhpcy5tb2RlbCk7XG4gICAgcmV0dXJuIGF3YWl0IHRoaXMubW9kZWwuZmluZE9uZSh7XG4gICAgICBhdHRyaWJ1dGVzOiBbXCJ1c2VybmFtZVwiLCBcInBhc3N3b3JkXCIsIFtcImNyZWF0ZV90aW1lXCIsIFwiY3JlYXRlVGltZVwiXV0sXG4gICAgICB3aGVyZTogeyBpZDogdXNlcm5hbWUgfVxuICAgIH0pO1xuICB9XG59Il19
