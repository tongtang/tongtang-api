import * as express from "express";
export declare class OssController {
    static getOssToken(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
}
