"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlTypeController = void 0;
const htmltypeservice_1 = require("../service/htmltypeservice");
const xlsx = require("node-xlsx");
const base_response_1 = require("../base/base-response");
class HtmlTypeController {
    static findAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltypes = yield htmlTypeService.findAll();
                res.json(new base_response_1.ResponseSuccess(htmltypes, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static findPCAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const isOpen = Number(req.query["isOpen"]);
                const pageIndex = req.query["pageIndex"] || 1;
                const pageSize = req.query["pageSize"] || 10;
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltypes = yield htmlTypeService.findPCAll(isOpen, pageIndex, pageSize);
                res.json(new base_response_1.ResponseSuccess(htmltypes, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static findOneById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltype = yield htmlTypeService.findOneById(id);
                res.json(new base_response_1.ResponseSuccess(htmltype, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const code = req.body["code"];
            const name = req.body["name"];
            const weight = req.body["weight"];
            const picUrl = req.body["picUrl"];
            const picUrlNew = req.body["picUrlNew"];
            try {
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltype = yield htmlTypeService.create(code, name, weight, picUrl, picUrlNew);
                res.json(new base_response_1.ResponseSuccess({}, "CREATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static updateById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.body["id"];
            const code = req.body["code"];
            const name = req.body["name"];
            const weight = req.body["weight"];
            const picUrl = req.body["picUrl"];
            const picUrlNew = req.body["picUrlNew"];
            try {
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltype = yield htmlTypeService.updateById(id, code, name, weight, picUrl, picUrlNew);
                res.json(new base_response_1.ResponseSuccess(htmltype[0], "UPDATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static isOpen(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.body.id;
            const isOpen = req.body.isOpen;
            try {
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltype = yield htmlTypeService.isOpen(id, isOpen);
                res.json(new base_response_1.ResponseSuccess(htmltype[0], "ISOPEN_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static deleteById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltype = yield htmlTypeService.deleteById(id);
                res.json(new base_response_1.ResponseSuccess(htmltype, "DELETE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static cleanDivision(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const path = req.file.path;
                const filename = req.file.originalname;
                if (filename.substring(filename.length - 5, filename.length) !== ".xlsx") {
                }
                const list = xlsx.parse(path);
                const districtsData = list[1].data;
                const htmlTypeService = new htmltypeservice_1.HtmlTypeService();
                const htmltype = yield htmlTypeService.cleanDivision(districtsData);
                res.json(new base_response_1.ResponseSuccess({}, "CLEAN_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
}
exports.HtmlTypeController = HtmlTypeController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXIvaHRtbHR5cGVjb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUlBLGdFQUE2RDtBQUM3RCxNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDbEMseURBQW1HO0FBQ25HLE1BQWEsa0JBQWtCO0lBQ3BCLE1BQU0sQ0FBTyxPQUFPLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ25GLElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sU0FBUyxHQUFRLE1BQU0sZUFBZSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN2RCxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQzthQUM1RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLFNBQVMsQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDckYsSUFBSTtnQkFDQSxNQUFNLE1BQU0sR0FBVyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNuRCxNQUFNLFNBQVMsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEQsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3JELE1BQU0sZUFBZSxHQUFRLElBQUksaUNBQWUsRUFBRSxDQUFDO2dCQUNuRCxNQUFNLFNBQVMsR0FBUSxNQUFNLGVBQWUsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDcEYsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7YUFDNUQ7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBTyxXQUFXLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ3ZGLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBQ2hDLElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sUUFBUSxHQUFRLE1BQU0sZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDNUQsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hELE9BQU87YUFDVjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sTUFBTSxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUNsRixNQUFNLElBQUksR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sSUFBSSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEMsTUFBTSxNQUFNLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxQyxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEQsSUFBSTtnQkFDQSxNQUFNLGVBQWUsR0FBUSxJQUFJLGlDQUFlLEVBQUUsQ0FBQztnQkFDbkQsTUFBTSxRQUFRLEdBQVEsTUFBTSxlQUFlLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDMUYsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsRUFBRSxFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDcEQsT0FBTzthQUNWO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBTyxVQUFVLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ3RGLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEMsTUFBTSxJQUFJLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QyxNQUFNLElBQUksR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sTUFBTSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDMUMsTUFBTSxNQUFNLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxQyxNQUFNLFNBQVMsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2hELElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sUUFBUSxHQUFRLE1BQU0sZUFBZSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUNsRyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUM3RCxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLE1BQU0sQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDbEYsTUFBTSxFQUFFLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7WUFDL0IsTUFBTSxNQUFNLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDdkMsSUFBSTtnQkFDQSxNQUFNLGVBQWUsR0FBUSxJQUFJLGlDQUFlLEVBQUUsQ0FBQztnQkFDbkQsTUFBTSxRQUFRLEdBQVEsTUFBTSxlQUFlLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDL0QsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDN0QsT0FBTzthQUNWO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUdNLE1BQU0sQ0FBTyxVQUFVLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ3RGLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBQ2hDLElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sUUFBUSxHQUFRLE1BQU0sZUFBZSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDM0QsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsUUFBUSxFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDMUQsT0FBTzthQUNWO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVELE1BQU0sQ0FBTyxhQUFhLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ2xGLElBQUk7Z0JBQ0YsTUFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzNCLE1BQU0sUUFBUSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO2dCQUN2QyxJQUFJLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLE9BQU8sRUFBRTtpQkFDekU7Z0JBQ0QsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDOUIsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbkMsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sUUFBUSxHQUFRLE1BQU0sZUFBZSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDekUsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsRUFBRSxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUM7YUFDcEQ7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0gsQ0FBQztLQUFBO0NBQ047QUF0SEQsZ0RBc0hDIiwiZmlsZSI6ImNvbnRyb2xsZXIvaHRtbHR5cGVjb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgZXhwcmVzcyBmcm9tIFwiZXhwcmVzc1wiO1xuaW1wb3J0IHsgVmFsaWRhdG9yIH0gZnJvbSBcInZhbGlkYXRvci10c1wiO1xuaW1wb3J0IHsgUmVxdWVzdFBhcmFtcyB9IGZyb20gXCIuLi90eXBlcy9pbnRlcmZhY2VzXCI7XG5pbXBvcnQgeyBSZXF1ZXN0VXRpbHMgfSBmcm9tIFwiLi4vdXRpbC9yZXF1ZXN0LXV0aWxzXCI7XG5pbXBvcnQgeyBIdG1sVHlwZVNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS9odG1sdHlwZXNlcnZpY2VcIjtcbmNvbnN0IHhsc3ggPSByZXF1aXJlKFwibm9kZS14bHN4XCIpO1xuaW1wb3J0IHsgUmVzcG9uc2VTZXJ2aWNlRXJyb3IsIFJlc3BvbnNlUGFyYW1zRXJyb3IsIFJlc3BvbnNlU3VjY2VzcyB9IGZyb20gXCIuLi9iYXNlL2Jhc2UtcmVzcG9uc2VcIjtcbmV4cG9ydCBjbGFzcyBIdG1sVHlwZUNvbnRyb2xsZXIge1xuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgZmluZEFsbChyZXE6IGFueSwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbik6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgaHRtbFR5cGVTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFR5cGVTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1sdHlwZXM6IGFueSA9IGF3YWl0IGh0bWxUeXBlU2VydmljZS5maW5kQWxsKCk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWx0eXBlcywgXCJGSU5EX1NVQ0NFU1NcIikpO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIubWVzc2FnZSkpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBmaW5kUENBbGwocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IGlzT3BlbjogbnVtYmVyID0gTnVtYmVyKHJlcS5xdWVyeVtcImlzT3BlblwiXSk7XG4gICAgICAgICAgICBjb25zdCBwYWdlSW5kZXg6IG51bWJlciA9IHJlcS5xdWVyeVtcInBhZ2VJbmRleFwiXSB8fCAxO1xuICAgICAgICAgICAgY29uc3QgcGFnZVNpemU6IG51bWJlciA9IHJlcS5xdWVyeVtcInBhZ2VTaXplXCJdIHx8IDEwO1xuICAgICAgICAgICAgY29uc3QgaHRtbFR5cGVTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFR5cGVTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1sdHlwZXM6IGFueSA9IGF3YWl0IGh0bWxUeXBlU2VydmljZS5maW5kUENBbGwoaXNPcGVuLCBwYWdlSW5kZXgsIHBhZ2VTaXplKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoaHRtbHR5cGVzLCBcIkZJTkRfU1VDQ0VTU1wiKSk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVyci5tZXNzYWdlKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRPbmVCeUlkKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEucXVlcnkuaWQ7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBodG1sVHlwZVNlcnZpY2U6IGFueSA9IG5ldyBIdG1sVHlwZVNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWx0eXBlOiBhbnkgPSBhd2FpdCBodG1sVHlwZVNlcnZpY2UuZmluZE9uZUJ5SWQoaWQpO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2VzcyhodG1sdHlwZSwgXCJGSU5EX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgY3JlYXRlKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGNvZGU6IHN0cmluZyA9IHJlcS5ib2R5W1wiY29kZVwiXTtcbiAgICAgICAgY29uc3QgbmFtZTogc3RyaW5nID0gcmVxLmJvZHlbXCJuYW1lXCJdO1xuICAgICAgICBjb25zdCB3ZWlnaHQ6IG51bWJlciA9IHJlcS5ib2R5W1wid2VpZ2h0XCJdO1xuICAgICAgICBjb25zdCBwaWNVcmw6IHN0cmluZyA9IHJlcS5ib2R5W1wicGljVXJsXCJdO1xuICAgICAgICBjb25zdCBwaWNVcmxOZXc6IHN0cmluZyA9IHJlcS5ib2R5W1wicGljVXJsTmV3XCJdO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgaHRtbFR5cGVTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFR5cGVTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1sdHlwZTogYW55ID0gYXdhaXQgaHRtbFR5cGVTZXJ2aWNlLmNyZWF0ZShjb2RlLCBuYW1lLCB3ZWlnaHQsIHBpY1VybCwgcGljVXJsTmV3KTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3Moe30sIFwiQ1JFQVRFX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgdXBkYXRlQnlJZChyZXE6IGFueSwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbik6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICBjb25zdCBpZDogc3RyaW5nID0gcmVxLmJvZHlbXCJpZFwiXTtcbiAgICAgICAgY29uc3QgY29kZTogc3RyaW5nID0gcmVxLmJvZHlbXCJjb2RlXCJdO1xuICAgICAgICBjb25zdCBuYW1lOiBzdHJpbmcgPSByZXEuYm9keVtcIm5hbWVcIl07XG4gICAgICAgIGNvbnN0IHdlaWdodDogbnVtYmVyID0gcmVxLmJvZHlbXCJ3ZWlnaHRcIl07XG4gICAgICAgIGNvbnN0IHBpY1VybDogc3RyaW5nID0gcmVxLmJvZHlbXCJwaWNVcmxcIl07XG4gICAgICAgIGNvbnN0IHBpY1VybE5ldzogc3RyaW5nID0gcmVxLmJvZHlbXCJwaWNVcmxOZXdcIl07XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBodG1sVHlwZVNlcnZpY2U6IGFueSA9IG5ldyBIdG1sVHlwZVNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWx0eXBlOiBhbnkgPSBhd2FpdCBodG1sVHlwZVNlcnZpY2UudXBkYXRlQnlJZChpZCwgY29kZSwgbmFtZSwgd2VpZ2h0LCBwaWNVcmwsIHBpY1VybE5ldyk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWx0eXBlWzBdLCBcIlVQREFURV9TVUNDRVNTXCIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGlzT3BlbihyZXE6IGFueSwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbik6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICBjb25zdCBpZDogc3RyaW5nID0gcmVxLmJvZHkuaWQ7XG4gICAgICAgIGNvbnN0IGlzT3BlbjogbnVtYmVyID0gcmVxLmJvZHkuaXNPcGVuO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgaHRtbFR5cGVTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFR5cGVTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1sdHlwZTogYW55ID0gYXdhaXQgaHRtbFR5cGVTZXJ2aWNlLmlzT3BlbihpZCwgaXNPcGVuKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoaHRtbHR5cGVbMF0sIFwiSVNPUEVOX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBkZWxldGVCeUlkKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEucXVlcnkuaWQ7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBodG1sVHlwZVNlcnZpY2U6IGFueSA9IG5ldyBIdG1sVHlwZVNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWx0eXBlOiBhbnkgPSBhd2FpdCBodG1sVHlwZVNlcnZpY2UuZGVsZXRlQnlJZChpZCk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWx0eXBlLCBcIkRFTEVURV9TVUNDRVNTXCIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdGF0aWMgYXN5bmMgY2xlYW5EaXZpc2lvbihyZXE6IGFueSwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbik6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIGNvbnN0IHBhdGggPSByZXEuZmlsZS5wYXRoO1xuICAgICAgICAgIGNvbnN0IGZpbGVuYW1lID0gcmVxLmZpbGUub3JpZ2luYWxuYW1lO1xuICAgICAgICAgIGlmIChmaWxlbmFtZS5zdWJzdHJpbmcoZmlsZW5hbWUubGVuZ3RoIC0gNSwgZmlsZW5hbWUubGVuZ3RoKSAhPT0gXCIueGxzeFwiKSB7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnN0IGxpc3QgPSB4bHN4LnBhcnNlKHBhdGgpO1xuICAgICAgICAgIGNvbnN0IGRpc3RyaWN0c0RhdGEgPSBsaXN0WzFdLmRhdGE7XG4gICAgICAgICAgY29uc3QgaHRtbFR5cGVTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFR5cGVTZXJ2aWNlKCk7XG4gICAgICAgICAgY29uc3QgaHRtbHR5cGU6IGFueSA9IGF3YWl0IGh0bWxUeXBlU2VydmljZS5jbGVhbkRpdmlzaW9uKGRpc3RyaWN0c0RhdGEpO1xuICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3Moe30sIFwiQ0xFQU5fU1VDQ0VTU1wiKSk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICB9XG59Il19
