"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserHealthInformationController = void 0;
const user_health_information_service_1 = require("../service/user-health-information-service");
const base_response_1 = require("../base/base-response");
const nodeExcel = require("excel-export");
class UserHealthInformationController {
    static findOneById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
                const userHealthInformation = yield userHealthInformationService.findOneById(id);
                res.json(new base_response_1.ResponseSuccess(userHealthInformation, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static findOneByOrignId(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const originId = req.query.originId;
            const origin = req.query.origin;
            try {
                const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
                const userHealthInformation = yield userHealthInformationService.findOneByOrignId(originId, origin);
                res.json(new base_response_1.ResponseSuccess(userHealthInformation, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static findAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const pageIndex = req.query["pageIndex"] || 1;
                const pageSize = req.query["pageSize"] || 10;
                const telephone = req.query["telephone"];
                const realName = req.query["realName"];
                const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
                const districts = yield userHealthInformationService.findAll(telephone, realName, pageIndex, pageSize);
                res.json(new base_response_1.ResponseSuccess(districts, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static findPCAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const pageIndex = req.query["pageIndex"] || 1;
                const pageSize = req.query["pageSize"] || 10;
                const telephone = req.query["telephone"];
                const realName = req.query["realName"];
                const startTime = req.query["startTime"];
                const endTime = req.query["endTime"];
                const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
                // 获取总访问
                const districts = yield userHealthInformationService.findPCAll(telephone, realName, startTime, endTime, pageIndex, pageSize);
                res.json(new base_response_1.ResponseSuccess(districts, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const realName = req.body["realName"];
            const sex = req.body["sex"]; // 0: 女  1: 男
            const age = req.body["age"];
            const telephone = req.body["telephone"];
            const idCard = req.body["idCard"];
            const healthReportUrl = req.body["healthReportUrl"];
            const height = req.body["height"];
            const weight = req.body["weight"];
            const waistline = req.body["waistline"];
            const hipline = req.body["hipline"];
            const sbp = req.body["sbp"];
            const dbp = req.body["dbp"];
            const rbg = req.body["rbg"];
            const fpg = req.body["fpg"];
            const hpg = req.body["hpg"];
            const ua = req.body["ua"];
            const tc = req.body["tc"];
            const tg = req.body["tg"];
            const hdlc = req.body["hdlc"];
            const ldlc = req.body["ldlc"];
            const origin = req.body["origin"]; // 1 : 大地
            const originId = req.body["originId"];
            try {
                const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
                const userHealthInformation = yield userHealthInformationService.create(realName, sex, age, telephone, idCard, healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, originId);
                console.log("userHealthInformation", userHealthInformation);
                res.json(new base_response_1.ResponseSuccess({}, "CREATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static updateById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.body["id"];
            const realName = req.body["realName"];
            const sex = req.body["sex"]; // 0: 女  1: 男
            const age = req.body["age"];
            const telephone = req.body["telephone"];
            const idCard = req.body["idCard"];
            const healthReportUrl = req.body["healthReportUrl"];
            const height = req.body["height"];
            const weight = req.body["weight"];
            const waistline = req.body["waistline"];
            const hipline = req.body["hipline"];
            const sbp = req.body["sbp"];
            const dbp = req.body["dbp"];
            const rbg = req.body["rbg"];
            const fpg = req.body["fpg"];
            const hpg = req.body["hpg"];
            const ua = req.body["ua"];
            const tc = req.body["tc"];
            const tg = req.body["tg"];
            const hdlc = req.body["hdlc"];
            const ldlc = req.body["ldlc"];
            const origin = req.body["origin"]; // 1 : 大地
            const originId = req.body["originId"];
            try {
                const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
                const userHealthInformation = yield userHealthInformationService.updateById(id, realName, sex, age, telephone, idCard, healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, originId);
                res.json(new base_response_1.ResponseSuccess(userHealthInformation[0], "UPDATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static deleteById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
                const userHealthInformation = yield userHealthInformationService.deleteById(id);
                res.json(new base_response_1.ResponseSuccess(userHealthInformation, "DELETE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    // export
    static export(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const startTime = req.query["startTime"];
            const endTime = req.query["endTime"];
            const userHealthInformationService = new user_health_information_service_1.UserHealthInformationService();
            const resultData = yield userHealthInformationService.export(startTime, endTime);
            console.log("resultData", resultData);
            const conf = {};
            conf.cols = [{
                    caption: "用户真实名称",
                    type: "string",
                    width: 30
                }, {
                    caption: "性别",
                    type: "string",
                    width: 30
                }, {
                    caption: "年龄",
                    type: "string",
                    width: 30
                }, {
                    caption: "电话号码",
                    type: "string",
                    width: 30
                }, {
                    caption: "身份证号码",
                    type: "string",
                    width: 30
                }, {
                    caption: "身高",
                    type: "string",
                    width: 30
                }, {
                    caption: "体重",
                    type: "string",
                    width: 30
                }, {
                    caption: "腰围",
                    type: "string",
                    width: 30
                }, {
                    caption: "臀围",
                    type: "string",
                    width: 30
                }, {
                    caption: "收缩压",
                    type: "string",
                    width: 30
                }, {
                    caption: "舒张压",
                    type: "string",
                    width: 30
                }, {
                    caption: "随机血糖",
                    type: "string",
                    width: 30
                }, {
                    caption: "空腹血糖",
                    type: "string",
                    width: 30
                }, {
                    caption: "餐后2小时血糖",
                    type: "string",
                    width: 30
                }, {
                    caption: "尿酸",
                    type: "string",
                    width: 30
                }, {
                    caption: "总胆固醇",
                    type: "string",
                    width: 30
                }, {
                    caption: "甘油三酯",
                    type: "string",
                    width: 30
                }, {
                    caption: "高密度脂蛋白胆固醇",
                    type: "string",
                    width: 30
                }, {
                    caption: "低密度脂蛋白胆固醇",
                    type: "string",
                    width: 30
                }, {
                    caption: "数据来源",
                    type: "string",
                    width: 30
                }, {
                    caption: "填写时间",
                    type: "string",
                    width: 30
                }, {
                    caption: "体检报告链接",
                    type: "string",
                    width: 100
                }];
            conf.rows = [];
            try {
                for (let i = 0; i < resultData.length; i++) {
                    const userHealthInformation = resultData[i];
                    const row = [];
                    row.push(userHealthInformation.realName ? userHealthInformation.realName + "" : "-");
                    row.push(userHealthInformation.sex === 1 ? "男" : "女");
                    row.push(userHealthInformation.age ? userHealthInformation.age + "" : "-");
                    row.push(userHealthInformation.telephone ? userHealthInformation.telephone + "" : "-");
                    row.push(userHealthInformation.idCard ? userHealthInformation.idCard + "" : "-");
                    row.push(userHealthInformation.height ? userHealthInformation.height + "" : "-");
                    row.push(userHealthInformation.weight ? userHealthInformation.weight + "" : "-");
                    row.push(userHealthInformation.waistline ? userHealthInformation.waistline + "" : "-");
                    row.push(userHealthInformation.hipline ? userHealthInformation.hipline + "" : "-");
                    row.push(userHealthInformation.sbp ? userHealthInformation.sbp + "" : "-");
                    row.push(userHealthInformation.dbp ? userHealthInformation.dbp + "" : "-");
                    row.push(userHealthInformation.rbg ? userHealthInformation.rbg + "" : "-");
                    row.push(userHealthInformation.fpg ? userHealthInformation.fpg + "" : "-");
                    row.push(userHealthInformation.hpg ? userHealthInformation.hpg + "" : "-");
                    row.push(userHealthInformation.ua ? userHealthInformation.ua + "" : "-");
                    row.push(userHealthInformation.tc ? userHealthInformation.tc + "" : "-");
                    row.push(userHealthInformation.tg ? userHealthInformation.tg + "" : "-");
                    row.push(userHealthInformation.hdlc ? userHealthInformation.hdlc + "" : "-");
                    row.push(userHealthInformation.ldlc ? userHealthInformation.ldlc + "" : "-");
                    row.push(userHealthInformation.origin === 1 ? "大地APP" : "-");
                    row.push(userHealthInformation.createdAt ? userHealthInformation.createdAt + "" : "-");
                    row.push(userHealthInformation.healthReportUrl ? userHealthInformation.healthReportUrl + "" : "-");
                    conf.rows.push(row);
                }
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
            const result = nodeExcel.execute(conf);
            res.setHeader("Content-Type", "application/vnd.openxmlformats");
            res.setHeader("Content-Disposition", "attachment; filename=" + "data.xlsx");
            res.end(result, "binary");
            return;
        });
    }
}
exports.UserHealthInformationController = UserHealthInformationController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXIvdXNlci1oZWFsdGgtaW5mb3JtYXRpb24tY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSxnR0FBMEY7QUFDMUYseURBQWtHO0FBQ2xHLE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztBQUMxQyxNQUFhLCtCQUErQjtJQUNqQyxNQUFNLENBQU8sV0FBVyxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUN2RixNQUFNLEVBQUUsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztZQUNoQyxJQUFJO2dCQUNBLE1BQU0sNEJBQTRCLEdBQVEsSUFBSSw4REFBNEIsRUFBRSxDQUFDO2dCQUM3RSxNQUFNLHFCQUFxQixHQUFRLE1BQU0sNEJBQTRCLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN0RixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxxQkFBcUIsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLGdCQUFnQixDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUM1RixNQUFNLFFBQVEsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUM1QyxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUN4QyxJQUFJO2dCQUNBLE1BQU0sNEJBQTRCLEdBQVEsSUFBSSw4REFBNEIsRUFBRSxDQUFDO2dCQUM3RSxNQUFNLHFCQUFxQixHQUFRLE1BQU0sNEJBQTRCLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUN6RyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxxQkFBcUIsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLE9BQU8sQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDbkYsSUFBSTtnQkFDQSxNQUFNLFNBQVMsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEQsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3JELE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2pELE1BQU0sUUFBUSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQy9DLE1BQU0sNEJBQTRCLEdBQVEsSUFBSSw4REFBNEIsRUFBRSxDQUFDO2dCQUM3RSxNQUFNLFNBQVMsR0FBUSxNQUFNLDRCQUE0QixDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDNUcsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7YUFDNUQ7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBTyxTQUFTLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ3JGLElBQUk7Z0JBQ0EsTUFBTSxTQUFTLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3RELE1BQU0sUUFBUSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNyRCxNQUFNLFNBQVMsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNqRCxNQUFNLFFBQVEsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUMvQyxNQUFNLFNBQVMsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNqRCxNQUFNLE9BQU8sR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM3QyxNQUFNLDRCQUE0QixHQUFRLElBQUksOERBQTRCLEVBQUUsQ0FBQztnQkFDN0UsUUFBUTtnQkFDUixNQUFNLFNBQVMsR0FBUSxNQUFNLDRCQUE0QixDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUNsSSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQzthQUM1RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLE1BQU0sQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDbEYsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxNQUFNLEdBQUcsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsYUFBYTtZQUNsRCxNQUFNLEdBQUcsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEQsTUFBTSxNQUFNLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxQyxNQUFNLGVBQWUsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDNUQsTUFBTSxNQUFNLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxQyxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEQsTUFBTSxPQUFPLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QyxNQUFNLEdBQUcsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sR0FBRyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsTUFBTSxHQUFHLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNwQyxNQUFNLEdBQUcsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sR0FBRyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsTUFBTSxFQUFFLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQyxNQUFNLEVBQUUsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xDLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEMsTUFBTSxJQUFJLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QyxNQUFNLElBQUksR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sTUFBTSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTO1lBQ3BELE1BQU0sUUFBUSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsSUFBSTtnQkFDQSxNQUFNLDRCQUE0QixHQUFRLElBQUksOERBQTRCLEVBQUUsQ0FBQztnQkFDN0UsTUFBTSxxQkFBcUIsR0FBUSxNQUFNLDRCQUE0QixDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDNU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO2dCQUM1RCxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLFVBQVUsQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDdEYsTUFBTSxFQUFFLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQyxNQUFNLFFBQVEsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlDLE1BQU0sR0FBRyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxhQUFhO1lBQ2xELE1BQU0sR0FBRyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsTUFBTSxTQUFTLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNoRCxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sZUFBZSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUM1RCxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sTUFBTSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDMUMsTUFBTSxTQUFTLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNoRCxNQUFNLE9BQU8sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sR0FBRyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsTUFBTSxHQUFHLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNwQyxNQUFNLEdBQUcsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sR0FBRyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsTUFBTSxHQUFHLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNwQyxNQUFNLEVBQUUsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xDLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEMsTUFBTSxFQUFFLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQyxNQUFNLElBQUksR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sSUFBSSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEMsTUFBTSxNQUFNLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVM7WUFDcEQsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxJQUFJO2dCQUNBLE1BQU0sNEJBQTRCLEdBQVEsSUFBSSw4REFBNEIsRUFBRSxDQUFDO2dCQUM3RSxNQUFNLHFCQUFxQixHQUFRLE1BQU0sNEJBQTRCLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDcFAsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUMxRSxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBR00sTUFBTSxDQUFPLFVBQVUsQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDdEYsTUFBTSxFQUFFLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDaEMsSUFBSTtnQkFDQSxNQUFNLDRCQUE0QixHQUFRLElBQUksOERBQTRCLEVBQUUsQ0FBQztnQkFDN0UsTUFBTSxxQkFBcUIsR0FBUSxNQUFNLDRCQUE0QixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDckYsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMscUJBQXFCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUN2RSxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRUQsU0FBUztJQUNGLE1BQU0sQ0FBTyxNQUFNLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ2xGLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakQsTUFBTSxPQUFPLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM3QyxNQUFNLDRCQUE0QixHQUFRLElBQUksOERBQTRCLEVBQUUsQ0FBQztZQUM3RSxNQUFNLFVBQVUsR0FBUSxNQUFNLDRCQUE0QixDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDdEMsTUFBTSxJQUFJLEdBQVEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQztvQkFDVCxPQUFPLEVBQUUsUUFBUTtvQkFDakIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7aUJBQ1osRUFBRTtvQkFDQyxPQUFPLEVBQUUsSUFBSTtvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsRUFBRTtpQkFDWixFQUFFO29CQUNDLE9BQU8sRUFBRSxJQUFJO29CQUNiLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLE1BQU07b0JBQ2YsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7aUJBQ1osRUFBRTtvQkFDQyxPQUFPLEVBQUUsT0FBTztvQkFDaEIsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7aUJBQ1osRUFBRTtvQkFDQyxPQUFPLEVBQUUsSUFBSTtvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsRUFBRTtpQkFDWixFQUFFO29CQUNDLE9BQU8sRUFBRSxJQUFJO29CQUNiLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLElBQUk7b0JBQ2IsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7aUJBQ1osRUFBRTtvQkFDQyxPQUFPLEVBQUUsSUFBSTtvQkFDYixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsRUFBRTtpQkFDWixFQUFFO29CQUNDLE9BQU8sRUFBRSxLQUFLO29CQUNkLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLEtBQUs7b0JBQ2QsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7aUJBQ1osRUFBRTtvQkFDQyxPQUFPLEVBQUUsTUFBTTtvQkFDZixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsRUFBRTtpQkFDWixFQUFFO29CQUNDLE9BQU8sRUFBRSxNQUFNO29CQUNmLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLFNBQVM7b0JBQ2xCLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLElBQUk7b0JBQ2IsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7aUJBQ1osRUFBRTtvQkFDQyxPQUFPLEVBQUUsTUFBTTtvQkFDZixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsRUFBRTtpQkFDWixFQUFFO29CQUNDLE9BQU8sRUFBRSxNQUFNO29CQUNmLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLFdBQVc7b0JBQ3BCLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLFdBQVc7b0JBQ3BCLElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxFQUFFO2lCQUNaLEVBQUU7b0JBQ0MsT0FBTyxFQUFFLE1BQU07b0JBQ2YsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsS0FBSyxFQUFFLEVBQUU7aUJBQ1osRUFBRTtvQkFDQyxPQUFPLEVBQUUsTUFBTTtvQkFDZixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsRUFBRTtpQkFDWixFQUFFO29CQUNDLE9BQU8sRUFBRSxRQUFRO29CQUNqQixJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsR0FBRztpQkFDYixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNmLElBQUk7Z0JBQ0EsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3hDLE1BQU0scUJBQXFCLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM1QyxNQUFNLEdBQUcsR0FBRyxFQUFFLENBQUM7b0JBQ2YsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNyRixHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3RELEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDM0UsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN2RixHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ2pGLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDakYsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNqRixHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3ZGLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbkYsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMzRSxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzNFLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDM0UsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMzRSxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzNFLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDekUsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN6RSxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3pFLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDN0UsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUM3RSxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzdELEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdkYsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNuRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDdkI7YUFDSjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7WUFDRCxNQUFNLE1BQU0sR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZDLEdBQUcsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLGdDQUFnQyxDQUFDLENBQUM7WUFDaEUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRSx1QkFBdUIsR0FBRyxXQUFXLENBQUMsQ0FBQztZQUM1RSxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztZQUMxQixPQUFPO1FBQ1gsQ0FBQztLQUFBO0NBQ0o7QUF6UkQsMEVBeVJDIiwiZmlsZSI6ImNvbnRyb2xsZXIvdXNlci1oZWFsdGgtaW5mb3JtYXRpb24tY29udHJvbGxlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCB7IFVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS91c2VyLWhlYWx0aC1pbmZvcm1hdGlvbi1zZXJ2aWNlXCI7XG5pbXBvcnQgeyBSZXNwb25zZVNlcnZpY2VFcnJvciwgQmFkUmVxdWVzdFJlc3BvbnNlLCBSZXNwb25zZVN1Y2Nlc3MgfSBmcm9tIFwiLi4vYmFzZS9iYXNlLXJlc3BvbnNlXCI7XG5jb25zdCBub2RlRXhjZWwgPSByZXF1aXJlKFwiZXhjZWwtZXhwb3J0XCIpO1xuZXhwb3J0IGNsYXNzIFVzZXJIZWFsdGhJbmZvcm1hdGlvbkNvbnRyb2xsZXIge1xuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgZmluZE9uZUJ5SWQocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3QgaWQ6IHN0cmluZyA9IHJlcS5xdWVyeS5pZDtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2U6IGFueSA9IG5ldyBVc2VySGVhbHRoSW5mb3JtYXRpb25TZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCB1c2VySGVhbHRoSW5mb3JtYXRpb246IGFueSA9IGF3YWl0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UuZmluZE9uZUJ5SWQoaWQpO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2Vzcyh1c2VySGVhbHRoSW5mb3JtYXRpb24sIFwiRklORF9TVUNDRVNTXCIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRPbmVCeU9yaWduSWQocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3Qgb3JpZ2luSWQ6IHN0cmluZyA9IHJlcS5xdWVyeS5vcmlnaW5JZDtcbiAgICAgICAgY29uc3Qgb3JpZ2luOiBudW1iZXIgPSByZXEucXVlcnkub3JpZ2luO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgdXNlckhlYWx0aEluZm9ybWF0aW9uU2VydmljZTogYW55ID0gbmV3IFVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvbjogYW55ID0gYXdhaXQgdXNlckhlYWx0aEluZm9ybWF0aW9uU2VydmljZS5maW5kT25lQnlPcmlnbklkKG9yaWdpbklkLCBvcmlnaW4pO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2Vzcyh1c2VySGVhbHRoSW5mb3JtYXRpb24sIFwiRklORF9TVUNDRVNTXCIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRBbGwocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHBhZ2VJbmRleDogbnVtYmVyID0gcmVxLnF1ZXJ5W1wicGFnZUluZGV4XCJdIHx8IDE7XG4gICAgICAgICAgICBjb25zdCBwYWdlU2l6ZTogbnVtYmVyID0gcmVxLnF1ZXJ5W1wicGFnZVNpemVcIl0gfHwgMTA7XG4gICAgICAgICAgICBjb25zdCB0ZWxlcGhvbmU6IHN0cmluZyA9IHJlcS5xdWVyeVtcInRlbGVwaG9uZVwiXTtcbiAgICAgICAgICAgIGNvbnN0IHJlYWxOYW1lOiBzdHJpbmcgPSByZXEucXVlcnlbXCJyZWFsTmFtZVwiXTtcbiAgICAgICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2U6IGFueSA9IG5ldyBVc2VySGVhbHRoSW5mb3JtYXRpb25TZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBkaXN0cmljdHM6IGFueSA9IGF3YWl0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UuZmluZEFsbCh0ZWxlcGhvbmUsIHJlYWxOYW1lLCBwYWdlSW5kZXgsIHBhZ2VTaXplKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoZGlzdHJpY3RzLCBcIkZJTkRfU1VDQ0VTU1wiKSk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVyci5tZXNzYWdlKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRQQ0FsbChyZXE6IGFueSwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbik6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgcGFnZUluZGV4OiBudW1iZXIgPSByZXEucXVlcnlbXCJwYWdlSW5kZXhcIl0gfHwgMTtcbiAgICAgICAgICAgIGNvbnN0IHBhZ2VTaXplOiBudW1iZXIgPSByZXEucXVlcnlbXCJwYWdlU2l6ZVwiXSB8fCAxMDtcbiAgICAgICAgICAgIGNvbnN0IHRlbGVwaG9uZTogc3RyaW5nID0gcmVxLnF1ZXJ5W1widGVsZXBob25lXCJdO1xuICAgICAgICAgICAgY29uc3QgcmVhbE5hbWU6IHN0cmluZyA9IHJlcS5xdWVyeVtcInJlYWxOYW1lXCJdO1xuICAgICAgICAgICAgY29uc3Qgc3RhcnRUaW1lOiBudW1iZXIgPSByZXEucXVlcnlbXCJzdGFydFRpbWVcIl07XG4gICAgICAgICAgICBjb25zdCBlbmRUaW1lOiBudW1iZXIgPSByZXEucXVlcnlbXCJlbmRUaW1lXCJdO1xuICAgICAgICAgICAgY29uc3QgdXNlckhlYWx0aEluZm9ybWF0aW9uU2VydmljZTogYW55ID0gbmV3IFVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UoKTtcbiAgICAgICAgICAgIC8vIOiOt+WPluaAu+iuv+mXrlxuICAgICAgICAgICAgY29uc3QgZGlzdHJpY3RzOiBhbnkgPSBhd2FpdCB1c2VySGVhbHRoSW5mb3JtYXRpb25TZXJ2aWNlLmZpbmRQQ0FsbCh0ZWxlcGhvbmUsIHJlYWxOYW1lLCBzdGFydFRpbWUsIGVuZFRpbWUsIHBhZ2VJbmRleCwgcGFnZVNpemUpO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2VzcyhkaXN0cmljdHMsIFwiRklORF9TVUNDRVNTXCIpKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyLm1lc3NhZ2UpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgY3JlYXRlKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IHJlYWxOYW1lOiBzdHJpbmcgPSByZXEuYm9keVtcInJlYWxOYW1lXCJdO1xuICAgICAgICBjb25zdCBzZXg6IG51bWJlciA9IHJlcS5ib2R5W1wic2V4XCJdOyAvLyAwOiDlpbMgIDE6IOeUt1xuICAgICAgICBjb25zdCBhZ2U6IG51bWJlciA9IHJlcS5ib2R5W1wiYWdlXCJdO1xuICAgICAgICBjb25zdCB0ZWxlcGhvbmU6IHN0cmluZyA9IHJlcS5ib2R5W1widGVsZXBob25lXCJdO1xuICAgICAgICBjb25zdCBpZENhcmQ6IHN0cmluZyA9IHJlcS5ib2R5W1wiaWRDYXJkXCJdO1xuICAgICAgICBjb25zdCBoZWFsdGhSZXBvcnRVcmw6IHN0cmluZyA9IHJlcS5ib2R5W1wiaGVhbHRoUmVwb3J0VXJsXCJdO1xuICAgICAgICBjb25zdCBoZWlnaHQ6IG51bWJlciA9IHJlcS5ib2R5W1wiaGVpZ2h0XCJdO1xuICAgICAgICBjb25zdCB3ZWlnaHQ6IG51bWJlciA9IHJlcS5ib2R5W1wid2VpZ2h0XCJdO1xuICAgICAgICBjb25zdCB3YWlzdGxpbmU6IG51bWJlciA9IHJlcS5ib2R5W1wid2Fpc3RsaW5lXCJdO1xuICAgICAgICBjb25zdCBoaXBsaW5lOiBudW1iZXIgPSByZXEuYm9keVtcImhpcGxpbmVcIl07XG4gICAgICAgIGNvbnN0IHNicDogbnVtYmVyID0gcmVxLmJvZHlbXCJzYnBcIl07XG4gICAgICAgIGNvbnN0IGRicDogbnVtYmVyID0gcmVxLmJvZHlbXCJkYnBcIl07XG4gICAgICAgIGNvbnN0IHJiZzogbnVtYmVyID0gcmVxLmJvZHlbXCJyYmdcIl07XG4gICAgICAgIGNvbnN0IGZwZzogbnVtYmVyID0gcmVxLmJvZHlbXCJmcGdcIl07XG4gICAgICAgIGNvbnN0IGhwZzogbnVtYmVyID0gcmVxLmJvZHlbXCJocGdcIl07XG4gICAgICAgIGNvbnN0IHVhOiBudW1iZXIgPSByZXEuYm9keVtcInVhXCJdO1xuICAgICAgICBjb25zdCB0YzogbnVtYmVyID0gcmVxLmJvZHlbXCJ0Y1wiXTtcbiAgICAgICAgY29uc3QgdGc6IG51bWJlciA9IHJlcS5ib2R5W1widGdcIl07XG4gICAgICAgIGNvbnN0IGhkbGM6IG51bWJlciA9IHJlcS5ib2R5W1wiaGRsY1wiXTtcbiAgICAgICAgY29uc3QgbGRsYzogbnVtYmVyID0gcmVxLmJvZHlbXCJsZGxjXCJdO1xuICAgICAgICBjb25zdCBvcmlnaW46IG51bWJlciA9IHJlcS5ib2R5W1wib3JpZ2luXCJdOyAvLyAxIDog5aSn5ZywXG4gICAgICAgIGNvbnN0IG9yaWdpbklkOiBzdHJpbmcgPSByZXEuYm9keVtcIm9yaWdpbklkXCJdO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgdXNlckhlYWx0aEluZm9ybWF0aW9uU2VydmljZTogYW55ID0gbmV3IFVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvbjogYW55ID0gYXdhaXQgdXNlckhlYWx0aEluZm9ybWF0aW9uU2VydmljZS5jcmVhdGUocmVhbE5hbWUsIHNleCwgYWdlLCB0ZWxlcGhvbmUsIGlkQ2FyZCwgaGVhbHRoUmVwb3J0VXJsLCBoZWlnaHQsIHdlaWdodCwgd2Fpc3RsaW5lLCBoaXBsaW5lLCBzYnAsIGRicCwgcmJnLCBmcGcsIGhwZywgdWEsIHRjLCB0ZywgaGRsYywgbGRsYywgb3JpZ2luLCBvcmlnaW5JZCk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInVzZXJIZWFsdGhJbmZvcm1hdGlvblwiLCB1c2VySGVhbHRoSW5mb3JtYXRpb24pO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2Vzcyh7fSwgXCJDUkVBVEVfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyB1cGRhdGVCeUlkKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEuYm9keVtcImlkXCJdO1xuICAgICAgICBjb25zdCByZWFsTmFtZTogc3RyaW5nID0gcmVxLmJvZHlbXCJyZWFsTmFtZVwiXTtcbiAgICAgICAgY29uc3Qgc2V4OiBudW1iZXIgPSByZXEuYm9keVtcInNleFwiXTsgLy8gMDog5aWzICAxOiDnlLdcbiAgICAgICAgY29uc3QgYWdlOiBudW1iZXIgPSByZXEuYm9keVtcImFnZVwiXTtcbiAgICAgICAgY29uc3QgdGVsZXBob25lOiBzdHJpbmcgPSByZXEuYm9keVtcInRlbGVwaG9uZVwiXTtcbiAgICAgICAgY29uc3QgaWRDYXJkOiBzdHJpbmcgPSByZXEuYm9keVtcImlkQ2FyZFwiXTtcbiAgICAgICAgY29uc3QgaGVhbHRoUmVwb3J0VXJsOiBzdHJpbmcgPSByZXEuYm9keVtcImhlYWx0aFJlcG9ydFVybFwiXTtcbiAgICAgICAgY29uc3QgaGVpZ2h0OiBudW1iZXIgPSByZXEuYm9keVtcImhlaWdodFwiXTtcbiAgICAgICAgY29uc3Qgd2VpZ2h0OiBudW1iZXIgPSByZXEuYm9keVtcIndlaWdodFwiXTtcbiAgICAgICAgY29uc3Qgd2Fpc3RsaW5lOiBudW1iZXIgPSByZXEuYm9keVtcIndhaXN0bGluZVwiXTtcbiAgICAgICAgY29uc3QgaGlwbGluZTogbnVtYmVyID0gcmVxLmJvZHlbXCJoaXBsaW5lXCJdO1xuICAgICAgICBjb25zdCBzYnA6IG51bWJlciA9IHJlcS5ib2R5W1wic2JwXCJdO1xuICAgICAgICBjb25zdCBkYnA6IG51bWJlciA9IHJlcS5ib2R5W1wiZGJwXCJdO1xuICAgICAgICBjb25zdCByYmc6IG51bWJlciA9IHJlcS5ib2R5W1wicmJnXCJdO1xuICAgICAgICBjb25zdCBmcGc6IG51bWJlciA9IHJlcS5ib2R5W1wiZnBnXCJdO1xuICAgICAgICBjb25zdCBocGc6IG51bWJlciA9IHJlcS5ib2R5W1wiaHBnXCJdO1xuICAgICAgICBjb25zdCB1YTogbnVtYmVyID0gcmVxLmJvZHlbXCJ1YVwiXTtcbiAgICAgICAgY29uc3QgdGM6IG51bWJlciA9IHJlcS5ib2R5W1widGNcIl07XG4gICAgICAgIGNvbnN0IHRnOiBudW1iZXIgPSByZXEuYm9keVtcInRnXCJdO1xuICAgICAgICBjb25zdCBoZGxjOiBudW1iZXIgPSByZXEuYm9keVtcImhkbGNcIl07XG4gICAgICAgIGNvbnN0IGxkbGM6IG51bWJlciA9IHJlcS5ib2R5W1wibGRsY1wiXTtcbiAgICAgICAgY29uc3Qgb3JpZ2luOiBudW1iZXIgPSByZXEuYm9keVtcIm9yaWdpblwiXTsgLy8gMSA6IOWkp+WcsFxuICAgICAgICBjb25zdCBvcmlnaW5JZDogc3RyaW5nID0gcmVxLmJvZHlbXCJvcmlnaW5JZFwiXTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2U6IGFueSA9IG5ldyBVc2VySGVhbHRoSW5mb3JtYXRpb25TZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCB1c2VySGVhbHRoSW5mb3JtYXRpb246IGFueSA9IGF3YWl0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UudXBkYXRlQnlJZChpZCwgcmVhbE5hbWUsIHNleCwgYWdlLCB0ZWxlcGhvbmUsIGlkQ2FyZCwgaGVhbHRoUmVwb3J0VXJsLCBoZWlnaHQsIHdlaWdodCwgd2Fpc3RsaW5lLCBoaXBsaW5lLCBzYnAsIGRicCwgcmJnLCBmcGcsIGhwZywgdWEsIHRjLCB0ZywgaGRsYywgbGRsYywgb3JpZ2luLCBvcmlnaW5JZCk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKHVzZXJIZWFsdGhJbmZvcm1hdGlvblswXSwgXCJVUERBVEVfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGRlbGV0ZUJ5SWQocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3QgaWQ6IHN0cmluZyA9IHJlcS5xdWVyeS5pZDtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2U6IGFueSA9IG5ldyBVc2VySGVhbHRoSW5mb3JtYXRpb25TZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCB1c2VySGVhbHRoSW5mb3JtYXRpb246IGFueSA9IGF3YWl0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UuZGVsZXRlQnlJZChpZCk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKHVzZXJIZWFsdGhJbmZvcm1hdGlvbiwgXCJERUxFVEVfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gZXhwb3J0XG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBleHBvcnQocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3Qgc3RhcnRUaW1lOiBudW1iZXIgPSByZXEucXVlcnlbXCJzdGFydFRpbWVcIl07XG4gICAgICAgIGNvbnN0IGVuZFRpbWU6IG51bWJlciA9IHJlcS5xdWVyeVtcImVuZFRpbWVcIl07XG4gICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2U6IGFueSA9IG5ldyBVc2VySGVhbHRoSW5mb3JtYXRpb25TZXJ2aWNlKCk7XG4gICAgICAgIGNvbnN0IHJlc3VsdERhdGE6IGFueSA9IGF3YWl0IHVzZXJIZWFsdGhJbmZvcm1hdGlvblNlcnZpY2UuZXhwb3J0KHN0YXJ0VGltZSwgZW5kVGltZSk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwicmVzdWx0RGF0YVwiLCByZXN1bHREYXRhKTtcbiAgICAgICAgY29uc3QgY29uZjogYW55ID0ge307XG4gICAgICAgIGNvbmYuY29scyA9IFt7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIueUqOaIt+ecn+WunuWQjeensFwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuaAp+WIq1wiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuW5tOm+hFwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIueUteivneWPt+eggVwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIui6q+S7veivgeWPt+eggVwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIui6q+mrmFwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuS9k+mHjVwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuiFsOWbtFwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuiHgOWbtFwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuaUtue8qeWOi1wiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuiIkuW8oOWOi1wiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIumaj+acuuihgOezllwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIuepuuiFueihgOezllwiLFxuICAgICAgICAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgICAgIHdpZHRoOiAzMFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBjYXB0aW9uOiBcIumkkOWQjjLlsI/ml7booYDns5ZcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLlsL/phbhcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLmgLvog4blm7rphodcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLnlJjmsrnkuInpha9cIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLpq5jlr4bluqbohILom4vnmb3og4blm7rphodcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLkvY7lr4bluqbohILom4vnmb3og4blm7rphodcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLmlbDmja7mnaXmupBcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLloavlhpnml7bpl7RcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMzBcbiAgICAgICAgfSwge1xuICAgICAgICAgICAgY2FwdGlvbjogXCLkvZPmo4DmiqXlkYrpk77mjqVcIixcbiAgICAgICAgICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgICAgICB3aWR0aDogMTAwXG4gICAgICAgIH1dO1xuICAgICAgICBjb25mLnJvd3MgPSBbXTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcmVzdWx0RGF0YS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJIZWFsdGhJbmZvcm1hdGlvbiA9IHJlc3VsdERhdGFbaV07XG4gICAgICAgICAgICAgICAgY29uc3Qgcm93ID0gW107XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLnJlYWxOYW1lID8gdXNlckhlYWx0aEluZm9ybWF0aW9uLnJlYWxOYW1lICsgXCJcIiA6IFwiLVwiKTtcbiAgICAgICAgICAgICAgICByb3cucHVzaCh1c2VySGVhbHRoSW5mb3JtYXRpb24uc2V4ID09PSAxID8gXCLnlLdcIiA6IFwi5aWzXCIpO1xuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5hZ2UgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24uYWdlICsgXCJcIiA6IFwiLVwiKTtcbiAgICAgICAgICAgICAgICByb3cucHVzaCh1c2VySGVhbHRoSW5mb3JtYXRpb24udGVsZXBob25lID8gdXNlckhlYWx0aEluZm9ybWF0aW9uLnRlbGVwaG9uZSArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLmlkQ2FyZCA/IHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5pZENhcmQgKyBcIlwiIDogXCItXCIpO1xuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5oZWlnaHQgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24uaGVpZ2h0ICsgXCJcIiA6IFwiLVwiKTtcbiAgICAgICAgICAgICAgICByb3cucHVzaCh1c2VySGVhbHRoSW5mb3JtYXRpb24ud2VpZ2h0ID8gdXNlckhlYWx0aEluZm9ybWF0aW9uLndlaWdodCArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLndhaXN0bGluZSA/IHVzZXJIZWFsdGhJbmZvcm1hdGlvbi53YWlzdGxpbmUgKyBcIlwiIDogXCItXCIpO1xuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5oaXBsaW5lID8gdXNlckhlYWx0aEluZm9ybWF0aW9uLmhpcGxpbmUgKyBcIlwiIDogXCItXCIpO1xuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5zYnAgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24uc2JwICsgXCJcIiA6IFwiLVwiKTtcbiAgICAgICAgICAgICAgICByb3cucHVzaCh1c2VySGVhbHRoSW5mb3JtYXRpb24uZGJwID8gdXNlckhlYWx0aEluZm9ybWF0aW9uLmRicCArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLnJiZyA/IHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5yYmcgKyBcIlwiIDogXCItXCIpO1xuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5mcGcgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24uZnBnICsgXCJcIiA6IFwiLVwiKTtcbiAgICAgICAgICAgICAgICByb3cucHVzaCh1c2VySGVhbHRoSW5mb3JtYXRpb24uaHBnID8gdXNlckhlYWx0aEluZm9ybWF0aW9uLmhwZyArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLnVhID8gdXNlckhlYWx0aEluZm9ybWF0aW9uLnVhICsgXCJcIiA6IFwiLVwiKTtcbiAgICAgICAgICAgICAgICByb3cucHVzaCh1c2VySGVhbHRoSW5mb3JtYXRpb24udGMgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24udGMgKyBcIlwiIDogXCItXCIpO1xuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi50ZyA/IHVzZXJIZWFsdGhJbmZvcm1hdGlvbi50ZyArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLmhkbGMgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24uaGRsYyArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLmxkbGMgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24ubGRsYyArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLm9yaWdpbiA9PT0gMSA/IFwi5aSn5ZywQVBQXCIgOiBcIi1cIik7XG4gICAgICAgICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLmNyZWF0ZWRBdCA/IHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5jcmVhdGVkQXQgKyBcIlwiIDogXCItXCIpO1xuICAgICAgICAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5oZWFsdGhSZXBvcnRVcmwgPyB1c2VySGVhbHRoSW5mb3JtYXRpb24uaGVhbHRoUmVwb3J0VXJsICsgXCJcIiA6IFwiLVwiKTtcbiAgICAgICAgICAgICAgICBjb25mLnJvd3MucHVzaChyb3cpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCByZXN1bHQgPSBub2RlRXhjZWwuZXhlY3V0ZShjb25mKTtcbiAgICAgICAgcmVzLnNldEhlYWRlcihcIkNvbnRlbnQtVHlwZVwiLCBcImFwcGxpY2F0aW9uL3ZuZC5vcGVueG1sZm9ybWF0c1wiKTtcbiAgICAgICAgcmVzLnNldEhlYWRlcihcIkNvbnRlbnQtRGlzcG9zaXRpb25cIiwgXCJhdHRhY2htZW50OyBmaWxlbmFtZT1cIiArIFwiZGF0YS54bHN4XCIpO1xuICAgICAgICByZXMuZW5kKHJlc3VsdCwgXCJiaW5hcnlcIik7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG59Il19
