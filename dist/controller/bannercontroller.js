"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BannerController = void 0;
const bannerservice_1 = require("../service/bannerservice");
const base_response_1 = require("../base/base-response");
class BannerController {
    static findOneById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const bannerService = new bannerservice_1.BannerService();
                const banner = yield bannerService.findOneById(id);
                res.json(new base_response_1.ResponseSuccess(banner, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static findAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const pageIndex = req.query["pageIndex"] || 1;
                const pageSize = req.query["pageSize"] || 50;
                const bannerService = new bannerservice_1.BannerService();
                const banners = yield bannerService.findAll(pageIndex, pageSize);
                res.json(new base_response_1.ResponseSuccess(banners));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const title = req.body["title"];
            const type = req.body["type"];
            const author = req.body["author"];
            const imageUrl = req.body["imageUrl"];
            const skipLink = req.body["skipLink"];
            const htmlPageNumber = req.body["htmlPageNumber"];
            try {
                const bannerService = new bannerservice_1.BannerService();
                const banner = yield bannerService.create(title, type, author, imageUrl, skipLink, htmlPageNumber);
                res.json(new base_response_1.ResponseSuccess({}, "CREATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static updateById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.body["realName"];
            const title = req.body["title"];
            const type = req.body["type"];
            const author = req.body["author"];
            const imageUrl = req.body["imageUrl"];
            const skipLink = req.body["skipLink"];
            const htmlPageNumber = req.body["htmlPageNumber"];
            try {
                const bannerService = new bannerservice_1.BannerService();
                const banner = yield bannerService.updateById(id, title, type, author, imageUrl, skipLink, htmlPageNumber);
                res.json(new base_response_1.ResponseSuccess({}, "UPDATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static deleteById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const bannerService = new bannerservice_1.BannerService();
                const banner = yield bannerService.deleteById(id);
                res.json(new base_response_1.ResponseSuccess(banner, "DELETE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
}
exports.BannerController = BannerController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXIvYmFubmVyY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSw0REFBeUQ7QUFDekQseURBQW1HO0FBRW5HLE1BQWEsZ0JBQWdCO0lBQ2xCLE1BQU0sQ0FBTyxXQUFXLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ3ZGLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBQ2hDLElBQUk7Z0JBQ0EsTUFBTSxhQUFhLEdBQVEsSUFBSSw2QkFBYSxFQUFFLENBQUM7Z0JBQy9DLE1BQU0sTUFBTSxHQUFRLE1BQU0sYUFBYSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDeEQsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsTUFBTSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RELE9BQU87YUFDVjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sT0FBTyxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUNuRixJQUFJO2dCQUNBLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0RCxNQUFNLFFBQVEsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDckQsTUFBTSxhQUFhLEdBQVEsSUFBSSw2QkFBYSxFQUFFLENBQUM7Z0JBQy9DLE1BQU0sT0FBTyxHQUFRLE1BQU0sYUFBYSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ3RFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDMUM7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBTyxNQUFNLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ2xGLE1BQU0sS0FBSyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsTUFBTSxJQUFJLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QyxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sUUFBUSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxNQUFNLGNBQWMsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDMUQsSUFBSTtnQkFDQSxNQUFNLGFBQWEsR0FBUSxJQUFJLDZCQUFhLEVBQUUsQ0FBQztnQkFDL0MsTUFBTSxNQUFNLEdBQVEsTUFBTSxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUM7Z0JBQ3hHLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLEVBQUUsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELE9BQU87YUFDVjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sVUFBVSxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUN0RixNQUFNLEVBQUUsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sS0FBSyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsTUFBTSxJQUFJLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QyxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sUUFBUSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxNQUFNLGNBQWMsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDMUQsSUFBSTtnQkFDQSxNQUFNLGFBQWEsR0FBUSxJQUFJLDZCQUFhLEVBQUUsQ0FBQztnQkFDL0MsTUFBTSxNQUFNLEdBQVEsTUFBTSxhQUFhLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDO2dCQUNoSCxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxFQUFFLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBR00sTUFBTSxDQUFPLFVBQVUsQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDdEYsTUFBTSxFQUFFLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDaEMsSUFBSTtnQkFDQSxNQUFNLGFBQWEsR0FBUSxJQUFJLDZCQUFhLEVBQUUsQ0FBQztnQkFDL0MsTUFBTSxNQUFNLEdBQVMsTUFBTSxhQUFhLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN4RCxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0NBQ0o7QUE3RUQsNENBNkVDIiwiZmlsZSI6ImNvbnRyb2xsZXIvYmFubmVyY29udHJvbGxlci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCB7IEJhbm5lclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS9iYW5uZXJzZXJ2aWNlXCI7XG5pbXBvcnQgeyBSZXNwb25zZVNlcnZpY2VFcnJvciwgUmVzcG9uc2VQYXJhbXNFcnJvciwgUmVzcG9uc2VTdWNjZXNzIH0gZnJvbSBcIi4uL2Jhc2UvYmFzZS1yZXNwb25zZVwiO1xuaW1wb3J0IHsganNvbiB9IGZyb20gXCJzZXF1ZWxpemVcIjtcbmV4cG9ydCBjbGFzcyBCYW5uZXJDb250cm9sbGVyIHtcbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRPbmVCeUlkKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEucXVlcnkuaWQ7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBiYW5uZXJTZXJ2aWNlOiBhbnkgPSBuZXcgQmFubmVyU2VydmljZSgpO1xuICAgICAgICAgICAgY29uc3QgYmFubmVyOiBhbnkgPSBhd2FpdCBiYW5uZXJTZXJ2aWNlLmZpbmRPbmVCeUlkKGlkKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoYmFubmVyLCBcIkZJTkRfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBmaW5kQWxsKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBwYWdlSW5kZXg6IG51bWJlciA9IHJlcS5xdWVyeVtcInBhZ2VJbmRleFwiXSB8fCAxO1xuICAgICAgICAgICAgY29uc3QgcGFnZVNpemU6IG51bWJlciA9IHJlcS5xdWVyeVtcInBhZ2VTaXplXCJdIHx8IDUwO1xuICAgICAgICAgICAgY29uc3QgYmFubmVyU2VydmljZTogYW55ID0gbmV3IEJhbm5lclNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGJhbm5lcnM6IGFueSA9IGF3YWl0IGJhbm5lclNlcnZpY2UuZmluZEFsbChwYWdlSW5kZXgsIHBhZ2VTaXplKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoYmFubmVycykpO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIubWVzc2FnZSkpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBjcmVhdGUocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3QgdGl0bGU6IHN0cmluZyA9IHJlcS5ib2R5W1widGl0bGVcIl07XG4gICAgICAgIGNvbnN0IHR5cGU6IG51bWJlciA9IHJlcS5ib2R5W1widHlwZVwiXTtcbiAgICAgICAgY29uc3QgYXV0aG9yOiBudW1iZXIgPSByZXEuYm9keVtcImF1dGhvclwiXTtcbiAgICAgICAgY29uc3QgaW1hZ2VVcmw6IHN0cmluZyA9IHJlcS5ib2R5W1wiaW1hZ2VVcmxcIl07XG4gICAgICAgIGNvbnN0IHNraXBMaW5rOiBzdHJpbmcgPSByZXEuYm9keVtcInNraXBMaW5rXCJdO1xuICAgICAgICBjb25zdCBodG1sUGFnZU51bWJlcjogc3RyaW5nID0gcmVxLmJvZHlbXCJodG1sUGFnZU51bWJlclwiXTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IGJhbm5lclNlcnZpY2U6IGFueSA9IG5ldyBCYW5uZXJTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBiYW5uZXI6IGFueSA9IGF3YWl0IGJhbm5lclNlcnZpY2UuY3JlYXRlKHRpdGxlLCB0eXBlLCBhdXRob3IsIGltYWdlVXJsLCBza2lwTGluaywgaHRtbFBhZ2VOdW1iZXIpO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2Vzcyh7fSwgXCJDUkVBVEVfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyB1cGRhdGVCeUlkKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEuYm9keVtcInJlYWxOYW1lXCJdO1xuICAgICAgICBjb25zdCB0aXRsZTogc3RyaW5nID0gcmVxLmJvZHlbXCJ0aXRsZVwiXTtcbiAgICAgICAgY29uc3QgdHlwZTogbnVtYmVyID0gcmVxLmJvZHlbXCJ0eXBlXCJdO1xuICAgICAgICBjb25zdCBhdXRob3I6IG51bWJlciA9IHJlcS5ib2R5W1wiYXV0aG9yXCJdO1xuICAgICAgICBjb25zdCBpbWFnZVVybDogc3RyaW5nID0gcmVxLmJvZHlbXCJpbWFnZVVybFwiXTtcbiAgICAgICAgY29uc3Qgc2tpcExpbms6IHN0cmluZyA9IHJlcS5ib2R5W1wic2tpcExpbmtcIl07XG4gICAgICAgIGNvbnN0IGh0bWxQYWdlTnVtYmVyOiBzdHJpbmcgPSByZXEuYm9keVtcImh0bWxQYWdlTnVtYmVyXCJdO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgYmFubmVyU2VydmljZTogYW55ID0gbmV3IEJhbm5lclNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGJhbm5lcjogYW55ID0gYXdhaXQgYmFubmVyU2VydmljZS51cGRhdGVCeUlkKGlkLCB0aXRsZSwgdHlwZSwgYXV0aG9yLCBpbWFnZVVybCwgc2tpcExpbmssIGh0bWxQYWdlTnVtYmVyKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3Moe30sIFwiVVBEQVRFX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBkZWxldGVCeUlkKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEucXVlcnkuaWQ7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBiYW5uZXJTZXJ2aWNlOiBhbnkgPSBuZXcgQmFubmVyU2VydmljZSgpO1xuICAgICAgICAgICAgY29uc3QgYmFubmVyOiBqc29uID0gYXdhaXQgYmFubmVyU2VydmljZS5kZWxldGVCeUlkKGlkKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoYmFubmVyLCBcIkRFTEVURV9TVUNDRVNTXCIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG59Il19
