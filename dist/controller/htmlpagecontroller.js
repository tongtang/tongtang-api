"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlPageController = void 0;
const htmlpageservice_1 = require("../service/htmlpageservice");
const htmlpagepvservice_1 = require("../service/htmlpagepvservice");
const base_response_1 = require("../base/base-response");
const nodeExcel = require("excel-export");
class HtmlPageController {
    static findAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const type = req.query["type"];
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpages = yield htmlPageService.findAll(type);
                res.json(new base_response_1.ResponseSuccess(htmlpages, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static findPCAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const title = req.query["title"];
                const type = req.query["type"];
                const isOpen = Number(req.query["isOpen"]);
                const startTime = req.query["startTime"];
                const endTime = req.query["endTime"];
                const pageIndex = req.query["pageIndex"] || 1;
                const pageSize = req.query["pageSize"] || 10;
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpages = yield htmlPageService.findPCAll(title, type, isOpen, startTime, endTime, pageIndex, pageSize);
                res.json(new base_response_1.ResponseSuccess(htmlpages, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static findOneById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.findOneById(id);
                htmlPageService.addPV(id);
                // APP端每次请求给这个页面加一次访问量
                const htmlPagePvService = new htmlpagepvservice_1.HtmlPagePvService();
                htmlPagePvService.addPV(id);
                res.json(new base_response_1.ResponseSuccess(htmlpage, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static findPCOneById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.findOneById(id);
                res.json(new base_response_1.ResponseSuccess(htmlpage, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static getHtmlPageCodeList(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpageCodeList = yield htmlPageService.getHtmlPageCodeList();
                res.json(new base_response_1.ResponseSuccess(htmlpageCodeList, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static findOneByPageNumber(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const pageNumber = req.query.pageNumber;
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.findOneByPageNumber(pageNumber);
                res.json(new base_response_1.ResponseSuccess(htmlpage, "FIND_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const title = req.body["title"];
            const comment = req.body["comment"];
            const type = req.body["type"];
            const imageUrl = req.body["imageUrl"];
            const author = req.body["author"];
            const content = req.body["content"];
            const creationAt = req.body["creationAt"];
            const htmlScriptFragment = req.body["htmlScriptFragment"];
            const pageNumber = req.body["pageNumber"];
            const weight = req.body["weight"];
            const isVideo = req.body["isVideo"];
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.create(imageUrl, title, comment, type, author, content, creationAt, htmlScriptFragment, pageNumber, weight, isVideo);
                res.json(new base_response_1.ResponseSuccess({}, "CREATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static updateById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.body["id"];
            const title = req.body["title"];
            const comment = req.body["comment"];
            const type = req.body["type"];
            const imageUrl = req.body["imageUrl"];
            const author = req.body["author"];
            const content = req.body["content"];
            const creationAt = req.body["creationAt"];
            const htmlScriptFragment = req.body["htmlScriptFragment"];
            const pageNumber = req.body["pageNumber"];
            const weight = req.body["weight"];
            const isVideo = req.body["isVideo"];
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.updateById(id, title, comment, type, imageUrl, author, content, creationAt, htmlScriptFragment, pageNumber, weight, isVideo);
                res.json(new base_response_1.ResponseSuccess(htmlpage[0], "UPDATE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static isOpen(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.body.id;
            const isOpen = req.body.isOpen;
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.isOpen(id, isOpen);
                res.json(new base_response_1.ResponseSuccess(htmlpage[0], "ISOPEN_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static deleteById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.query.id;
            try {
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.deleteById(id);
                res.json(new base_response_1.ResponseSuccess(htmlpage, "DELETE_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static export(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const startTime = req.query.startTime;
            const endTime = req.query.endTime;
            const htmlPageService = new htmlpageservice_1.HtmlPageService();
            // 获取所有的页面
            const resultPage = yield htmlPageService.findAllPagePv("");
            const htmlpageIds = [];
            for (let i = 0; i < resultPage.length; i++) {
                htmlpageIds.push(resultPage[i]["id"]);
            }
            if (htmlpageIds.length > 0) {
            }
            console.log("resultPage", resultPage);
            // const resultData: any = await htmlPageService.exportHtmlPagePV(startTime, endTime);
            // const conf: any = {};
            // conf.cols = [{
            //     caption: "页面名称",
            //     type: "string",
            //     width: 30
            // }, {
            //     caption: "访问总PV",
            //     type: "string",
            //     width: 30
            // }];
            // conf.rows = [];
            // try {
            //     for (let i = 0; i < resultData.length; i++) {
            //         const userHealthInformation = resultData[i];
            //         const row = [];
            //         row.push(userHealthInformation.realName ? userHealthInformation.realName + "" : "-");
            //         row.push(userHealthInformation.sex === 1 ? "男" : "女");
            //         conf.rows.push(row);
            //     }
            // } catch (err) {
            //     res.json(new ResponseServiceError(err));
            //     return;
            // }
            // const result = nodeExcel.execute(conf);
            // res.setHeader("Content-Type", "application/vnd.openxmlformats");
            // res.setHeader("Content-Disposition", "attachment; filename=" + "data.xlsx");
            // res.end(result, "binary");
            return;
        });
    }
}
exports.HtmlPageController = HtmlPageController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXIvaHRtbHBhZ2Vjb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUlBLGdFQUE2RDtBQUM3RCxvRUFBaUU7QUFDakUseURBQW1HO0FBQ25HLE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztBQUMxQyxNQUFhLGtCQUFrQjtJQUNwQixNQUFNLENBQU8sT0FBTyxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUNuRixJQUFJO2dCQUNBLE1BQU0sSUFBSSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3ZDLE1BQU0sZUFBZSxHQUFRLElBQUksaUNBQWUsRUFBRSxDQUFDO2dCQUNuRCxNQUFNLFNBQVMsR0FBUSxNQUFNLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNELEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLFNBQVMsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2FBQzVEO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sU0FBUyxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUNyRixJQUFJO2dCQUNBLE1BQU0sS0FBSyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3pDLE1BQU0sSUFBSSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3ZDLE1BQU0sTUFBTSxHQUFXLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2pELE1BQU0sT0FBTyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzdDLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0RCxNQUFNLFFBQVEsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDckQsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sU0FBUyxHQUFRLE1BQU0sZUFBZSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDckgsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7YUFDNUQ7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBTyxXQUFXLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ3ZGLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBQ2hDLElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sUUFBUSxHQUFRLE1BQU0sZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDNUQsZUFBZSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDMUIsc0JBQXNCO2dCQUN0QixNQUFNLGlCQUFpQixHQUFRLElBQUkscUNBQWlCLEVBQUUsQ0FBQztnQkFDdkQsaUJBQWlCLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM1QixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDeEQsT0FBTzthQUNWO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBTyxhQUFhLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ3pGLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBQ2hDLElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sUUFBUSxHQUFRLE1BQU0sZUFBZSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDNUQsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hELE9BQU87YUFDVjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sbUJBQW1CLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQy9GLElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sZ0JBQWdCLEdBQVEsTUFBTSxlQUFlLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDMUUsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsZ0JBQWdCLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDaEUsT0FBTzthQUNWO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUlNLE1BQU0sQ0FBTyxtQkFBbUIsQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDL0YsTUFBTSxVQUFVLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7WUFDaEQsSUFBSTtnQkFDQSxNQUFNLGVBQWUsR0FBUSxJQUFJLGlDQUFlLEVBQUUsQ0FBQztnQkFDbkQsTUFBTSxRQUFRLEdBQVEsTUFBTSxlQUFlLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzVFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN4RCxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLE1BQU0sQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDbEYsTUFBTSxLQUFLLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN4QyxNQUFNLE9BQU8sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sSUFBSSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEMsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sT0FBTyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDNUMsTUFBTSxVQUFVLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRCxNQUFNLGtCQUFrQixHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUNsRSxNQUFNLFVBQVUsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2xELE1BQU0sTUFBTSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDMUMsTUFBTSxPQUFPLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QyxJQUFJO2dCQUNBLE1BQU0sZUFBZSxHQUFRLElBQUksaUNBQWUsRUFBRSxDQUFDO2dCQUNuRCxNQUFNLFFBQVEsR0FBUSxNQUFNLGVBQWUsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLGtCQUFrQixFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ2pLLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLEVBQUUsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELE9BQU87YUFDVjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sVUFBVSxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUN0RixNQUFNLEVBQUUsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xDLE1BQU0sS0FBSyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsTUFBTSxPQUFPLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QyxNQUFNLElBQUksR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sUUFBUSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsTUFBTSxNQUFNLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxQyxNQUFNLE9BQU8sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sVUFBVSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEQsTUFBTSxrQkFBa0IsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDbEUsTUFBTSxVQUFVLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRCxNQUFNLE1BQU0sR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sT0FBTyxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDNUMsSUFBSTtnQkFDQSxNQUFNLGVBQWUsR0FBUSxJQUFJLGlDQUFlLEVBQUUsQ0FBQztnQkFDbkQsTUFBTSxRQUFRLEdBQVEsTUFBTSxlQUFlLENBQUMsVUFBVSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsa0JBQWtCLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDekssR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDN0QsT0FBTzthQUNWO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtJQUVNLE1BQU0sQ0FBTyxNQUFNLENBQUMsR0FBUSxFQUFFLEdBQXFCLEVBQUUsSUFBMEI7O1lBQ2xGLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBQy9CLE1BQU0sTUFBTSxHQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ3ZDLElBQUk7Z0JBQ0EsTUFBTSxlQUFlLEdBQVEsSUFBSSxpQ0FBZSxFQUFFLENBQUM7Z0JBQ25ELE1BQU0sUUFBUSxHQUFRLE1BQU0sZUFBZSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQy9ELEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQzdELE9BQU87YUFDVjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFHTSxNQUFNLENBQU8sVUFBVSxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUN0RixNQUFNLEVBQUUsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztZQUNoQyxJQUFJO2dCQUNBLE1BQU0sZUFBZSxHQUFRLElBQUksaUNBQWUsRUFBRSxDQUFDO2dCQUNuRCxNQUFNLFFBQVEsR0FBUSxNQUFNLGVBQWUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzNELEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLFFBQVEsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0JBQzFELE9BQU87YUFDVjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPO2FBQ1Y7UUFDTCxDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sTUFBTSxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUNsRixNQUFNLFNBQVMsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUM5QyxNQUFNLE9BQU8sR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztZQUMxQyxNQUFNLGVBQWUsR0FBUSxJQUFJLGlDQUFlLEVBQUUsQ0FBQztZQUNuRCxVQUFVO1lBQ1YsTUFBTSxVQUFVLEdBQVEsTUFBTSxlQUFlLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2hFLE1BQU0sV0FBVyxHQUFrQixFQUFFLENBQUM7WUFDdEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3hDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDekM7WUFDRCxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2FBRTNCO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDdEMsc0ZBQXNGO1lBQ3RGLHdCQUF3QjtZQUN4QixpQkFBaUI7WUFDakIsdUJBQXVCO1lBQ3ZCLHNCQUFzQjtZQUN0QixnQkFBZ0I7WUFDaEIsT0FBTztZQUNQLHdCQUF3QjtZQUN4QixzQkFBc0I7WUFDdEIsZ0JBQWdCO1lBQ2hCLE1BQU07WUFDTixrQkFBa0I7WUFDbEIsUUFBUTtZQUNSLG9EQUFvRDtZQUNwRCx1REFBdUQ7WUFDdkQsMEJBQTBCO1lBQzFCLGdHQUFnRztZQUNoRyxpRUFBaUU7WUFDakUsK0JBQStCO1lBQy9CLFFBQVE7WUFDUixrQkFBa0I7WUFDbEIsK0NBQStDO1lBQy9DLGNBQWM7WUFDZCxJQUFJO1lBQ0osMENBQTBDO1lBQzFDLG1FQUFtRTtZQUNuRSwrRUFBK0U7WUFDL0UsNkJBQTZCO1lBQzdCLE9BQU87UUFDWCxDQUFDO0tBQUE7Q0FDSjtBQS9NRCxnREErTUMiLCJmaWxlIjoiY29udHJvbGxlci9odG1scGFnZWNvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gXCJleHByZXNzXCI7XG5pbXBvcnQgeyBWYWxpZGF0b3IgfSBmcm9tIFwidmFsaWRhdG9yLXRzXCI7XG5pbXBvcnQgeyBSZXF1ZXN0UGFyYW1zIH0gZnJvbSBcIi4uL3R5cGVzL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IFJlcXVlc3RVdGlscyB9IGZyb20gXCIuLi91dGlsL3JlcXVlc3QtdXRpbHNcIjtcbmltcG9ydCB7IEh0bWxQYWdlU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlL2h0bWxwYWdlc2VydmljZVwiO1xuaW1wb3J0IHsgSHRtbFBhZ2VQdlNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS9odG1scGFnZXB2c2VydmljZVwiO1xuaW1wb3J0IHsgUmVzcG9uc2VTZXJ2aWNlRXJyb3IsIFJlc3BvbnNlUGFyYW1zRXJyb3IsIFJlc3BvbnNlU3VjY2VzcyB9IGZyb20gXCIuLi9iYXNlL2Jhc2UtcmVzcG9uc2VcIjtcbmNvbnN0IG5vZGVFeGNlbCA9IHJlcXVpcmUoXCJleGNlbC1leHBvcnRcIik7XG5leHBvcnQgY2xhc3MgSHRtbFBhZ2VDb250cm9sbGVyIHtcbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRBbGwocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHR5cGU6IHN0cmluZyA9IHJlcS5xdWVyeVtcInR5cGVcIl07XG4gICAgICAgICAgICBjb25zdCBodG1sUGFnZVNlcnZpY2U6IGFueSA9IG5ldyBIdG1sUGFnZVNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxwYWdlczogYW55ID0gYXdhaXQgaHRtbFBhZ2VTZXJ2aWNlLmZpbmRBbGwodHlwZSk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWxwYWdlcywgXCJGSU5EX1NVQ0NFU1NcIikpO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIubWVzc2FnZSkpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBmaW5kUENBbGwocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHRpdGxlOiBzdHJpbmcgPSByZXEucXVlcnlbXCJ0aXRsZVwiXTtcbiAgICAgICAgICAgIGNvbnN0IHR5cGU6IHN0cmluZyA9IHJlcS5xdWVyeVtcInR5cGVcIl07XG4gICAgICAgICAgICBjb25zdCBpc09wZW46IG51bWJlciA9IE51bWJlcihyZXEucXVlcnlbXCJpc09wZW5cIl0pO1xuICAgICAgICAgICAgY29uc3Qgc3RhcnRUaW1lOiBudW1iZXIgPSByZXEucXVlcnlbXCJzdGFydFRpbWVcIl07XG4gICAgICAgICAgICBjb25zdCBlbmRUaW1lOiBudW1iZXIgPSByZXEucXVlcnlbXCJlbmRUaW1lXCJdO1xuICAgICAgICAgICAgY29uc3QgcGFnZUluZGV4OiBudW1iZXIgPSByZXEucXVlcnlbXCJwYWdlSW5kZXhcIl0gfHwgMTtcbiAgICAgICAgICAgIGNvbnN0IHBhZ2VTaXplOiBudW1iZXIgPSByZXEucXVlcnlbXCJwYWdlU2l6ZVwiXSB8fCAxMDtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxQYWdlU2VydmljZTogYW55ID0gbmV3IEh0bWxQYWdlU2VydmljZSgpO1xuICAgICAgICAgICAgY29uc3QgaHRtbHBhZ2VzOiBhbnkgPSBhd2FpdCBodG1sUGFnZVNlcnZpY2UuZmluZFBDQWxsKHRpdGxlLCB0eXBlLCBpc09wZW4sIHN0YXJ0VGltZSwgZW5kVGltZSwgcGFnZUluZGV4LCBwYWdlU2l6ZSk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWxwYWdlcywgXCJGSU5EX1NVQ0NFU1NcIikpO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIubWVzc2FnZSkpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBmaW5kT25lQnlJZChyZXE6IGFueSwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbik6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICBjb25zdCBpZDogc3RyaW5nID0gcmVxLnF1ZXJ5LmlkO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgaHRtbFBhZ2VTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFBhZ2VTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1scGFnZTogYW55ID0gYXdhaXQgaHRtbFBhZ2VTZXJ2aWNlLmZpbmRPbmVCeUlkKGlkKTtcbiAgICAgICAgICAgIGh0bWxQYWdlU2VydmljZS5hZGRQVihpZCk7XG4gICAgICAgICAgICAvLyBBUFDnq6/mr4/mrKHor7fmsYLnu5nov5nkuKrpobXpnaLliqDkuIDmrKHorr/pl67ph49cbiAgICAgICAgICAgIGNvbnN0IGh0bWxQYWdlUHZTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFBhZ2VQdlNlcnZpY2UoKTtcbiAgICAgICAgICAgIGh0bWxQYWdlUHZTZXJ2aWNlLmFkZFBWKGlkKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoaHRtbHBhZ2UsIFwiRklORF9TVUNDRVNTXCIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRQQ09uZUJ5SWQocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3QgaWQ6IHN0cmluZyA9IHJlcS5xdWVyeS5pZDtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxQYWdlU2VydmljZTogYW55ID0gbmV3IEh0bWxQYWdlU2VydmljZSgpO1xuICAgICAgICAgICAgY29uc3QgaHRtbHBhZ2U6IGFueSA9IGF3YWl0IGh0bWxQYWdlU2VydmljZS5maW5kT25lQnlJZChpZCk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWxwYWdlLCBcIkZJTkRfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBnZXRIdG1sUGFnZUNvZGVMaXN0KHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBodG1sUGFnZVNlcnZpY2U6IGFueSA9IG5ldyBIdG1sUGFnZVNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxwYWdlQ29kZUxpc3Q6IGFueSA9IGF3YWl0IGh0bWxQYWdlU2VydmljZS5nZXRIdG1sUGFnZUNvZGVMaXN0KCk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWxwYWdlQ29kZUxpc3QsIFwiRklORF9TVUNDRVNTXCIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBmaW5kT25lQnlQYWdlTnVtYmVyKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IHBhZ2VOdW1iZXI6IHN0cmluZyA9IHJlcS5xdWVyeS5wYWdlTnVtYmVyO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgaHRtbFBhZ2VTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFBhZ2VTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1scGFnZTogYW55ID0gYXdhaXQgaHRtbFBhZ2VTZXJ2aWNlLmZpbmRPbmVCeVBhZ2VOdW1iZXIocGFnZU51bWJlcik7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKGh0bWxwYWdlLCBcIkZJTkRfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBjcmVhdGUocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3QgdGl0bGU6IHN0cmluZyA9IHJlcS5ib2R5W1widGl0bGVcIl07XG4gICAgICAgIGNvbnN0IGNvbW1lbnQ6IHN0cmluZyA9IHJlcS5ib2R5W1wiY29tbWVudFwiXTtcbiAgICAgICAgY29uc3QgdHlwZTogc3RyaW5nID0gcmVxLmJvZHlbXCJ0eXBlXCJdO1xuICAgICAgICBjb25zdCBpbWFnZVVybDogc3RyaW5nID0gcmVxLmJvZHlbXCJpbWFnZVVybFwiXTtcbiAgICAgICAgY29uc3QgYXV0aG9yOiBzdHJpbmcgPSByZXEuYm9keVtcImF1dGhvclwiXTtcbiAgICAgICAgY29uc3QgY29udGVudDogc3RyaW5nID0gcmVxLmJvZHlbXCJjb250ZW50XCJdO1xuICAgICAgICBjb25zdCBjcmVhdGlvbkF0OiBzdHJpbmcgPSByZXEuYm9keVtcImNyZWF0aW9uQXRcIl07XG4gICAgICAgIGNvbnN0IGh0bWxTY3JpcHRGcmFnbWVudDogc3RyaW5nID0gcmVxLmJvZHlbXCJodG1sU2NyaXB0RnJhZ21lbnRcIl07XG4gICAgICAgIGNvbnN0IHBhZ2VOdW1iZXI6IHN0cmluZyA9IHJlcS5ib2R5W1wicGFnZU51bWJlclwiXTtcbiAgICAgICAgY29uc3Qgd2VpZ2h0OiBudW1iZXIgPSByZXEuYm9keVtcIndlaWdodFwiXTtcbiAgICAgICAgY29uc3QgaXNWaWRlbzogbnVtYmVyID0gcmVxLmJvZHlbXCJpc1ZpZGVvXCJdO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgaHRtbFBhZ2VTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFBhZ2VTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1scGFnZTogYW55ID0gYXdhaXQgaHRtbFBhZ2VTZXJ2aWNlLmNyZWF0ZShpbWFnZVVybCwgdGl0bGUsIGNvbW1lbnQsIHR5cGUsIGF1dGhvciwgY29udGVudCwgY3JlYXRpb25BdCwgaHRtbFNjcmlwdEZyYWdtZW50LCBwYWdlTnVtYmVyLCB3ZWlnaHQsIGlzVmlkZW8pO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2Vzcyh7fSwgXCJDUkVBVEVfU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBhc3luYyB1cGRhdGVCeUlkKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEuYm9keVtcImlkXCJdO1xuICAgICAgICBjb25zdCB0aXRsZTogc3RyaW5nID0gcmVxLmJvZHlbXCJ0aXRsZVwiXTtcbiAgICAgICAgY29uc3QgY29tbWVudDogc3RyaW5nID0gcmVxLmJvZHlbXCJjb21tZW50XCJdO1xuICAgICAgICBjb25zdCB0eXBlOiBzdHJpbmcgPSByZXEuYm9keVtcInR5cGVcIl07XG4gICAgICAgIGNvbnN0IGltYWdlVXJsOiBzdHJpbmcgPSByZXEuYm9keVtcImltYWdlVXJsXCJdO1xuICAgICAgICBjb25zdCBhdXRob3I6IHN0cmluZyA9IHJlcS5ib2R5W1wiYXV0aG9yXCJdO1xuICAgICAgICBjb25zdCBjb250ZW50OiBzdHJpbmcgPSByZXEuYm9keVtcImNvbnRlbnRcIl07XG4gICAgICAgIGNvbnN0IGNyZWF0aW9uQXQ6IHN0cmluZyA9IHJlcS5ib2R5W1wiY3JlYXRpb25BdFwiXTtcbiAgICAgICAgY29uc3QgaHRtbFNjcmlwdEZyYWdtZW50OiBzdHJpbmcgPSByZXEuYm9keVtcImh0bWxTY3JpcHRGcmFnbWVudFwiXTtcbiAgICAgICAgY29uc3QgcGFnZU51bWJlcjogc3RyaW5nID0gcmVxLmJvZHlbXCJwYWdlTnVtYmVyXCJdO1xuICAgICAgICBjb25zdCB3ZWlnaHQ6IG51bWJlciA9IHJlcS5ib2R5W1wid2VpZ2h0XCJdO1xuICAgICAgICBjb25zdCBpc1ZpZGVvOiBudW1iZXIgPSByZXEuYm9keVtcImlzVmlkZW9cIl07XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBodG1sUGFnZVNlcnZpY2U6IGFueSA9IG5ldyBIdG1sUGFnZVNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxwYWdlOiBhbnkgPSBhd2FpdCBodG1sUGFnZVNlcnZpY2UudXBkYXRlQnlJZChpZCwgdGl0bGUsIGNvbW1lbnQsIHR5cGUsIGltYWdlVXJsLCBhdXRob3IsIGNvbnRlbnQsIGNyZWF0aW9uQXQsIGh0bWxTY3JpcHRGcmFnbWVudCwgcGFnZU51bWJlciwgd2VpZ2h0LCBpc1ZpZGVvKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoaHRtbHBhZ2VbMF0sIFwiVVBEQVRFX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgaXNPcGVuKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IGlkOiBzdHJpbmcgPSByZXEuYm9keS5pZDtcbiAgICAgICAgY29uc3QgaXNPcGVuOiBudW1iZXIgPSByZXEuYm9keS5pc09wZW47XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCBodG1sUGFnZVNlcnZpY2U6IGFueSA9IG5ldyBIdG1sUGFnZVNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxwYWdlOiBhbnkgPSBhd2FpdCBodG1sUGFnZVNlcnZpY2UuaXNPcGVuKGlkLCBpc09wZW4pO1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU3VjY2VzcyhodG1scGFnZVswXSwgXCJJU09QRU5fU1VDQ0VTU1wiKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVycikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfVxuXG5cbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGRlbGV0ZUJ5SWQocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3QgaWQ6IHN0cmluZyA9IHJlcS5xdWVyeS5pZDtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxQYWdlU2VydmljZTogYW55ID0gbmV3IEh0bWxQYWdlU2VydmljZSgpO1xuICAgICAgICAgICAgY29uc3QgaHRtbHBhZ2U6IGFueSA9IGF3YWl0IGh0bWxQYWdlU2VydmljZS5kZWxldGVCeUlkKGlkKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MoaHRtbHBhZ2UsIFwiREVMRVRFX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgZXhwb3J0KHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IHN0YXJ0VGltZTogbnVtYmVyID0gcmVxLnF1ZXJ5LnN0YXJ0VGltZTtcbiAgICAgICAgY29uc3QgZW5kVGltZTogbnVtYmVyID0gcmVxLnF1ZXJ5LmVuZFRpbWU7XG4gICAgICAgIGNvbnN0IGh0bWxQYWdlU2VydmljZTogYW55ID0gbmV3IEh0bWxQYWdlU2VydmljZSgpO1xuICAgICAgICAvLyDojrflj5bmiYDmnInnmoTpobXpnaJcbiAgICAgICAgY29uc3QgcmVzdWx0UGFnZTogYW55ID0gYXdhaXQgaHRtbFBhZ2VTZXJ2aWNlLmZpbmRBbGxQYWdlUHYoXCJcIik7XG4gICAgICAgIGNvbnN0IGh0bWxwYWdlSWRzOiBBcnJheTxzdHJpbmc+ID0gW107XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcmVzdWx0UGFnZS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaHRtbHBhZ2VJZHMucHVzaChyZXN1bHRQYWdlW2ldW1wiaWRcIl0pO1xuICAgICAgICB9XG4gICAgICAgIGlmIChodG1scGFnZUlkcy5sZW5ndGggPiAwKSB7XG5cbiAgICAgICAgfVxuICAgICAgICBjb25zb2xlLmxvZyhcInJlc3VsdFBhZ2VcIiwgcmVzdWx0UGFnZSk7XG4gICAgICAgIC8vIGNvbnN0IHJlc3VsdERhdGE6IGFueSA9IGF3YWl0IGh0bWxQYWdlU2VydmljZS5leHBvcnRIdG1sUGFnZVBWKHN0YXJ0VGltZSwgZW5kVGltZSk7XG4gICAgICAgIC8vIGNvbnN0IGNvbmY6IGFueSA9IHt9O1xuICAgICAgICAvLyBjb25mLmNvbHMgPSBbe1xuICAgICAgICAvLyAgICAgY2FwdGlvbjogXCLpobXpnaLlkI3np7BcIixcbiAgICAgICAgLy8gICAgIHR5cGU6IFwic3RyaW5nXCIsXG4gICAgICAgIC8vICAgICB3aWR0aDogMzBcbiAgICAgICAgLy8gfSwge1xuICAgICAgICAvLyAgICAgY2FwdGlvbjogXCLorr/pl67mgLtQVlwiLFxuICAgICAgICAvLyAgICAgdHlwZTogXCJzdHJpbmdcIixcbiAgICAgICAgLy8gICAgIHdpZHRoOiAzMFxuICAgICAgICAvLyB9XTtcbiAgICAgICAgLy8gY29uZi5yb3dzID0gW107XG4gICAgICAgIC8vIHRyeSB7XG4gICAgICAgIC8vICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlc3VsdERhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgLy8gICAgICAgICBjb25zdCB1c2VySGVhbHRoSW5mb3JtYXRpb24gPSByZXN1bHREYXRhW2ldO1xuICAgICAgICAvLyAgICAgICAgIGNvbnN0IHJvdyA9IFtdO1xuICAgICAgICAvLyAgICAgICAgIHJvdy5wdXNoKHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5yZWFsTmFtZSA/IHVzZXJIZWFsdGhJbmZvcm1hdGlvbi5yZWFsTmFtZSArIFwiXCIgOiBcIi1cIik7XG4gICAgICAgIC8vICAgICAgICAgcm93LnB1c2godXNlckhlYWx0aEluZm9ybWF0aW9uLnNleCA9PT0gMSA/IFwi55S3XCIgOiBcIuWls1wiKTtcbiAgICAgICAgLy8gICAgICAgICBjb25mLnJvd3MucHVzaChyb3cpO1xuICAgICAgICAvLyAgICAgfVxuICAgICAgICAvLyB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgLy8gICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgLy8gICAgIHJldHVybjtcbiAgICAgICAgLy8gfVxuICAgICAgICAvLyBjb25zdCByZXN1bHQgPSBub2RlRXhjZWwuZXhlY3V0ZShjb25mKTtcbiAgICAgICAgLy8gcmVzLnNldEhlYWRlcihcIkNvbnRlbnQtVHlwZVwiLCBcImFwcGxpY2F0aW9uL3ZuZC5vcGVueG1sZm9ybWF0c1wiKTtcbiAgICAgICAgLy8gcmVzLnNldEhlYWRlcihcIkNvbnRlbnQtRGlzcG9zaXRpb25cIiwgXCJhdHRhY2htZW50OyBmaWxlbmFtZT1cIiArIFwiZGF0YS54bHN4XCIpO1xuICAgICAgICAvLyByZXMuZW5kKHJlc3VsdCwgXCJiaW5hcnlcIik7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG59Il19
