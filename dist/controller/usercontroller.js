"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const base_response_1 = require("../base/base-response");
const userservice_1 = require("../service/userservice");
const base_controller_1 = require("../base/base-controller");
class UserController extends base_controller_1.BaseController {
    static findAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const pageIndex = req.query["pageIndex"] || 1;
                const pageSize = req.query["pageSize"] || 10;
                const userService = new userservice_1.UserService();
                const users = yield userService.findAll(pageIndex, pageSize);
                res.json(new base_response_1.ResponseSuccess(users, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
    static login(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const username = req.body["username"];
            const password = req.body["password"];
            try {
                const userService = new userservice_1.UserService();
                const user = yield userService.findOneByUserName(username);
                if (!user) {
                    res.json(new base_response_1.BadRequestResponse("USER_NOT_FIND"));
                    return;
                }
                if (user.password !== password) {
                    res.json(new base_response_1.BadRequestResponse("USERNAME_OR_PASSWORD_IS_ERR"));
                    return;
                }
                const loginData = yield userService.setTokenInRedis({ id: user.id, username: username });
                res.json(new base_response_1.ResponseSuccess(loginData, "LOGIN_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
    static logout(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = req.headers.token;
            // 清除登陆记录
            try {
                if (token) {
                    const userService = new userservice_1.UserService();
                    yield userService.logout(token);
                }
                res.json(new base_response_1.ResponseSuccess(true, "LOGOUT_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
}
exports.UserController = UserController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXIvdXNlcmNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQ0EseURBQWtHO0FBQ2xHLHdEQUFxRDtBQUNyRCw2REFBeUQ7QUFDekQsTUFBYSxjQUFlLFNBQVEsZ0NBQWM7SUFDdkMsTUFBTSxDQUFPLE9BQU8sQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDbkYsSUFBSTtnQkFDQSxNQUFNLFNBQVMsR0FBVyxHQUFHLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEQsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ3JELE1BQU0sV0FBVyxHQUFRLElBQUkseUJBQVcsRUFBRSxDQUFDO2dCQUMzQyxNQUFNLEtBQUssR0FBUSxNQUFNLFdBQVcsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUNsRSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksK0JBQWUsQ0FBQyxLQUFLLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQzthQUN4RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLEtBQUssQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDakYsTUFBTSxRQUFRLEdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxNQUFNLFFBQVEsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlDLElBQUk7Z0JBQ0EsTUFBTSxXQUFXLEdBQVEsSUFBSSx5QkFBVyxFQUFFLENBQUM7Z0JBQzNDLE1BQU0sSUFBSSxHQUFRLE1BQU0sV0FBVyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNoRSxJQUFJLENBQUMsSUFBSSxFQUFFO29CQUNQLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxrQ0FBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO29CQUNsRCxPQUFPO2lCQUNWO2dCQUNELElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7b0JBQzVCLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxrQ0FBa0IsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7b0JBQ2hFLE9BQU87aUJBQ1Y7Z0JBQ0QsTUFBTSxTQUFTLEdBQVEsTUFBTSxXQUFXLENBQUMsZUFBZSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7Z0JBQzlGLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUMxRCxPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0lBRU0sTUFBTSxDQUFPLE1BQU0sQ0FBQyxHQUFRLEVBQUUsR0FBcUIsRUFBRSxJQUEwQjs7WUFDbEYsTUFBTSxLQUFLLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDaEMsU0FBUztZQUNULElBQUk7Z0JBQ0EsSUFBSSxLQUFLLEVBQUU7b0JBQ1AsTUFBTSxXQUFXLEdBQVEsSUFBSSx5QkFBVyxFQUFFLENBQUM7b0JBQzNDLE1BQU0sV0FBVyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDbkM7Z0JBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLCtCQUFlLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQztnQkFDdEQsT0FBTzthQUNWO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLG9DQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLE9BQU87YUFDVjtRQUNMLENBQUM7S0FBQTtDQUNKO0FBcERELHdDQW9EQyIsImZpbGUiOiJjb250cm9sbGVyL3VzZXJjb250cm9sbGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgZXhwcmVzcyBmcm9tIFwiZXhwcmVzc1wiO1xuaW1wb3J0IHsgUmVzcG9uc2VTZXJ2aWNlRXJyb3IsIEJhZFJlcXVlc3RSZXNwb25zZSwgUmVzcG9uc2VTdWNjZXNzIH0gZnJvbSBcIi4uL2Jhc2UvYmFzZS1yZXNwb25zZVwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS91c2Vyc2VydmljZVwiO1xuaW1wb3J0IHsgQmFzZUNvbnRyb2xsZXIgfSBmcm9tIFwiLi4vYmFzZS9iYXNlLWNvbnRyb2xsZXJcIjtcbmV4cG9ydCBjbGFzcyBVc2VyQ29udHJvbGxlciBleHRlbmRzIEJhc2VDb250cm9sbGVyIHtcbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGZpbmRBbGwocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHBhZ2VJbmRleDogbnVtYmVyID0gcmVxLnF1ZXJ5W1wicGFnZUluZGV4XCJdIHx8IDE7XG4gICAgICAgICAgICBjb25zdCBwYWdlU2l6ZTogbnVtYmVyID0gcmVxLnF1ZXJ5W1wicGFnZVNpemVcIl0gfHwgMTA7XG4gICAgICAgICAgICBjb25zdCB1c2VyU2VydmljZTogYW55ID0gbmV3IFVzZXJTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCB1c2VyczogYW55ID0gYXdhaXQgdXNlclNlcnZpY2UuZmluZEFsbChwYWdlSW5kZXgsIHBhZ2VTaXplKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3ModXNlcnMsIFwiRklORF9TVUNDRVNTXCIpKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTZXJ2aWNlRXJyb3IoZXJyLm1lc3NhZ2UpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgbG9naW4ocmVxOiBhbnksIHJlczogZXhwcmVzcy5SZXNwb25zZSwgbmV4dDogZXhwcmVzcy5OZXh0RnVuY3Rpb24pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgY29uc3QgdXNlcm5hbWU6IHN0cmluZyA9IHJlcS5ib2R5W1widXNlcm5hbWVcIl07XG4gICAgICAgIGNvbnN0IHBhc3N3b3JkOiBzdHJpbmcgPSByZXEuYm9keVtcInBhc3N3b3JkXCJdO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgdXNlclNlcnZpY2U6IGFueSA9IG5ldyBVc2VyU2VydmljZSgpO1xuICAgICAgICAgICAgY29uc3QgdXNlcjogYW55ID0gYXdhaXQgdXNlclNlcnZpY2UuZmluZE9uZUJ5VXNlck5hbWUodXNlcm5hbWUpO1xuICAgICAgICAgICAgaWYgKCF1c2VyKSB7XG4gICAgICAgICAgICAgICAgcmVzLmpzb24obmV3IEJhZFJlcXVlc3RSZXNwb25zZShcIlVTRVJfTk9UX0ZJTkRcIikpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh1c2VyLnBhc3N3b3JkICE9PSBwYXNzd29yZCkge1xuICAgICAgICAgICAgICAgIHJlcy5qc29uKG5ldyBCYWRSZXF1ZXN0UmVzcG9uc2UoXCJVU0VSTkFNRV9PUl9QQVNTV09SRF9JU19FUlJcIikpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IGxvZ2luRGF0YTogYW55ID0gYXdhaXQgdXNlclNlcnZpY2Uuc2V0VG9rZW5JblJlZGlzKHsgaWQ6IHVzZXIuaWQsIHVzZXJuYW1lOiB1c2VybmFtZSB9KTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3MobG9naW5EYXRhLCBcIkxPR0lOX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgYXN5bmMgbG9nb3V0KHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIGNvbnN0IHRva2VuID0gcmVxLmhlYWRlcnMudG9rZW47XG4gICAgICAgIC8vIOa4hemZpOeZu+mZhuiusOW9lVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgaWYgKHRva2VuKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgdXNlclNlcnZpY2U6IGFueSA9IG5ldyBVc2VyU2VydmljZSgpO1xuICAgICAgICAgICAgICAgIGF3YWl0IHVzZXJTZXJ2aWNlLmxvZ291dCh0b2tlbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKHRydWUsIFwiTE9HT1VUX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cbn0iXX0=
