import * as express from "express";
export declare class BannerController {
    static findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static create(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
}
