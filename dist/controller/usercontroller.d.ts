import * as express from "express";
import { BaseController } from "../base/base-controller";
export declare class UserController extends BaseController {
    static findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static login(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static logout(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
}
