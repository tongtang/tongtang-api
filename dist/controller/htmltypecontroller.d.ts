import * as express from "express";
export declare class HtmlTypeController {
    static findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findPCAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static create(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static isOpen(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static cleanDivision(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
}
