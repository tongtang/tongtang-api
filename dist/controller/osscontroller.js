"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OssController = void 0;
const OSS = require("ali-oss");
const STS = OSS.STS;
const co = require("co");
const sts = new STS({
    accessKeyId: "LTAI4GBRs8RDimfuGKJrWtda",
    accessKeySecret: "mqvv4fDmRMK8r2qwADYUjA085DPWvW"
});
const rolearn = "acs:ram::1594210554081543:role/tongtangram";
const policy = {
    "Version": "1",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "oss:ListObjects",
                "oss:GetObject"
            ],
            "Resource": [
                "acs:oss:*:*:ram-test",
                "acs:oss:*:*:ram-test/*"
            ]
        }
    ]
};
const base_response_1 = require("../base/base-response");
class OssController {
    static getOssToken(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const token = yield sts.assumeRole(rolearn, policy, 15 * 60, "tongtangRAM");
                res.json(new base_response_1.ResponseSuccess({
                    token: token.credentials
                }, "GET_OSS_TOKEN_SUCCESS"));
                return;
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err));
                return;
            }
        });
    }
}
exports.OssController = OssController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXIvb3NzY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFDQSxNQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDL0IsTUFBTSxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQztBQUNwQixNQUFNLEVBQUUsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDekIsTUFBTSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUM7SUFDaEIsV0FBVyxFQUFFLDBCQUEwQjtJQUN2QyxlQUFlLEVBQUUsZ0NBQWdDO0NBQ3BELENBQUMsQ0FBQztBQUNILE1BQU0sT0FBTyxHQUFHLDRDQUE0QyxDQUFDO0FBQzdELE1BQU0sTUFBTSxHQUFHO0lBQ1gsU0FBUyxFQUFFLEdBQUc7SUFDZCxXQUFXLEVBQUU7UUFDVDtZQUNJLFFBQVEsRUFBRSxPQUFPO1lBQ2pCLFFBQVEsRUFBRTtnQkFDTixpQkFBaUI7Z0JBQ2pCLGVBQWU7YUFDbEI7WUFDRCxVQUFVLEVBQUU7Z0JBQ1Isc0JBQXNCO2dCQUN0Qix3QkFBd0I7YUFDM0I7U0FDSjtLQUNKO0NBQ0osQ0FBQztBQUNGLHlEQUFrRztBQUNsRyxNQUFhLGFBQWE7SUFDZixNQUFNLENBQU8sV0FBVyxDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUN2RixJQUFJO2dCQUNBLE1BQU0sS0FBSyxHQUFHLE1BQU0sR0FBRyxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBQzVFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDO29CQUN6QixLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVc7aUJBQzNCLEVBQUUsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixPQUFPO2FBQ1Y7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDVixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksb0NBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0NBQ0o7QUFiRCxzQ0FhQyIsImZpbGUiOiJjb250cm9sbGVyL29zc2NvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gXCJleHByZXNzXCI7XG5jb25zdCBPU1MgPSByZXF1aXJlKFwiYWxpLW9zc1wiKTtcbmNvbnN0IFNUUyA9IE9TUy5TVFM7XG5jb25zdCBjbyA9IHJlcXVpcmUoXCJjb1wiKTtcbmNvbnN0IHN0cyA9IG5ldyBTVFMoe1xuICAgIGFjY2Vzc0tleUlkOiBcIkxUQUk0R0JSczhSRGltZnVHS0pyV3RkYVwiLFxuICAgIGFjY2Vzc0tleVNlY3JldDogXCJtcXZ2NGZEbVJNSzhyMnF3QURZVWpBMDg1RFBXdldcIlxufSk7XG5jb25zdCByb2xlYXJuID0gXCJhY3M6cmFtOjoxNTk0MjEwNTU0MDgxNTQzOnJvbGUvdG9uZ3RhbmdyYW1cIjtcbmNvbnN0IHBvbGljeSA9IHtcbiAgICBcIlZlcnNpb25cIjogXCIxXCIsXG4gICAgXCJTdGF0ZW1lbnRcIjogW1xuICAgICAgICB7XG4gICAgICAgICAgICBcIkVmZmVjdFwiOiBcIkFsbG93XCIsXG4gICAgICAgICAgICBcIkFjdGlvblwiOiBbXG4gICAgICAgICAgICAgICAgXCJvc3M6TGlzdE9iamVjdHNcIixcbiAgICAgICAgICAgICAgICBcIm9zczpHZXRPYmplY3RcIlxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIFwiUmVzb3VyY2VcIjogW1xuICAgICAgICAgICAgICAgIFwiYWNzOm9zczoqOio6cmFtLXRlc3RcIixcbiAgICAgICAgICAgICAgICBcImFjczpvc3M6KjoqOnJhbS10ZXN0LypcIlxuICAgICAgICAgICAgXVxuICAgICAgICB9XG4gICAgXVxufTtcbmltcG9ydCB7IFJlc3BvbnNlU2VydmljZUVycm9yLCBCYWRSZXF1ZXN0UmVzcG9uc2UsIFJlc3BvbnNlU3VjY2VzcyB9IGZyb20gXCIuLi9iYXNlL2Jhc2UtcmVzcG9uc2VcIjtcbmV4cG9ydCBjbGFzcyBPc3NDb250cm9sbGVyIHtcbiAgICBwdWJsaWMgc3RhdGljIGFzeW5jIGdldE9zc1Rva2VuKHJlcTogYW55LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UsIG5leHQ6IGV4cHJlc3MuTmV4dEZ1bmN0aW9uKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCB0b2tlbiA9IGF3YWl0IHN0cy5hc3N1bWVSb2xlKHJvbGVhcm4sIHBvbGljeSwgMTUgKiA2MCwgXCJ0b25ndGFuZ1JBTVwiKTtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVN1Y2Nlc3Moe1xuICAgICAgICAgICAgICAgIHRva2VuOiB0b2tlbi5jcmVkZW50aWFsc1xuICAgICAgICAgICAgfSwgXCJHRVRfT1NTX1RPS0VOX1NVQ0NFU1NcIikpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHJlcy5qc29uKG5ldyBSZXNwb25zZVNlcnZpY2VFcnJvcihlcnIpKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cbn0iXX0=
