import * as express from "express";
export declare class UserHealthInformationController {
    static findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findOneByOrignId(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findPCAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static create(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static export(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
}
