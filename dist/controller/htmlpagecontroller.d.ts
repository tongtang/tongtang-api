import * as express from "express";
export declare class HtmlPageController {
    static findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findPCAll(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findPCOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static getHtmlPageCodeList(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static findOneByPageNumber(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static create(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static isOpen(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
    static export(req: any, res: express.Response, next: express.NextFunction): Promise<void>;
}
