"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlPagePvController = void 0;
const htmlpageservice_1 = require("../service/htmlpageservice");
const htmlpagepvservice_1 = require("../service/htmlpagepvservice");
const base_response_1 = require("../base/base-response");
class HtmlPagePvController {
    static getHtmlPagePvById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.query["id"];
                const startTime = req.query["startTime"];
                const endTime = req.query["endTime"];
                const htmlPageService = new htmlpageservice_1.HtmlPageService();
                const htmlpage = yield htmlPageService.findOneById(id);
                const htmlPagePvService = new htmlpagepvservice_1.HtmlPagePvService();
                const result = yield htmlPagePvService.getHtmlPagePvById(id, startTime, endTime);
                res.json(new base_response_1.ResponseSuccess({ "id": id, "pv": htmlpage.pv, "list": result }, "FIND_SUCCESS"));
            }
            catch (err) {
                res.json(new base_response_1.ResponseServiceError(err.message));
                return;
            }
        });
    }
}
exports.HtmlPagePvController = HtmlPagePvController;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRyb2xsZXIvaHRtbHBhZ2VwdmNvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBSUEsZ0VBQTZEO0FBQzdELG9FQUFpRTtBQUNqRSx5REFBbUc7QUFDbkcsTUFBYSxvQkFBb0I7SUFDdEIsTUFBTSxDQUFPLGlCQUFpQixDQUFDLEdBQVEsRUFBRSxHQUFxQixFQUFFLElBQTBCOztZQUM3RixJQUFJO2dCQUNBLE1BQU0sRUFBRSxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25DLE1BQU0sU0FBUyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2pELE1BQU0sT0FBTyxHQUFXLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzdDLE1BQU0sZUFBZSxHQUFRLElBQUksaUNBQWUsRUFBRSxDQUFDO2dCQUNuRCxNQUFNLFFBQVEsR0FBUSxNQUFNLGVBQWUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzVELE1BQU0saUJBQWlCLEdBQVEsSUFBSSxxQ0FBaUIsRUFBRSxDQUFDO2dCQUN2RCxNQUFNLE1BQU0sR0FBUSxNQUFNLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLEVBQUUsRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3RGLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBZSxDQUFDLEVBQUUsSUFBSSxFQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDLEVBQUUsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFDLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQzthQUNsRztZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxvQ0FBb0IsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDaEQsT0FBTzthQUNWO1FBQ0wsQ0FBQztLQUFBO0NBQ0o7QUFoQkQsb0RBZ0JDIiwiZmlsZSI6ImNvbnRyb2xsZXIvaHRtbHBhZ2VwdmNvbnRyb2xsZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gXCJleHByZXNzXCI7XG5pbXBvcnQgeyBWYWxpZGF0b3IgfSBmcm9tIFwidmFsaWRhdG9yLXRzXCI7XG5pbXBvcnQgeyBSZXF1ZXN0UGFyYW1zIH0gZnJvbSBcIi4uL3R5cGVzL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IFJlcXVlc3RVdGlscyB9IGZyb20gXCIuLi91dGlsL3JlcXVlc3QtdXRpbHNcIjtcbmltcG9ydCB7IEh0bWxQYWdlU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlL2h0bWxwYWdlc2VydmljZVwiO1xuaW1wb3J0IHsgSHRtbFBhZ2VQdlNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS9odG1scGFnZXB2c2VydmljZVwiO1xuaW1wb3J0IHsgUmVzcG9uc2VTZXJ2aWNlRXJyb3IsIFJlc3BvbnNlUGFyYW1zRXJyb3IsIFJlc3BvbnNlU3VjY2VzcyB9IGZyb20gXCIuLi9iYXNlL2Jhc2UtcmVzcG9uc2VcIjtcbmV4cG9ydCBjbGFzcyBIdG1sUGFnZVB2Q29udHJvbGxlciB7XG4gICAgcHVibGljIHN0YXRpYyBhc3luYyBnZXRIdG1sUGFnZVB2QnlJZChyZXE6IGFueSwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbik6IFByb21pc2U8dm9pZD4ge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgaWQ6IHN0cmluZyA9IHJlcS5xdWVyeVtcImlkXCJdO1xuICAgICAgICAgICAgY29uc3Qgc3RhcnRUaW1lOiBzdHJpbmcgPSByZXEucXVlcnlbXCJzdGFydFRpbWVcIl07XG4gICAgICAgICAgICBjb25zdCBlbmRUaW1lOiBzdHJpbmcgPSByZXEucXVlcnlbXCJlbmRUaW1lXCJdO1xuICAgICAgICAgICAgY29uc3QgaHRtbFBhZ2VTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFBhZ2VTZXJ2aWNlKCk7XG4gICAgICAgICAgICBjb25zdCBodG1scGFnZTogYW55ID0gYXdhaXQgaHRtbFBhZ2VTZXJ2aWNlLmZpbmRPbmVCeUlkKGlkKTtcbiAgICAgICAgICAgIGNvbnN0IGh0bWxQYWdlUHZTZXJ2aWNlOiBhbnkgPSBuZXcgSHRtbFBhZ2VQdlNlcnZpY2UoKTtcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdDogYW55ID0gYXdhaXQgaHRtbFBhZ2VQdlNlcnZpY2UuZ2V0SHRtbFBhZ2VQdkJ5SWQoaWQsIHN0YXJ0VGltZSwgZW5kVGltZSk7XG4gICAgICAgICAgICByZXMuanNvbihuZXcgUmVzcG9uc2VTdWNjZXNzKHsgXCJpZFwiIDogaWQsIFwicHZcIjogaHRtbHBhZ2UucHYsIFwibGlzdFwiOiByZXN1bHR9LCBcIkZJTkRfU1VDQ0VTU1wiKSk7XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgcmVzLmpzb24obmV3IFJlc3BvbnNlU2VydmljZUVycm9yKGVyci5tZXNzYWdlKSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG59Il19
