"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
const express = require("express");
const bodyParser = require("body-parser");
const sequelize = require("sequelize");
const timeout = require("connect-timeout");
const v1_1 = require("./routers/api/v1");
const v1_2 = require("./routers/pc/v1");
const logger_1 = require("./logger/logger");
const fetch_options_1 = require("./middleware/fetch-options");
const request_timeout_1 = require("./middleware/request-timeout");
const request_dad_1 = require("./middleware/request-dad");
const request_log_1 = require("./middleware/request-log");
const applicationcontext_1 = require("./common/applicationcontext");
const redishelper_1 = require("./common/redishelper");
const base_response_1 = require("./base/base-response");
const config = require("config");
const sequelizeConnction = require("./common/models")();
// 获取node运行环境
let NODE_ENV = process.env.NODE_ENV || "default";
// 初始化运行环境和端口
applicationcontext_1.ApplicationContext.setInstance(applicationcontext_1.ApplicationContext.getInstance().init(NODE_ENV));
console.log("ApplicationContext", applicationcontext_1.ApplicationContext.getRedisConfig());
exports.app = express();
// 设置超时时间
exports.app.use(timeout("600s"));
// 跨域快速返回
exports.app.use(fetch_options_1.fetchOptions);
// 处理超时
exports.app.use(request_timeout_1.requestTimeout);
// for parsing application/json
exports.app.use(bodyParser.json({ limit: "20mb" }));
// for parsing application/x-www-form-urlencoded
exports.app.use(bodyParser.urlencoded({ extended: true }));
exports.app.use("/api/v1/health", function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        res.json({
            data: {
                code: 200,
                data: true
            }
        });
        res.end();
    });
});
const healthArr = [
    "/user/login",
    "/userHealthInformations",
    "/htmlpages",
    "/htmltype/create",
    "/htmltype/update/id",
    "/htmltype/isOpen",
    "/htmltypes",
    "/userHealthInformation/export"
];
// login check
exports.app.use("/pc/v1", function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let pathname = req.path;
        console.log("pathname:" + pathname);
        for (let url of healthArr) {
            if (url === pathname) {
                next();
                return;
            }
        }
        let token = req.headers.token;
        if (!token) {
            res.json(base_response_1.AuthErrorResponse.missingAuthToken());
            return;
        }
        const user = yield redishelper_1.RedisHelper.getInstance().getDataByKey("USER:" + token);
        if (!user) {
            res.json(base_response_1.AuthErrorResponse.missingAuthToken());
            return;
        }
        req.headers.userInfo = JSON.parse(user);
        next();
    });
});
// 业务路由
exports.app.use("/api/v1", v1_1.v1);
exports.app.use("/pc/v1", v1_2.p1);
// 所有路由都未匹配（404）
exports.app.use("*", request_dad_1.requestDad);
// request log
exports.app.use(request_log_1.requestLog);
//  程序启动
const globalAny = global;
globalAny.db = sequelizeConnction;
globalAny.db.query("select name from system where code = 'manage';", { type: sequelize.QueryTypes.SELECT })
    .then((data) => {
    console.log("data", data[0]);
    exports.app.listen(3000, () => {
        console.info(`app start ${process.env.HOST_IP}:3000`);
    });
}, (err) => {
    console.error("数据库连接失败：", err);
});
// uncaughtException 避免程序崩溃
process.on("uncaughtException", function (err) {
    logger_1.AppLogger.error("未知异常:" + err);
});
process.on("unhandledRejection", function (err, promise) {
    logger_1.AppLogger.error("未知异常:", err, promise);
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxtQ0FBbUM7QUFFbkMsMENBQTBDO0FBQzFDLHVDQUF1QztBQUN2QywyQ0FBMkM7QUFDM0MseUNBQXNDO0FBQ3RDLHdDQUFxQztBQUNyQyw0Q0FBNEM7QUFDNUMsOERBQTBEO0FBQzFELGtFQUE4RDtBQUM5RCwwREFBc0Q7QUFDdEQsMERBQXNEO0FBQ3RELG9FQUFpRTtBQUNqRSxzREFBbUQ7QUFDbkQsd0RBQXlEO0FBQ3pELE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUNqQyxNQUFNLGtCQUFrQixHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUM7QUFDeEQsYUFBYTtBQUNiLElBQUksUUFBUSxHQUFXLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxJQUFJLFNBQVMsQ0FBQztBQUN6RCxhQUFhO0FBQ2IsdUNBQWtCLENBQUMsV0FBVyxDQUFDLHVDQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0FBQ2hGLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsdUNBQWtCLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztBQUU1RCxRQUFBLEdBQUcsR0FBd0IsT0FBTyxFQUFFLENBQUM7QUFDaEQsU0FBUztBQUNULFdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7QUFDekIsU0FBUztBQUNULFdBQUcsQ0FBQyxHQUFHLENBQUMsNEJBQVksQ0FBQyxDQUFDO0FBQ3RCLE9BQU87QUFDUCxXQUFHLENBQUMsR0FBRyxDQUFDLGdDQUFjLENBQUMsQ0FBQztBQUN4QiwrQkFBK0I7QUFDL0IsV0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztBQUM1QyxnREFBZ0Q7QUFDaEQsV0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztBQUNuRCxXQUFHLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLFVBQWdCLEdBQW9CLEVBQUUsR0FBcUI7O1FBQ2pGLEdBQUcsQ0FBQyxJQUFJLENBQUM7WUFDTCxJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFLEdBQUc7Z0JBQ1QsSUFBSSxFQUFFLElBQUk7YUFDYjtTQUNKLENBQUMsQ0FBQztRQUNILEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztJQUNkLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxNQUFNLFNBQVMsR0FBYTtJQUN4QixhQUFhO0lBQ2IseUJBQXlCO0lBQ3pCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osK0JBQStCO0NBQ2xDLENBQUM7QUFDRixjQUFjO0FBQ2QsV0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsVUFBZ0IsR0FBb0IsRUFBRSxHQUFxQixFQUFFLElBQTBCOztRQUNyRyxJQUFJLFFBQVEsR0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxDQUFDO1FBQ3BDLEtBQUssSUFBSSxHQUFHLElBQUksU0FBUyxFQUFFO1lBQ3ZCLElBQUksR0FBRyxLQUFLLFFBQVEsRUFBRTtnQkFDbEIsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsT0FBTzthQUNWO1NBQ0o7UUFDRCxJQUFJLEtBQUssR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1IsR0FBRyxDQUFDLElBQUksQ0FBQyxpQ0FBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7WUFDL0MsT0FBTztTQUNWO1FBQ0QsTUFBTSxJQUFJLEdBQVEsTUFBTSx5QkFBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLFlBQVksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNQLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUNBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO1lBQy9DLE9BQU87U0FDVjtRQUNELEdBQUcsQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEMsSUFBSSxFQUFFLENBQUM7SUFDWCxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsT0FBTztBQUNQLFdBQUcsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQUUsQ0FBQyxDQUFDO0FBQ3ZCLFdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE9BQUUsQ0FBQyxDQUFDO0FBQ3RCLGdCQUFnQjtBQUNoQixXQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSx3QkFBVSxDQUFDLENBQUM7QUFDekIsY0FBYztBQUNkLFdBQUcsQ0FBQyxHQUFHLENBQUMsd0JBQVUsQ0FBQyxDQUFDO0FBQ3BCLFFBQVE7QUFDUixNQUFNLFNBQVMsR0FBUSxNQUFNLENBQUM7QUFDOUIsU0FBUyxDQUFDLEVBQUUsR0FBRyxrQkFBa0IsQ0FBQztBQUNsQyxTQUFTLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxnREFBZ0QsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDO0tBQ3RHLElBQUksQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFO0lBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdCLFdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRTtRQUNsQixPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLE9BQU8sQ0FBQyxDQUFDO0lBQzFELENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxFQUFFLENBQUMsR0FBUSxFQUFFLEVBQUU7SUFDWixPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztBQUNuQyxDQUFDLENBQUMsQ0FBQztBQUdQLDJCQUEyQjtBQUMzQixPQUFPLENBQUMsRUFBRSxDQUFDLG1CQUFtQixFQUFFLFVBQVUsR0FBRztJQUN6QyxrQkFBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLENBQUM7QUFDbkMsQ0FBQyxDQUFDLENBQUM7QUFFSCxPQUFPLENBQUMsRUFBRSxDQUFDLG9CQUFvQixFQUFFLFVBQVUsR0FBRyxFQUFFLE9BQU87SUFDbkQsa0JBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztBQUMzQyxDQUFDLENBQUMsQ0FBQyIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gXCJleHByZXNzXCI7XG5pbXBvcnQgKiBhcyB1cmwgZnJvbSBcInVybFwiO1xuaW1wb3J0ICogYXMgYm9keVBhcnNlciBmcm9tIFwiYm9keS1wYXJzZXJcIjtcbmltcG9ydCAqIGFzIHNlcXVlbGl6ZSBmcm9tIFwic2VxdWVsaXplXCI7XG5pbXBvcnQgKiBhcyB0aW1lb3V0IGZyb20gXCJjb25uZWN0LXRpbWVvdXRcIjtcbmltcG9ydCB7IHYxIH0gZnJvbSBcIi4vcm91dGVycy9hcGkvdjFcIjtcbmltcG9ydCB7IHAxIH0gZnJvbSBcIi4vcm91dGVycy9wYy92MVwiO1xuaW1wb3J0IHsgQXBwTG9nZ2VyIH0gZnJvbSBcIi4vbG9nZ2VyL2xvZ2dlclwiO1xuaW1wb3J0IHsgZmV0Y2hPcHRpb25zIH0gZnJvbSBcIi4vbWlkZGxld2FyZS9mZXRjaC1vcHRpb25zXCI7XG5pbXBvcnQgeyByZXF1ZXN0VGltZW91dCB9IGZyb20gXCIuL21pZGRsZXdhcmUvcmVxdWVzdC10aW1lb3V0XCI7XG5pbXBvcnQgeyByZXF1ZXN0RGFkIH0gZnJvbSBcIi4vbWlkZGxld2FyZS9yZXF1ZXN0LWRhZFwiO1xuaW1wb3J0IHsgcmVxdWVzdExvZyB9IGZyb20gXCIuL21pZGRsZXdhcmUvcmVxdWVzdC1sb2dcIjtcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29udGV4dCB9IGZyb20gXCIuL2NvbW1vbi9hcHBsaWNhdGlvbmNvbnRleHRcIjtcbmltcG9ydCB7IFJlZGlzSGVscGVyIH0gZnJvbSBcIi4vY29tbW9uL3JlZGlzaGVscGVyXCI7XG5pbXBvcnQgeyBBdXRoRXJyb3JSZXNwb25zZSB9IGZyb20gXCIuL2Jhc2UvYmFzZS1yZXNwb25zZVwiO1xuY29uc3QgY29uZmlnID0gcmVxdWlyZShcImNvbmZpZ1wiKTtcbmNvbnN0IHNlcXVlbGl6ZUNvbm5jdGlvbiA9IHJlcXVpcmUoXCIuL2NvbW1vbi9tb2RlbHNcIikoKTtcbi8vIOiOt+WPlm5vZGXov5DooYznjq/looNcbmxldCBOT0RFX0VOVjogc3RyaW5nID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgfHwgXCJkZWZhdWx0XCI7XG4vLyDliJ3lp4vljJbov5DooYznjq/looPlkoznq6/lj6NcbkFwcGxpY2F0aW9uQ29udGV4dC5zZXRJbnN0YW5jZShBcHBsaWNhdGlvbkNvbnRleHQuZ2V0SW5zdGFuY2UoKS5pbml0KE5PREVfRU5WKSk7XG5jb25zb2xlLmxvZyhcIkFwcGxpY2F0aW9uQ29udGV4dFwiLCBBcHBsaWNhdGlvbkNvbnRleHQuZ2V0UmVkaXNDb25maWcoKSk7XG5cbmV4cG9ydCBsZXQgYXBwOiBleHByZXNzLkFwcGxpY2F0aW9uID0gZXhwcmVzcygpO1xuLy8g6K6+572u6LaF5pe25pe26Ze0XG5hcHAudXNlKHRpbWVvdXQoXCI2MDBzXCIpKTtcbi8vIOi3qOWfn+W/q+mAn+i/lOWbnlxuYXBwLnVzZShmZXRjaE9wdGlvbnMpO1xuLy8g5aSE55CG6LaF5pe2XG5hcHAudXNlKHJlcXVlc3RUaW1lb3V0KTtcbi8vIGZvciBwYXJzaW5nIGFwcGxpY2F0aW9uL2pzb25cbmFwcC51c2UoYm9keVBhcnNlci5qc29uKHsgbGltaXQ6IFwiMjBtYlwiIH0pKTtcbi8vIGZvciBwYXJzaW5nIGFwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZFxuYXBwLnVzZShib2R5UGFyc2VyLnVybGVuY29kZWQoeyBleHRlbmRlZDogdHJ1ZSB9KSk7XG5hcHAudXNlKFwiL2FwaS92MS9oZWFsdGhcIiwgYXN5bmMgZnVuY3Rpb24gKHJlcTogZXhwcmVzcy5SZXF1ZXN0LCByZXM6IGV4cHJlc3MuUmVzcG9uc2UpIHtcbiAgICByZXMuanNvbih7XG4gICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgIGNvZGU6IDIwMCxcbiAgICAgICAgICAgIGRhdGE6IHRydWVcbiAgICAgICAgfVxuICAgIH0pO1xuICAgIHJlcy5lbmQoKTtcbn0pO1xuY29uc3QgaGVhbHRoQXJyOiBzdHJpbmdbXSA9IFtcbiAgICBcIi91c2VyL2xvZ2luXCIsXG4gICAgXCIvdXNlckhlYWx0aEluZm9ybWF0aW9uc1wiLFxuICAgIFwiL2h0bWxwYWdlc1wiLFxuICAgIFwiL2h0bWx0eXBlL2NyZWF0ZVwiLFxuICAgIFwiL2h0bWx0eXBlL3VwZGF0ZS9pZFwiLFxuICAgIFwiL2h0bWx0eXBlL2lzT3BlblwiLFxuICAgIFwiL2h0bWx0eXBlc1wiLFxuICAgIFwiL3VzZXJIZWFsdGhJbmZvcm1hdGlvbi9leHBvcnRcIlxuXTtcbi8vIGxvZ2luIGNoZWNrXG5hcHAudXNlKFwiL3BjL3YxXCIsIGFzeW5jIGZ1bmN0aW9uIChyZXE6IGV4cHJlc3MuUmVxdWVzdCwgcmVzOiBleHByZXNzLlJlc3BvbnNlLCBuZXh0OiBleHByZXNzLk5leHRGdW5jdGlvbikge1xuICAgIGxldCBwYXRobmFtZTogc3RyaW5nID0gcmVxLnBhdGg7XG4gICAgY29uc29sZS5sb2coXCJwYXRobmFtZTpcIiArIHBhdGhuYW1lKTtcbiAgICBmb3IgKGxldCB1cmwgb2YgaGVhbHRoQXJyKSB7XG4gICAgICAgIGlmICh1cmwgPT09IHBhdGhuYW1lKSB7XG4gICAgICAgICAgICBuZXh0KCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG4gICAgbGV0IHRva2VuID0gcmVxLmhlYWRlcnMudG9rZW47XG4gICAgaWYgKCF0b2tlbikge1xuICAgICAgICByZXMuanNvbihBdXRoRXJyb3JSZXNwb25zZS5taXNzaW5nQXV0aFRva2VuKCkpO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGNvbnN0IHVzZXI6IGFueSA9IGF3YWl0IFJlZGlzSGVscGVyLmdldEluc3RhbmNlKCkuZ2V0RGF0YUJ5S2V5KFwiVVNFUjpcIiArIHRva2VuKTtcbiAgICBpZiAoIXVzZXIpIHtcbiAgICAgICAgcmVzLmpzb24oQXV0aEVycm9yUmVzcG9uc2UubWlzc2luZ0F1dGhUb2tlbigpKTtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICByZXEuaGVhZGVycy51c2VySW5mbyA9IEpTT04ucGFyc2UodXNlcik7XG4gICAgbmV4dCgpO1xufSk7XG5cbi8vIOS4muWKoei3r+eUsVxuYXBwLnVzZShcIi9hcGkvdjFcIiwgdjEpO1xuYXBwLnVzZShcIi9wYy92MVwiLCBwMSk7XG4vLyDmiYDmnInot6/nlLHpg73mnKrljLnphY3vvIg0MDTvvIlcbmFwcC51c2UoXCIqXCIsIHJlcXVlc3REYWQpO1xuLy8gcmVxdWVzdCBsb2dcbmFwcC51c2UocmVxdWVzdExvZyk7XG4vLyAg56iL5bqP5ZCv5YqoXG5jb25zdCBnbG9iYWxBbnk6IGFueSA9IGdsb2JhbDtcbmdsb2JhbEFueS5kYiA9IHNlcXVlbGl6ZUNvbm5jdGlvbjtcbmdsb2JhbEFueS5kYi5xdWVyeShcInNlbGVjdCBuYW1lIGZyb20gc3lzdGVtIHdoZXJlIGNvZGUgPSAnbWFuYWdlJztcIiwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QgfSlcbiAgICAudGhlbigoZGF0YTogYW55KSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiZGF0YVwiLCBkYXRhWzBdKTtcbiAgICAgICAgYXBwLmxpc3RlbigzMDAwLCAoKSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmluZm8oYGFwcCBzdGFydCAke3Byb2Nlc3MuZW52LkhPU1RfSVB9OjMwMDBgKTtcbiAgICAgICAgfSk7XG4gICAgfSwgKGVycjogYW55KSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoXCLmlbDmja7lupPov57mjqXlpLHotKXvvJpcIiwgZXJyKTtcbiAgICB9KTtcblxuXG4vLyB1bmNhdWdodEV4Y2VwdGlvbiDpgb/lhY3nqIvluo/ltKnmuoNcbnByb2Nlc3Mub24oXCJ1bmNhdWdodEV4Y2VwdGlvblwiLCBmdW5jdGlvbiAoZXJyKSB7XG4gICAgQXBwTG9nZ2VyLmVycm9yKFwi5pyq55+l5byC5bi4OlwiICsgZXJyKTtcbn0pO1xuXG5wcm9jZXNzLm9uKFwidW5oYW5kbGVkUmVqZWN0aW9uXCIsIGZ1bmN0aW9uIChlcnIsIHByb21pc2UpIHtcbiAgICBBcHBMb2dnZXIuZXJyb3IoXCLmnKrnn6XlvILluLg6XCIsIGVyciwgcHJvbWlzZSk7XG59KTsiXX0=
