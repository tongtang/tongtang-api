import { ClusterNode, ClusterOptions } from "ioredis";
export interface RedisConfig {
    host?: string;
    port?: number;
    password?: string;
    isClusterMode?: boolean;
    clusterNode?: ClusterNode[];
    clusterOptions?: ClusterOptions;
}
