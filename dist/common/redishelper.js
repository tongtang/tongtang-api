"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisHelper = void 0;
const applicationcontext_1 = require("../common/applicationcontext");
const ioredis_1 = require("ioredis");
const redis = require("ioredis");
/*
* redis 辅助类
* */
class RedisHelper {
    constructor() {
        let redisConfig = applicationcontext_1.ApplicationContext.getRedisConfig();
        this.isClusterMode = redisConfig.isClusterMode;
        if (redisConfig.isClusterMode) {
            this.redisClient_ = new ioredis_1.Cluster(redisConfig.clusterNode, redisConfig.clusterOptions);
        }
        else {
            this.redisClient_ = new redis(redisConfig);
        }
    }
    static getInstance() {
        if (!RedisHelper.instance_) {
            RedisHelper.instance_ = new RedisHelper();
        }
        return RedisHelper.instance_;
    }
    static setInstance(instance) {
        RedisHelper.instance_ = instance;
    }
    getClient() {
        return this.redisClient_;
    }
    /*
    * 根据key读取数据
    * @author gaoqiang@gagogroup.com
    * */
    getDataByKey(key) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.redisClient_.get(key);
        });
    }
    /*
    * 根据key保存数据
    * @author gaoqiang@gagogroup.com
    * */
    setDataWithKey(key, content, expiredMinutes = 10, forever = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!forever) {
                let time = RedisHelper.getExpiredTimestamp_(expiredMinutes);
                return this.redisClient_.set(key, content, "EX", time);
            }
            else {
                return this.redisClient_.set(key, content);
            }
        });
    }
    /*
    * 根据key保存数据 设置有效时间  过后redis删除
    * @author gaoqiang@gagogroup.com
    * */
    setKeyEffectivityDate(key, content, expiredMinutes = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.redisClient_.set(key, content, "EX", expiredMinutes * 60);
        });
    }
    /*
    * 根据key读取数据
    * @author gaoqiang@gagogroup.com
    * */
    lLenListByKey(key) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.redisClient_.llen(key);
        });
    }
    /*
    * 根据key读取数据
    * @author gaoqiang@gagogroup.com
    * */
    rPushBufferDataByKey(key, content) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.redisClient_.rpushBuffer(key, new Buffer(content));
        });
    }
    /*
    * 根据key保存数据
    * @author gaoqiang@gagogroup.com
    * */
    lPopBufferDataWithKey(key) {
        return __awaiter(this, void 0, void 0, function* () {
            let buffer = yield this.redisClient_.lpopBuffer(key);
            if (buffer) {
                return buffer.toString();
            }
            return null;
        });
    }
    /*
    * 根据key更新数据和过期时间
    * @author gaoqiang@gagogroup.com
    * */
    updateDataWithKey(key, content, expiredMinutes = 60) {
        return __awaiter(this, void 0, void 0, function* () {
            let time = RedisHelper.getExpiredTimestamp_(expiredMinutes);
            return this.redisClient_.set(key, content, "EX", time);
        });
    }
    /*
    * 根据key清除数据
    * @author gaoqiang@gagogroup.com
    * */
    clearDataWithKey(key) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.redisClient_.del(key);
        });
    }
    /*
    * 根据正则模糊读取key数据
    * @author gaoqiang@gagogroup.com
    * */
    getKeysByPattern(pattern) {
        return __awaiter(this, void 0, void 0, function* () {
            let keys = [];
            if (this.isClusterMode) {
                yield this.redisClient_.keys("elb@tpi@connect@test");
                // 集群
                // @ts-ignore
                let redisNodes = this.redisClient_.nodes();
                for (let i = 0; i < redisNodes.length; i++) {
                    let redisClient = redisNodes[i];
                    let replication = yield redisClient.info("Replication");
                    // 是否为主节点
                    let isMaster = !(replication.indexOf("role:slave") > -1);
                    if (isMaster) {
                        let ks = yield redisClient.keys(pattern);
                        keys.push(...ks);
                    }
                }
                return keys;
            }
            else {
                // 单实例
                keys = yield this.redisClient_.keys(pattern);
            }
            return keys;
        });
    }
    /*
    * 过期时间:当前时间+x分
    * @author gaoqiang@gagogroup.com
    * */
    static getExpiredTimestamp_(minute) {
        let today = new Date();
        today.setMinutes(today.getMinutes() + minute);
        return Math.floor(today.getTime() / 1000);
    }
}
exports.RedisHelper = RedisHelper;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1vbi9yZWRpc2hlbHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSxxRUFBZ0U7QUFFaEUscUNBQXdGO0FBQ3hGLGlDQUFpQztBQUNqQzs7SUFFSTtBQUNKLE1BQWEsV0FBVztJQUt0QjtRQUNFLElBQUksV0FBVyxHQUFnQix1Q0FBa0IsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNuRSxJQUFJLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQyxhQUFhLENBQUM7UUFDL0MsSUFBSSxXQUFXLENBQUMsYUFBYSxFQUFFO1lBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxpQkFBTyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3RGO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQzVDO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxXQUFXO1FBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFO1lBQzFCLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztTQUMzQztRQUNELE9BQU8sV0FBVyxDQUFDLFNBQVMsQ0FBQztJQUMvQixDQUFDO0lBRUQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFxQjtRQUN0QyxXQUFXLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUNuQyxDQUFDO0lBRUQsU0FBUztRQUNQLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMzQixDQUFDO0lBRUQ7OztRQUdJO0lBQ0UsWUFBWSxDQUFDLEdBQVc7O1lBQzVCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEMsQ0FBQztLQUFBO0lBRUM7OztRQUdJO0lBQ0EsY0FBYyxDQUFDLEdBQVcsRUFBRSxPQUFlLEVBQUUsaUJBQXlCLEVBQUUsRUFBRSxVQUFtQixLQUFLOztZQUN0RyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNaLElBQUksSUFBSSxHQUFXLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDcEUsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzthQUN4RDtpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUM1QztRQUNILENBQUM7S0FBQTtJQUVDOzs7UUFHSTtJQUNDLHFCQUFxQixDQUFDLEdBQVcsRUFBRSxPQUFlLEVBQUUsaUJBQXlCLEVBQUU7O1lBQ2xGLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsY0FBYyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQzFFLENBQUM7S0FBQTtJQUdEOzs7UUFHSTtJQUNFLGFBQWEsQ0FBQyxHQUFXOztZQUM3QixPQUFPLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0MsQ0FBQztLQUFBO0lBRUQ7OztRQUdJO0lBQ0Msb0JBQW9CLENBQUMsR0FBVyxFQUFFLE9BQWU7O1lBQ3RELE9BQU8sTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUN0RSxDQUFDO0tBQUE7SUFFRTs7O1FBR0k7SUFDQSxxQkFBcUIsQ0FBQyxHQUFXOztZQUNyQyxJQUFJLE1BQU0sR0FBVyxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdELElBQUksTUFBTSxFQUFFO2dCQUNWLE9BQU8sTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQzFCO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDO0tBQUE7SUFFRDs7O1FBR0k7SUFDRSxpQkFBaUIsQ0FBQyxHQUFXLEVBQUUsT0FBZSxFQUFFLGlCQUF5QixFQUFFOztZQUMvRSxJQUFJLElBQUksR0FBVyxXQUFXLENBQUMsb0JBQW9CLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDcEUsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN6RCxDQUFDO0tBQUE7SUFFRDs7O1FBR0k7SUFDRSxnQkFBZ0IsQ0FBQyxHQUFXOztZQUNoQyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7S0FBQTtJQUVEOzs7UUFHSTtJQUNFLGdCQUFnQixDQUFDLE9BQWU7O1lBQ3BDLElBQUksSUFBSSxHQUFhLEVBQUUsQ0FBQztZQUN4QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3RCLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDckQsS0FBSztnQkFDTCxhQUFhO2dCQUNiLElBQUksVUFBVSxHQUFZLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3BELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUMxQyxJQUFJLFdBQVcsR0FBVSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZDLElBQUksV0FBVyxHQUFXLE1BQU0sV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztvQkFDaEUsU0FBUztvQkFDVCxJQUFJLFFBQVEsR0FBWSxDQUFDLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNsRSxJQUFJLFFBQVEsRUFBRTt3QkFDWixJQUFJLEVBQUUsR0FBYSxNQUFNLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ25ELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztxQkFDbEI7aUJBQ0Y7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxNQUFNO2dCQUNOLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzlDO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDO0tBQUE7SUFFRDs7O1FBR0k7SUFDSSxNQUFNLENBQUMsb0JBQW9CLENBQUMsTUFBYztRQUNoRCxJQUFJLEtBQUssR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQzdCLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDNUMsQ0FBQztDQUVGO0FBaEpELGtDQWdKQyIsImZpbGUiOiJjb21tb24vcmVkaXNoZWxwZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FwcGxpY2F0aW9uQ29udGV4dH0gZnJvbSBcIi4uL2NvbW1vbi9hcHBsaWNhdGlvbmNvbnRleHRcIjtcbmltcG9ydCB7IFJlZGlzQ29uZmlnIH0gZnJvbSBcIi4uL3R5cGVzL2NvbmZpZy10eXBlc1wiO1xuaW1wb3J0IHsgQ2x1c3Rlck5vZGUsIENsdXN0ZXIsIENsdXN0ZXJPcHRpb25zLCBSZWRpcywgU2NhblN0cmVhbU9wdGlvbiB9IGZyb20gXCJpb3JlZGlzXCI7XG5pbXBvcnQgKiBhcyByZWRpcyBmcm9tIFwiaW9yZWRpc1wiO1xuLypcbiogcmVkaXMg6L6F5Yqp57G7XG4qICovXG5leHBvcnQgY2xhc3MgUmVkaXNIZWxwZXIge1xuICBwcml2YXRlIHN0YXRpYyBpbnN0YW5jZV86IFJlZGlzSGVscGVyO1xuICBwcml2YXRlIHJlZGlzQ2xpZW50XzogUmVkaXMgfCBDbHVzdGVyO1xuICBwcml2YXRlIGlzQ2x1c3Rlck1vZGU6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgbGV0IHJlZGlzQ29uZmlnOiBSZWRpc0NvbmZpZyA9IEFwcGxpY2F0aW9uQ29udGV4dC5nZXRSZWRpc0NvbmZpZygpO1xuICAgIHRoaXMuaXNDbHVzdGVyTW9kZSA9IHJlZGlzQ29uZmlnLmlzQ2x1c3Rlck1vZGU7XG4gICAgaWYgKHJlZGlzQ29uZmlnLmlzQ2x1c3Rlck1vZGUpIHtcbiAgICAgIHRoaXMucmVkaXNDbGllbnRfID0gbmV3IENsdXN0ZXIocmVkaXNDb25maWcuY2x1c3Rlck5vZGUsIHJlZGlzQ29uZmlnLmNsdXN0ZXJPcHRpb25zKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5yZWRpc0NsaWVudF8gPSBuZXcgcmVkaXMocmVkaXNDb25maWcpO1xuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBnZXRJbnN0YW5jZSgpOiBSZWRpc0hlbHBlciB7XG4gICAgaWYgKCFSZWRpc0hlbHBlci5pbnN0YW5jZV8pIHtcbiAgICAgIFJlZGlzSGVscGVyLmluc3RhbmNlXyA9IG5ldyBSZWRpc0hlbHBlcigpO1xuICAgIH1cbiAgICByZXR1cm4gUmVkaXNIZWxwZXIuaW5zdGFuY2VfO1xuICB9XG5cbiAgc3RhdGljIHNldEluc3RhbmNlKGluc3RhbmNlOiBSZWRpc0hlbHBlcik6IHZvaWQge1xuICAgIFJlZGlzSGVscGVyLmluc3RhbmNlXyA9IGluc3RhbmNlO1xuICB9XG5cbiAgZ2V0Q2xpZW50KCk6IFJlZGlzIHwgQ2x1c3RlciB7XG4gICAgcmV0dXJuIHRoaXMucmVkaXNDbGllbnRfO1xuICB9XG5cbiAgLypcbiAgKiDmoLnmja5rZXnor7vlj5bmlbDmja5cbiAgKiBAYXV0aG9yIGdhb3FpYW5nQGdhZ29ncm91cC5jb21cbiAgKiAqL1xuICBhc3luYyBnZXREYXRhQnlLZXkoa2V5OiBzdHJpbmcpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLnJlZGlzQ2xpZW50Xy5nZXQoa2V5KTtcbiAgfVxuXG4gICAgLypcbiAgICAqIOagueaNrmtleeS/neWtmOaVsOaNrlxuICAgICogQGF1dGhvciBnYW9xaWFuZ0BnYWdvZ3JvdXAuY29tXG4gICAgKiAqL1xuICBhc3luYyBzZXREYXRhV2l0aEtleShrZXk6IHN0cmluZywgY29udGVudDogc3RyaW5nLCBleHBpcmVkTWludXRlczogbnVtYmVyID0gMTAsIGZvcmV2ZXI6IGJvb2xlYW4gPSBmYWxzZSk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgaWYgKCFmb3JldmVyKSB7XG4gICAgICBsZXQgdGltZTogbnVtYmVyID0gUmVkaXNIZWxwZXIuZ2V0RXhwaXJlZFRpbWVzdGFtcF8oZXhwaXJlZE1pbnV0ZXMpO1xuICAgICAgcmV0dXJuIHRoaXMucmVkaXNDbGllbnRfLnNldChrZXksIGNvbnRlbnQsIFwiRVhcIiwgdGltZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0aGlzLnJlZGlzQ2xpZW50Xy5zZXQoa2V5LCBjb250ZW50KTtcbiAgICB9XG4gIH1cblxuICAgIC8qXG4gICAgKiDmoLnmja5rZXnkv53lrZjmlbDmja4g6K6+572u5pyJ5pWI5pe26Ze0ICDov4flkI5yZWRpc+WIoOmZpFxuICAgICogQGF1dGhvciBnYW9xaWFuZ0BnYWdvZ3JvdXAuY29tXG4gICAgKiAqL1xuICAgYXN5bmMgc2V0S2V5RWZmZWN0aXZpdHlEYXRlKGtleTogc3RyaW5nLCBjb250ZW50OiBzdHJpbmcsIGV4cGlyZWRNaW51dGVzOiBudW1iZXIgPSAxMCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgICByZXR1cm4gdGhpcy5yZWRpc0NsaWVudF8uc2V0KGtleSwgY29udGVudCwgXCJFWFwiLCBleHBpcmVkTWludXRlcyAqIDYwKTtcbiAgfVxuXG5cbiAgLypcbiAgKiDmoLnmja5rZXnor7vlj5bmlbDmja5cbiAgKiBAYXV0aG9yIGdhb3FpYW5nQGdhZ29ncm91cC5jb21cbiAgKiAqL1xuICBhc3luYyBsTGVuTGlzdEJ5S2V5KGtleTogc3RyaW5nKTogUHJvbWlzZTxudW1iZXI+IHtcbiAgICByZXR1cm4gYXdhaXQgdGhpcy5yZWRpc0NsaWVudF8ubGxlbihrZXkpO1xuICB9XG5cbiAgLypcbiAgKiDmoLnmja5rZXnor7vlj5bmlbDmja5cbiAgKiBAYXV0aG9yIGdhb3FpYW5nQGdhZ29ncm91cC5jb21cbiAgKiAqL1xuIGFzeW5jIHJQdXNoQnVmZmVyRGF0YUJ5S2V5KGtleTogc3RyaW5nLCBjb250ZW50OiBzdHJpbmcpOiBQcm9taXNlPG51bWJlcj4ge1xuICByZXR1cm4gYXdhaXQgdGhpcy5yZWRpc0NsaWVudF8ucnB1c2hCdWZmZXIoa2V5LCBuZXcgQnVmZmVyKGNvbnRlbnQpKTtcbiB9XG5cbiAgICAvKlxuICAgICog5qC55o2ua2V55L+d5a2Y5pWw5o2uXG4gICAgKiBAYXV0aG9yIGdhb3FpYW5nQGdhZ29ncm91cC5jb21cbiAgICAqICovXG4gIGFzeW5jIGxQb3BCdWZmZXJEYXRhV2l0aEtleShrZXk6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IGJ1ZmZlcjogQnVmZmVyID0gYXdhaXQgdGhpcy5yZWRpc0NsaWVudF8ubHBvcEJ1ZmZlcihrZXkpO1xuICAgIGlmIChidWZmZXIpIHtcbiAgICAgIHJldHVybiBidWZmZXIudG9TdHJpbmcoKTtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICAvKlxuICAqIOagueaNrmtleeabtOaWsOaVsOaNruWSjOi/h+acn+aXtumXtFxuICAqIEBhdXRob3IgZ2FvcWlhbmdAZ2Fnb2dyb3VwLmNvbVxuICAqICovXG4gIGFzeW5jIHVwZGF0ZURhdGFXaXRoS2V5KGtleTogc3RyaW5nLCBjb250ZW50OiBzdHJpbmcsIGV4cGlyZWRNaW51dGVzOiBudW1iZXIgPSA2MCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgbGV0IHRpbWU6IG51bWJlciA9IFJlZGlzSGVscGVyLmdldEV4cGlyZWRUaW1lc3RhbXBfKGV4cGlyZWRNaW51dGVzKTtcbiAgICByZXR1cm4gdGhpcy5yZWRpc0NsaWVudF8uc2V0KGtleSwgY29udGVudCwgXCJFWFwiLCB0aW1lKTtcbiAgfVxuXG4gIC8qXG4gICog5qC55o2ua2V55riF6Zmk5pWw5o2uXG4gICogQGF1dGhvciBnYW9xaWFuZ0BnYWdvZ3JvdXAuY29tXG4gICogKi9cbiAgYXN5bmMgY2xlYXJEYXRhV2l0aEtleShrZXk6IHN0cmluZyk6IFByb21pc2U8bnVtYmVyPiB7XG4gICAgcmV0dXJuIHRoaXMucmVkaXNDbGllbnRfLmRlbChrZXkpO1xuICB9XG5cbiAgLypcbiAgKiDmoLnmja7mraPliJnmqKHns4ror7vlj5ZrZXnmlbDmja5cbiAgKiBAYXV0aG9yIGdhb3FpYW5nQGdhZ29ncm91cC5jb21cbiAgKiAqL1xuICBhc3luYyBnZXRLZXlzQnlQYXR0ZXJuKHBhdHRlcm46IHN0cmluZyk6IFByb21pc2U8c3RyaW5nW10+IHtcbiAgICBsZXQga2V5czogc3RyaW5nW10gPSBbXTtcbiAgICBpZiAodGhpcy5pc0NsdXN0ZXJNb2RlKSB7XG4gICAgICBhd2FpdCB0aGlzLnJlZGlzQ2xpZW50Xy5rZXlzKFwiZWxiQHRwaUBjb25uZWN0QHRlc3RcIik7XG4gICAgICAvLyDpm4bnvqRcbiAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgIGxldCByZWRpc05vZGVzOiBSZWRpc1tdID0gdGhpcy5yZWRpc0NsaWVudF8ubm9kZXMoKTtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcmVkaXNOb2Rlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBsZXQgcmVkaXNDbGllbnQ6IFJlZGlzID0gcmVkaXNOb2Rlc1tpXTtcbiAgICAgICAgbGV0IHJlcGxpY2F0aW9uOiBzdHJpbmcgPSBhd2FpdCByZWRpc0NsaWVudC5pbmZvKFwiUmVwbGljYXRpb25cIik7XG4gICAgICAgIC8vIOaYr+WQpuS4uuS4u+iKgueCuVxuICAgICAgICBsZXQgaXNNYXN0ZXI6IGJvb2xlYW4gPSAhKHJlcGxpY2F0aW9uLmluZGV4T2YoXCJyb2xlOnNsYXZlXCIpID4gLTEpO1xuICAgICAgICBpZiAoaXNNYXN0ZXIpIHtcbiAgICAgICAgICBsZXQga3M6IHN0cmluZ1tdID0gYXdhaXQgcmVkaXNDbGllbnQua2V5cyhwYXR0ZXJuKTtcbiAgICAgICAgICBrZXlzLnB1c2goLi4ua3MpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4ga2V5cztcbiAgICB9IGVsc2Uge1xuICAgICAgLy8g5Y2V5a6e5L6LXG4gICAgICBrZXlzID0gYXdhaXQgdGhpcy5yZWRpc0NsaWVudF8ua2V5cyhwYXR0ZXJuKTtcbiAgICB9XG4gICAgcmV0dXJuIGtleXM7XG4gIH1cblxuICAvKlxuICAqIOi/h+acn+aXtumXtDrlvZPliY3ml7bpl7QreOWIhlxuICAqIEBhdXRob3IgZ2FvcWlhbmdAZ2Fnb2dyb3VwLmNvbVxuICAqICovXG4gIHByaXZhdGUgc3RhdGljIGdldEV4cGlyZWRUaW1lc3RhbXBfKG1pbnV0ZTogbnVtYmVyKTogbnVtYmVyIHtcbiAgICBsZXQgdG9kYXk6IERhdGUgPSBuZXcgRGF0ZSgpO1xuICAgIHRvZGF5LnNldE1pbnV0ZXModG9kYXkuZ2V0TWludXRlcygpICsgbWludXRlKTtcbiAgICByZXR1cm4gTWF0aC5mbG9vcih0b2RheS5nZXRUaW1lKCkgLyAxMDAwKTtcbiAgfVxuXG59XG4iXX0=
