/**
 * Describes API error.
 */
export declare class ApiError {
    domain: string;
    reason: string;
    message: string;
    constructor(reason: string, message: string);
}
