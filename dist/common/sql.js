"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SQL = void 0;
class SQL {
    getUser(offset, limit) {
        let sql = `select username, password from
        user where is_deleted = 0`;
        sql += ` limit ${offset} ,${limit}`;
        return sql;
    }
    getUserCount() {
        let sql = `select count(*) as count from  user where is_deleted = 0 `;
        return sql;
    }
    getUserHealthInformation(telephone, realName, startTime, endTime, offset, limit) {
        let sql = `select id, real_name as realName, sex, age, telephone, id_card as idCard, health_report_url as healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, origin_id as originId, created_at as createdAt from
        user_health_information where is_deleted = 0`;
        if (telephone && telephone !== "") {
            sql += ` and telephone = $telephone`;
        }
        if (realName && realName !== "") {
            sql += ` and real_name like $realName`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        sql += ` order by created_at  desc  limit ${offset} ,${limit}`;
        return sql;
    }
    getUserHealthInformationCount(telephone, realName, startTime, endTime) {
        let sql = `select count(*) as count from  user_health_information where is_deleted = 0 `;
        if (telephone && telephone !== "") {
            sql += ` and telephone = $telephone`;
        }
        if (realName && realName !== "") {
            sql += ` and real_name like $realName`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        return sql;
    }
    getHtmlPage(title, type, isOpen, startTime, endTime, offset, limit) {
        let sql = `select id, title, comment, type, is_open as isOpen, pv, image_url as imageUrl, author, creation_at as creationAt, html_script_fragment as htmlScriptFragment, page_number as pageNumber, content, weight from
        html_page where is_deleted = 0`;
        if (title && title !== "") {
            sql += ` and title like $title`;
        }
        if (type && type !== "") {
            sql += ` and type = $type`;
        }
        if (isOpen === 0 || isOpen === 1) {
            sql += ` and is_open = $isOpen`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        sql += ` order by weight  desc limit ${offset} ,${limit}`;
        return sql;
    }
    getHtmlPageCount(title, type, isOpen, startTime, endTime) {
        let sql = `select count(*) as count from html_page where is_deleted = 0`;
        if (title && title !== "") {
            sql += ` and title like $title`;
        }
        if (type && type !== "") {
            sql += ` and type = $type`;
        }
        if (isOpen === 0 || isOpen === 1) {
            sql += ` and is_open = $isOpen`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        return sql;
    }
    getPvCount() {
        let sql = `select sum(pv) as count from html_page where is_deleted = 0`;
        return sql;
    }
    getBanner(offset, limit) {
        let sql = `select id, real_name as realName from
        banner  where is_deleted = 0 `;
        sql += ` limit ${offset} ,${limit}`;
        return sql;
    }
    getBannerCount() {
        let sql = `select count(*) as count from  banner where is_deleted = 0`;
        return sql;
    }
}
exports.SQL = SQL;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1vbi9zcWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsTUFBYSxHQUFHO0lBQ1osT0FBTyxDQUFDLE1BQWMsRUFBRSxLQUFhO1FBQ2pDLElBQUksR0FBRyxHQUFXO2tDQUNRLENBQUM7UUFDM0IsR0FBRyxJQUFJLFVBQVUsTUFBTSxLQUFLLEtBQUssRUFBRSxDQUFDO1FBQ3BDLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVELFlBQVk7UUFDUixJQUFJLEdBQUcsR0FBRywyREFBMkQsQ0FBQztRQUN0RSxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxTQUFpQixFQUFFLFFBQWdCLEVBQUUsU0FBaUIsRUFBRSxPQUFlLEVBQUUsTUFBYyxFQUFFLEtBQWE7UUFDM0gsSUFBSSxHQUFHLEdBQVc7cURBQzJCLENBQUM7UUFDOUMsSUFBSSxTQUFTLElBQUksU0FBUyxLQUFLLEVBQUUsRUFBRTtZQUMvQixHQUFHLElBQUksNkJBQTZCLENBQUM7U0FDeEM7UUFDRCxJQUFJLFFBQVEsSUFBSSxRQUFRLEtBQUssRUFBRSxFQUFFO1lBQzdCLEdBQUcsSUFBSSwrQkFBK0IsQ0FBQztTQUMxQztRQUNELElBQUksU0FBUyxHQUFHLENBQUMsRUFBRTtZQUNmLEdBQUcsSUFBSSxpQ0FBaUMsQ0FBQztTQUM1QztRQUNELElBQUksT0FBTyxHQUFHLENBQUMsRUFBRTtZQUNiLEdBQUcsSUFBSSw2QkFBNkIsQ0FBQztTQUN4QztRQUNELEdBQUcsSUFBSSxxQ0FBcUMsTUFBTSxLQUFLLEtBQUssRUFBRSxDQUFDO1FBQy9ELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVELDZCQUE2QixDQUFDLFNBQWlCLEVBQUUsUUFBZ0IsRUFBRSxTQUFpQixFQUFFLE9BQWU7UUFDakcsSUFBSSxHQUFHLEdBQUcsOEVBQThFLENBQUM7UUFDekYsSUFBSSxTQUFTLElBQUksU0FBUyxLQUFLLEVBQUUsRUFBRTtZQUMvQixHQUFHLElBQUksNkJBQTZCLENBQUM7U0FDeEM7UUFDRCxJQUFJLFFBQVEsSUFBSSxRQUFRLEtBQUssRUFBRSxFQUFFO1lBQzdCLEdBQUcsSUFBSSwrQkFBK0IsQ0FBQztTQUMxQztRQUNELElBQUksU0FBUyxHQUFHLENBQUMsRUFBRTtZQUNmLEdBQUcsSUFBSSxpQ0FBaUMsQ0FBQztTQUM1QztRQUNELElBQUksT0FBTyxHQUFHLENBQUMsRUFBRTtZQUNiLEdBQUcsSUFBSSw2QkFBNkIsQ0FBQztTQUN4QztRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFhLEVBQUUsSUFBWSxFQUFFLE1BQWMsRUFBRSxTQUFpQixFQUFFLE9BQWUsRUFBRSxNQUFjLEVBQUUsS0FBYTtRQUN0SCxJQUFJLEdBQUcsR0FBVzt1Q0FDYSxDQUFDO1FBQ2hDLElBQUksS0FBSyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDdkIsR0FBRyxJQUFJLHdCQUF3QixDQUFDO1NBQ25DO1FBQ0QsSUFBSSxJQUFJLElBQUksSUFBSSxLQUFLLEVBQUUsRUFBRTtZQUNyQixHQUFHLElBQUksbUJBQW1CLENBQUM7U0FDOUI7UUFDRCxJQUFJLE1BQU0sS0FBSyxDQUFDLElBQUksTUFBTSxLQUFLLENBQUMsRUFBRTtZQUM5QixHQUFHLElBQUksd0JBQXdCLENBQUM7U0FDbkM7UUFDRCxJQUFJLFNBQVMsR0FBRyxDQUFDLEVBQUU7WUFDZixHQUFHLElBQUksaUNBQWlDLENBQUM7U0FDNUM7UUFDRCxJQUFJLE9BQU8sR0FBRyxDQUFDLEVBQUU7WUFDYixHQUFHLElBQUksNkJBQTZCLENBQUM7U0FDeEM7UUFDRCxHQUFHLElBQUksZ0NBQWdDLE1BQU0sS0FBSyxLQUFLLEVBQUUsQ0FBQztRQUMxRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxLQUFhLEVBQUUsSUFBWSxFQUFFLE1BQWMsRUFBRSxTQUFpQixFQUFFLE9BQWU7UUFDNUYsSUFBSSxHQUFHLEdBQUcsOERBQThELENBQUM7UUFDekUsSUFBSSxLQUFLLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUN2QixHQUFHLElBQUksd0JBQXdCLENBQUM7U0FDbkM7UUFDRCxJQUFJLElBQUksSUFBSSxJQUFJLEtBQUssRUFBRSxFQUFFO1lBQ3JCLEdBQUcsSUFBSSxtQkFBbUIsQ0FBQztTQUM5QjtRQUNELElBQUksTUFBTSxLQUFLLENBQUMsSUFBSSxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzlCLEdBQUcsSUFBSSx3QkFBd0IsQ0FBQztTQUNuQztRQUNELElBQUksU0FBUyxHQUFHLENBQUMsRUFBRTtZQUNmLEdBQUcsSUFBSSxpQ0FBaUMsQ0FBQztTQUM1QztRQUNELElBQUksT0FBTyxHQUFHLENBQUMsRUFBRTtZQUNiLEdBQUcsSUFBSSw2QkFBNkIsQ0FBQztTQUN4QztRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVELFVBQVU7UUFDTixJQUFJLEdBQUcsR0FBRyw2REFBNkQsQ0FBQztRQUN4RSxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCxTQUFTLENBQUMsTUFBYyxFQUFFLEtBQWE7UUFDbkMsSUFBSSxHQUFHLEdBQVc7c0NBQ1ksQ0FBQztRQUMvQixHQUFHLElBQUksVUFBVSxNQUFNLEtBQUssS0FBSyxFQUFFLENBQUM7UUFDcEMsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRUQsY0FBYztRQUNWLElBQUksR0FBRyxHQUFHLDREQUE0RCxDQUFDO1FBQ3ZFLE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztDQUNKO0FBM0dELGtCQTJHQyIsImZpbGUiOiJjb21tb24vc3FsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFNRTCB7XG4gICAgZ2V0VXNlcihvZmZzZXQ6IG51bWJlciwgbGltaXQ6IG51bWJlcikge1xuICAgICAgICBsZXQgc3FsOiBzdHJpbmcgPSBgc2VsZWN0IHVzZXJuYW1lLCBwYXNzd29yZCBmcm9tXG4gICAgICAgIHVzZXIgd2hlcmUgaXNfZGVsZXRlZCA9IDBgO1xuICAgICAgICBzcWwgKz0gYCBsaW1pdCAke29mZnNldH0gLCR7bGltaXR9YDtcbiAgICAgICAgcmV0dXJuIHNxbDtcbiAgICB9XG5cbiAgICBnZXRVc2VyQ291bnQoKSB7XG4gICAgICAgIGxldCBzcWwgPSBgc2VsZWN0IGNvdW50KCopIGFzIGNvdW50IGZyb20gIHVzZXIgd2hlcmUgaXNfZGVsZXRlZCA9IDAgYDtcbiAgICAgICAgcmV0dXJuIHNxbDtcbiAgICB9XG5cbiAgICBnZXRVc2VySGVhbHRoSW5mb3JtYXRpb24odGVsZXBob25lOiBzdHJpbmcsIHJlYWxOYW1lOiBzdHJpbmcsIHN0YXJ0VGltZTogbnVtYmVyLCBlbmRUaW1lOiBudW1iZXIsIG9mZnNldDogbnVtYmVyLCBsaW1pdDogbnVtYmVyKSB7XG4gICAgICAgIGxldCBzcWw6IHN0cmluZyA9IGBzZWxlY3QgaWQsIHJlYWxfbmFtZSBhcyByZWFsTmFtZSwgc2V4LCBhZ2UsIHRlbGVwaG9uZSwgaWRfY2FyZCBhcyBpZENhcmQsIGhlYWx0aF9yZXBvcnRfdXJsIGFzIGhlYWx0aFJlcG9ydFVybCwgaGVpZ2h0LCB3ZWlnaHQsIHdhaXN0bGluZSwgaGlwbGluZSwgc2JwLCBkYnAsIHJiZywgZnBnLCBocGcsIHVhLCB0YywgdGcsIGhkbGMsIGxkbGMsIG9yaWdpbiwgb3JpZ2luX2lkIGFzIG9yaWdpbklkLCBjcmVhdGVkX2F0IGFzIGNyZWF0ZWRBdCBmcm9tXG4gICAgICAgIHVzZXJfaGVhbHRoX2luZm9ybWF0aW9uIHdoZXJlIGlzX2RlbGV0ZWQgPSAwYDtcbiAgICAgICAgaWYgKHRlbGVwaG9uZSAmJiB0ZWxlcGhvbmUgIT09IFwiXCIpIHtcbiAgICAgICAgICAgIHNxbCArPSBgIGFuZCB0ZWxlcGhvbmUgPSAkdGVsZXBob25lYDtcbiAgICAgICAgfVxuICAgICAgICBpZiAocmVhbE5hbWUgJiYgcmVhbE5hbWUgIT09IFwiXCIpIHtcbiAgICAgICAgICAgIHNxbCArPSBgIGFuZCByZWFsX25hbWUgbGlrZSAkcmVhbE5hbWVgO1xuICAgICAgICB9XG4gICAgICAgIGlmIChzdGFydFRpbWUgPiAxKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgICBjcmVhdGVkX2F0ID49ICRzdGFydFRpbWVgO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlbmRUaW1lID4gMSkge1xuICAgICAgICAgICAgc3FsICs9IGAgYW5kIGNyZWF0ZWRfYXQgPD0gJGVuZFRpbWVgO1xuICAgICAgICB9XG4gICAgICAgIHNxbCArPSBgIG9yZGVyIGJ5IGNyZWF0ZWRfYXQgIGRlc2MgIGxpbWl0ICR7b2Zmc2V0fSAsJHtsaW1pdH1gO1xuICAgICAgICByZXR1cm4gc3FsO1xuICAgIH1cblxuICAgIGdldFVzZXJIZWFsdGhJbmZvcm1hdGlvbkNvdW50KHRlbGVwaG9uZTogc3RyaW5nLCByZWFsTmFtZTogc3RyaW5nLCBzdGFydFRpbWU6IG51bWJlciwgZW5kVGltZTogbnVtYmVyKSB7XG4gICAgICAgIGxldCBzcWwgPSBgc2VsZWN0IGNvdW50KCopIGFzIGNvdW50IGZyb20gIHVzZXJfaGVhbHRoX2luZm9ybWF0aW9uIHdoZXJlIGlzX2RlbGV0ZWQgPSAwIGA7XG4gICAgICAgIGlmICh0ZWxlcGhvbmUgJiYgdGVsZXBob25lICE9PSBcIlwiKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgdGVsZXBob25lID0gJHRlbGVwaG9uZWA7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHJlYWxOYW1lICYmIHJlYWxOYW1lICE9PSBcIlwiKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgcmVhbF9uYW1lIGxpa2UgJHJlYWxOYW1lYDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoc3RhcnRUaW1lID4gMSkge1xuICAgICAgICAgICAgc3FsICs9IGAgYW5kICAgY3JlYXRlZF9hdCA+PSAkc3RhcnRUaW1lYDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZW5kVGltZSA+IDEpIHtcbiAgICAgICAgICAgIHNxbCArPSBgIGFuZCBjcmVhdGVkX2F0IDw9ICRlbmRUaW1lYDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc3FsO1xuICAgIH1cblxuICAgIGdldEh0bWxQYWdlKHRpdGxlOiBzdHJpbmcsIHR5cGU6IHN0cmluZywgaXNPcGVuOiBudW1iZXIsIHN0YXJ0VGltZTogbnVtYmVyLCBlbmRUaW1lOiBudW1iZXIsIG9mZnNldDogbnVtYmVyLCBsaW1pdDogbnVtYmVyKSB7XG4gICAgICAgIGxldCBzcWw6IHN0cmluZyA9IGBzZWxlY3QgaWQsIHRpdGxlLCBjb21tZW50LCB0eXBlLCBpc19vcGVuIGFzIGlzT3BlbiwgcHYsIGltYWdlX3VybCBhcyBpbWFnZVVybCwgYXV0aG9yLCBjcmVhdGlvbl9hdCBhcyBjcmVhdGlvbkF0LCBodG1sX3NjcmlwdF9mcmFnbWVudCBhcyBodG1sU2NyaXB0RnJhZ21lbnQsIHBhZ2VfbnVtYmVyIGFzIHBhZ2VOdW1iZXIsIGNvbnRlbnQsIHdlaWdodCBmcm9tXG4gICAgICAgIGh0bWxfcGFnZSB3aGVyZSBpc19kZWxldGVkID0gMGA7XG4gICAgICAgIGlmICh0aXRsZSAmJiB0aXRsZSAhPT0gXCJcIikge1xuICAgICAgICAgICAgc3FsICs9IGAgYW5kIHRpdGxlIGxpa2UgJHRpdGxlYDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodHlwZSAmJiB0eXBlICE9PSBcIlwiKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgdHlwZSA9ICR0eXBlYDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaXNPcGVuID09PSAwIHx8IGlzT3BlbiA9PT0gMSkge1xuICAgICAgICAgICAgc3FsICs9IGAgYW5kIGlzX29wZW4gPSAkaXNPcGVuYDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoc3RhcnRUaW1lID4gMSkge1xuICAgICAgICAgICAgc3FsICs9IGAgYW5kICAgY3JlYXRlZF9hdCA+PSAkc3RhcnRUaW1lYDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZW5kVGltZSA+IDEpIHtcbiAgICAgICAgICAgIHNxbCArPSBgIGFuZCBjcmVhdGVkX2F0IDw9ICRlbmRUaW1lYDtcbiAgICAgICAgfVxuICAgICAgICBzcWwgKz0gYCBvcmRlciBieSB3ZWlnaHQgIGRlc2MgbGltaXQgJHtvZmZzZXR9ICwke2xpbWl0fWA7XG4gICAgICAgIHJldHVybiBzcWw7XG4gICAgfVxuXG4gICAgZ2V0SHRtbFBhZ2VDb3VudCh0aXRsZTogc3RyaW5nLCB0eXBlOiBzdHJpbmcsIGlzT3BlbjogbnVtYmVyLCBzdGFydFRpbWU6IG51bWJlciwgZW5kVGltZTogbnVtYmVyKSB7XG4gICAgICAgIGxldCBzcWwgPSBgc2VsZWN0IGNvdW50KCopIGFzIGNvdW50IGZyb20gaHRtbF9wYWdlIHdoZXJlIGlzX2RlbGV0ZWQgPSAwYDtcbiAgICAgICAgaWYgKHRpdGxlICYmIHRpdGxlICE9PSBcIlwiKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgdGl0bGUgbGlrZSAkdGl0bGVgO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0eXBlICYmIHR5cGUgIT09IFwiXCIpIHtcbiAgICAgICAgICAgIHNxbCArPSBgIGFuZCB0eXBlID0gJHR5cGVgO1xuICAgICAgICB9XG4gICAgICAgIGlmIChpc09wZW4gPT09IDAgfHwgaXNPcGVuID09PSAxKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgaXNfb3BlbiA9ICRpc09wZW5gO1xuICAgICAgICB9XG4gICAgICAgIGlmIChzdGFydFRpbWUgPiAxKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgICBjcmVhdGVkX2F0ID49ICRzdGFydFRpbWVgO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlbmRUaW1lID4gMSkge1xuICAgICAgICAgICAgc3FsICs9IGAgYW5kIGNyZWF0ZWRfYXQgPD0gJGVuZFRpbWVgO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzcWw7XG4gICAgfVxuXG4gICAgZ2V0UHZDb3VudCgpIHtcbiAgICAgICAgbGV0IHNxbCA9IGBzZWxlY3Qgc3VtKHB2KSBhcyBjb3VudCBmcm9tIGh0bWxfcGFnZSB3aGVyZSBpc19kZWxldGVkID0gMGA7XG4gICAgICAgIHJldHVybiBzcWw7XG4gICAgfVxuXG4gICAgZ2V0QmFubmVyKG9mZnNldDogbnVtYmVyLCBsaW1pdDogbnVtYmVyKSB7XG4gICAgICAgIGxldCBzcWw6IHN0cmluZyA9IGBzZWxlY3QgaWQsIHJlYWxfbmFtZSBhcyByZWFsTmFtZSBmcm9tXG4gICAgICAgIGJhbm5lciAgd2hlcmUgaXNfZGVsZXRlZCA9IDAgYDtcbiAgICAgICAgc3FsICs9IGAgbGltaXQgJHtvZmZzZXR9ICwke2xpbWl0fWA7XG4gICAgICAgIHJldHVybiBzcWw7XG4gICAgfVxuXG4gICAgZ2V0QmFubmVyQ291bnQoKSB7XG4gICAgICAgIGxldCBzcWwgPSBgc2VsZWN0IGNvdW50KCopIGFzIGNvdW50IGZyb20gIGJhbm5lciB3aGVyZSBpc19kZWxldGVkID0gMGA7XG4gICAgICAgIHJldHVybiBzcWw7XG4gICAgfVxufVxuIl19
