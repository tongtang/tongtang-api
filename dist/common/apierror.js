"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiError = void 0;
/**
 * Describes API error.
 */
class ApiError {
    constructor(reason, message) {
        this.domain = "ErrorDomain";
        this.reason = reason;
        this.message = message;
    }
}
exports.ApiError = ApiError;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1vbi9hcGllcnJvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQTs7R0FFRztBQUNILE1BQWEsUUFBUTtJQUtqQixZQUFZLE1BQWMsRUFBRSxPQUFlO1FBSjNDLFdBQU0sR0FBVyxhQUFhLENBQUM7UUFLN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7SUFDekIsQ0FBQztDQUNGO0FBVEgsNEJBU0ciLCJmaWxlIjoiY29tbW9uL2FwaWVycm9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBEZXNjcmliZXMgQVBJIGVycm9yLlxuICovXG5leHBvcnQgY2xhc3MgQXBpRXJyb3Ige1xuICAgIGRvbWFpbjogc3RyaW5nID0gXCJFcnJvckRvbWFpblwiO1xuICAgIHJlYXNvbjogc3RyaW5nO1xuICAgIG1lc3NhZ2U6IHN0cmluZztcbiAgXG4gICAgY29uc3RydWN0b3IocmVhc29uOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZykge1xuICAgICAgdGhpcy5yZWFzb24gPSByZWFzb247XG4gICAgICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xuICAgIH1cbiAgfVxuICAiXX0=
