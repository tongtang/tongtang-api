"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApplicationContext = void 0;
const fs = require("fs");
const password_1 = require("../common/password");
class ApplicationContext {
    init(env) {
        ApplicationContext.ENV = env;
        return this;
    }
    static getInstance() {
        return this.instance || (this.instance = new this());
    }
    static setInstance(app) {
        this.instance = app;
    }
    static getApplicationENV() {
        return ApplicationContext.ENV;
    }
    static getApplicationPort() {
        return ApplicationContext.PORT;
    }
    static getGagoProxy() {
        return ApplicationContext.GAGOPROXY;
    }
    /*
    * 获取微服务中的自定义配置文件
    * */
    static getMicroServiceConfig() {
        let env = this.getApplicationENV();
        return JSON.parse(fs.readFileSync(`config/${env}.json`).toString());
    }
    /*
  * 获取redis配置
  * */
    static getRedisConfig() {
        let microServiceConfig = this.getMicroServiceConfig();
        let redisConfig = microServiceConfig["database"]["redis"];
        // 集群模式配置
        if (redisConfig.isClusterMode) {
            return {
                isClusterMode: redisConfig.isClusterMode,
                clusterNode: redisConfig.clusterNode,
                clusterOptions: {
                    redisOptions: {
                        password: password_1.PasswordUtils.decipherDatabase(redisConfig.password)
                    }
                }
            };
        }
        return {
            host: redisConfig.host,
            password: password_1.PasswordUtils.decipherDatabase(redisConfig.password),
            port: redisConfig.port
        };
    }
}
exports.ApplicationContext = ApplicationContext;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1vbi9hcHBsaWNhdGlvbmNvbnRleHQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEseUJBQXlCO0FBR3pCLGlEQUFtRDtBQUNuRCxNQUFhLGtCQUFrQjtJQU83QixJQUFJLENBQUMsR0FBVztRQUNkLGtCQUFrQixDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDN0IsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsTUFBTSxDQUFDLFdBQVc7UUFDaEIsT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBdUI7UUFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7SUFDdEIsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUI7UUFDdEIsT0FBTyxrQkFBa0IsQ0FBQyxHQUFHLENBQUM7SUFDaEMsQ0FBQztJQUVELE1BQU0sQ0FBQyxrQkFBa0I7UUFDdkIsT0FBTyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQUVELE1BQU0sQ0FBQyxZQUFZO1FBQ2pCLE9BQU8sa0JBQWtCLENBQUMsU0FBUyxDQUFDO0lBQ3RDLENBQUM7SUFFRDs7UUFFSTtJQUNKLE1BQU0sQ0FBQyxxQkFBcUI7UUFDMUIsSUFBSSxHQUFHLEdBQVcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDM0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVEOztNQUVFO0lBQ0YsTUFBTSxDQUFDLGNBQWM7UUFDbkIsSUFBSSxrQkFBa0IsR0FBUSxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUMzRCxJQUFJLFdBQVcsR0FBZ0Isa0JBQWtCLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkUsU0FBUztRQUNULElBQUksV0FBVyxDQUFDLGFBQWEsRUFBRTtZQUM3QixPQUFPO2dCQUNMLGFBQWEsRUFBRSxXQUFXLENBQUMsYUFBYTtnQkFDeEMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxXQUFXO2dCQUNwQyxjQUFjLEVBQUU7b0JBQ2QsWUFBWSxFQUFFO3dCQUNaLFFBQVEsRUFBRSx3QkFBYSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUM7cUJBQy9EO2lCQUNGO2FBQ0YsQ0FBQztTQUNIO1FBQ0QsT0FBTztZQUNMLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSTtZQUN0QixRQUFRLEVBQUUsd0JBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1lBQzlELElBQUksRUFBRSxXQUFXLENBQUMsSUFBSTtTQUN2QixDQUFDO0lBQ0osQ0FBQztDQUVGO0FBakVELGdEQWlFQyIsImZpbGUiOiJjb21tb24vYXBwbGljYXRpb25jb250ZXh0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgZnMgZnJvbSBcImZzXCI7XG5pbXBvcnQgKiBhcyBwYXRoIGZyb20gXCJwYXRoXCI7XG5pbXBvcnQgeyBSZWRpc0NvbmZpZyB9IGZyb20gXCIuLi90eXBlcy9jb25maWctdHlwZXNcIjtcbmltcG9ydCB7IFBhc3N3b3JkVXRpbHMgfSBmcm9tIFwiLi4vY29tbW9uL3Bhc3N3b3JkXCI7XG5leHBvcnQgY2xhc3MgQXBwbGljYXRpb25Db250ZXh0IHtcbiAgcHJpdmF0ZSBzdGF0aWMgaW5zdGFuY2U6IEFwcGxpY2F0aW9uQ29udGV4dDtcblxuICBwcml2YXRlIHN0YXRpYyBFTlY6IHN0cmluZztcbiAgcHJpdmF0ZSBzdGF0aWMgUE9SVDogbnVtYmVyO1xuICBwcml2YXRlIHN0YXRpYyBHQUdPUFJPWFk6IHN0cmluZztcblxuICBpbml0KGVudjogc3RyaW5nKTogQXBwbGljYXRpb25Db250ZXh0IHtcbiAgICBBcHBsaWNhdGlvbkNvbnRleHQuRU5WID0gZW52O1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgc3RhdGljIGdldEluc3RhbmNlKCk6IEFwcGxpY2F0aW9uQ29udGV4dCB7XG4gICAgcmV0dXJuIHRoaXMuaW5zdGFuY2UgfHwgKHRoaXMuaW5zdGFuY2UgPSBuZXcgdGhpcygpKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRJbnN0YW5jZShhcHA6IEFwcGxpY2F0aW9uQ29udGV4dCk6IHZvaWQge1xuICAgIHRoaXMuaW5zdGFuY2UgPSBhcHA7XG4gIH1cblxuICBzdGF0aWMgZ2V0QXBwbGljYXRpb25FTlYoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gQXBwbGljYXRpb25Db250ZXh0LkVOVjtcbiAgfVxuXG4gIHN0YXRpYyBnZXRBcHBsaWNhdGlvblBvcnQoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gQXBwbGljYXRpb25Db250ZXh0LlBPUlQ7XG4gIH1cblxuICBzdGF0aWMgZ2V0R2Fnb1Byb3h5KCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIEFwcGxpY2F0aW9uQ29udGV4dC5HQUdPUFJPWFk7XG4gIH1cblxuICAvKlxuICAqIOiOt+WPluW+ruacjeWKoeS4reeahOiHquWumuS5iemFjee9ruaWh+S7tlxuICAqICovXG4gIHN0YXRpYyBnZXRNaWNyb1NlcnZpY2VDb25maWcoKTogYW55IHtcbiAgICBsZXQgZW52OiBzdHJpbmcgPSB0aGlzLmdldEFwcGxpY2F0aW9uRU5WKCk7XG4gICAgcmV0dXJuIEpTT04ucGFyc2UoZnMucmVhZEZpbGVTeW5jKGBjb25maWcvJHtlbnZ9Lmpzb25gKS50b1N0cmluZygpKTtcbiAgfVxuXG4gIC8qXG4qIOiOt+WPlnJlZGlz6YWN572uXG4qICovXG4gIHN0YXRpYyBnZXRSZWRpc0NvbmZpZygpOiBSZWRpc0NvbmZpZyB7XG4gICAgbGV0IG1pY3JvU2VydmljZUNvbmZpZzogYW55ID0gdGhpcy5nZXRNaWNyb1NlcnZpY2VDb25maWcoKTtcbiAgICBsZXQgcmVkaXNDb25maWc6IFJlZGlzQ29uZmlnID0gbWljcm9TZXJ2aWNlQ29uZmlnW1wiZGF0YWJhc2VcIl1bXCJyZWRpc1wiXTtcbiAgICAvLyDpm4bnvqTmqKHlvI/phY3nva5cbiAgICBpZiAocmVkaXNDb25maWcuaXNDbHVzdGVyTW9kZSkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgaXNDbHVzdGVyTW9kZTogcmVkaXNDb25maWcuaXNDbHVzdGVyTW9kZSxcbiAgICAgICAgY2x1c3Rlck5vZGU6IHJlZGlzQ29uZmlnLmNsdXN0ZXJOb2RlLFxuICAgICAgICBjbHVzdGVyT3B0aW9uczoge1xuICAgICAgICAgIHJlZGlzT3B0aW9uczoge1xuICAgICAgICAgICAgcGFzc3dvcmQ6IFBhc3N3b3JkVXRpbHMuZGVjaXBoZXJEYXRhYmFzZShyZWRpc0NvbmZpZy5wYXNzd29yZClcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfVxuICAgIHJldHVybiB7XG4gICAgICBob3N0OiByZWRpc0NvbmZpZy5ob3N0LFxuICAgICAgcGFzc3dvcmQ6IFBhc3N3b3JkVXRpbHMuZGVjaXBoZXJEYXRhYmFzZShyZWRpc0NvbmZpZy5wYXNzd29yZCksXG4gICAgICBwb3J0OiByZWRpc0NvbmZpZy5wb3J0XG4gICAgfTtcbiAgfVxuXG59XG5cblxuIl19
