import { ApiError } from "./apierror";
/**
 * Utility to validate input parameters and used by Controller.
 */
export declare class Validator {
    errors: Array<ApiError>;
    /**
     * Returns true if encounter errors through routine.
     * @returns {boolean} true if encounter errors through routine.
     */
    hasErrors(): boolean;
    /**
     * Casts any type to number and record error if result is not a number.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {number} Result, should be number type if success.
     */
    toNumber(param: any, reason?: string): number;
    /**
     * turn array string into number array
     * @param {*} param number array as string format like: "[1,2,3]"
     * @param {string} [reason="param invalid"]
     * @returns {number}
     *
     * @memberOf Validator
     */
    toNumberArray(param: string, reason?: string): number[];
    /**
     * Casts any type to string.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {String} Result, should be string type if success.
     */
    toStr(param: any, reason?: string): string;
    /**
     * Casts any type to date.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {String} Result, should be date type if success.
     */
    toDate(param: any, reason?: string): Date;
    /**
     * Casts any type to unix timestamp.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {String} Result, should be number(int) if success.
     */
    toUnixTimestamp(param: any, reason?: string): number;
    /**
     * Casts any type to boolean.
     */
    toBoolean(param: any, reason?: string): boolean;
    /**
     * Asserts condition is true, otherwise it will records the error.
     * @param cond Condition.
     * @param reason Error reason.
     */
    assert(cond: boolean, reason: string): void;
}
