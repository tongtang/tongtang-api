"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Validator = void 0;
const apierror_1 = require("./apierror");
const util_1 = require("util");
const moment = require("moment");
/**
 * Utility to validate input parameters and used by Controller.
 */
class Validator {
    constructor() {
        this.errors = [];
    }
    /**
     * Returns true if encounter errors through routine.
     * @returns {boolean} true if encounter errors through routine.
     */
    hasErrors() {
        return (this.errors.length > 0);
    }
    /**
     * Casts any type to number and record error if result is not a number.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {number} Result, should be number type if success.
     */
    toNumber(param, reason = "param invalid") {
        let result = Number(param);
        if (isNaN(result)) {
            this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
        }
        return result;
    }
    /**
     * turn array string into number array
     * @param {*} param number array as string format like: "[1,2,3]"
     * @param {string} [reason="param invalid"]
     * @returns {number}
     *
     * @memberOf Validator
     */
    toNumberArray(param, reason = "param invalid") {
        if (param && param.indexOf("[") === 0 && param.lastIndexOf("]") === param.length - 1) {
            let strArr = param.substring(1, param.length - 1).split(",");
            let numArr = [];
            for (let str of strArr) {
                let num = Number(str);
                if (str !== "" && !isNaN(num)) {
                    numArr.push(num);
                }
                else {
                    this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
                    break;
                }
            }
            return numArr;
        }
        else {
            this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
        }
    }
    /**
     * Casts any type to string.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {String} Result, should be string type if success.
     */
    toStr(param, reason = "param invalid") {
        if (param === undefined) {
            this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
        }
        return String(param);
    }
    /**
     * Casts any type to date.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {String} Result, should be date type if success.
     */
    toDate(param, reason = "param invalid") {
        let dateStr = param.toString();
        let date = new Date(dateStr);
        if (!date || date.toString() === "Invalid Date") {
            this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
            return;
        }
        let strArr = param.substring(0, dateStr.length).split("-");
        for (let str of strArr) {
            if (isNaN(Number(str)) || Number(str) === 0) {
                this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
                return;
            }
        }
        let yearString = "";
        let monthString = "";
        let dateString = "";
        if (strArr.length === 1) {
            yearString = strArr[0];
            monthString = "01";
            dateString = "01";
        }
        else if (strArr.length === 2) {
            yearString = strArr[0];
            monthString = (Number(strArr[1]) > 9) ? strArr[1] : `0${Number(strArr[1])}`;
            dateString = "01";
        }
        else if (strArr.length === 3) {
            yearString = strArr[0];
            monthString = (Number(strArr[1]) > 9) ? strArr[1] : `0${Number(strArr[1])}`;
            dateString = (Number(strArr[2]) > 9) ? strArr[2] : `0${Number(strArr[2])}`;
        }
        let dataFormatString = `${yearString}-${monthString}-${dateString}T00:00:00+00`;
        let mo = moment(dataFormatString);
        let timestamp = Number(mo.format("X"));
        return new Date(timestamp * 1000);
    }
    /**
     * Casts any type to unix timestamp.
     * @param param Any type of input value.
     * @param reason Error reason.
     * @returns {String} Result, should be number(int) if success.
     */
    toUnixTimestamp(param, reason = "param invalid") {
        let result = new Date(param);
        if (!result || isNaN(result)) {
            this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
        }
        result = Math.floor(result.getTime() / 1000);
        return result;
    }
    /**
     * Casts any type to boolean.
     */
    toBoolean(param, reason = "param invalid") {
        if (util_1.isBoolean(param)) {
            return Boolean(param);
        }
        else {
            this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
            return param;
        }
    }
    /**
     * Asserts condition is true, otherwise it will records the error.
     * @param cond Condition.
     * @param reason Error reason.
     */
    assert(cond, reason) {
        if (!cond) {
            this.errors.push(new apierror_1.ApiError(reason, "Bad Request"));
        }
    }
}
exports.Validator = Validator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbW1vbi92YWxpZGF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEseUNBQW9DO0FBQ3BDLCtCQUErQjtBQUMvQixpQ0FBaUM7QUFHakM7O0dBRUc7QUFDSCxNQUFhLFNBQVM7SUFBdEI7UUFDRSxXQUFNLEdBQW9CLEVBQUUsQ0FBQztJQWlKL0IsQ0FBQztJQS9JQzs7O09BR0c7SUFDSCxTQUFTO1FBQ1AsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILFFBQVEsQ0FBQyxLQUFVLEVBQUUsU0FBaUIsZUFBZTtRQUNuRCxJQUFJLE1BQU0sR0FBUSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDakIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxtQkFBUSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1NBQ3ZEO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSCxhQUFhLENBQUMsS0FBYSxFQUFFLFNBQWlCLGVBQWU7UUFDM0QsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNwRixJQUFJLE1BQU0sR0FBYyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN4RSxJQUFJLE1BQU0sR0FBYyxFQUFFLENBQUM7WUFDM0IsS0FBSyxJQUFJLEdBQUcsSUFBSSxNQUFNLEVBQUU7Z0JBQ3RCLElBQUksR0FBRyxHQUFXLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUNsQjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLG1CQUFRLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7b0JBQ3RELE1BQU07aUJBQ1A7YUFDRjtZQUNELE9BQU8sTUFBTSxDQUFDO1NBQ2Y7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksbUJBQVEsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztTQUN2RDtJQUNILENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILEtBQUssQ0FBQyxLQUFVLEVBQUUsU0FBaUIsZUFBZTtRQUNoRCxJQUFJLEtBQUssS0FBSyxTQUFTLEVBQUU7WUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxtQkFBUSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1NBQ3ZEO1FBQ0QsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsTUFBTSxDQUFDLEtBQVUsRUFBRSxTQUFpQixlQUFlO1FBQ2pELElBQUksT0FBTyxHQUFXLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN2QyxJQUFJLElBQUksR0FBUSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVsQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUUsS0FBSyxjQUFjLEVBQUU7WUFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxtQkFBUSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ3RELE9BQU87U0FDUjtRQUNELElBQUksTUFBTSxHQUFjLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEUsS0FBSyxJQUFJLEdBQUcsSUFBSSxNQUFNLEVBQUU7WUFDdEIsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxtQkFBUSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDO2dCQUN0RCxPQUFPO2FBQ1I7U0FDRjtRQUNELElBQUksVUFBVSxHQUFXLEVBQUUsQ0FBQztRQUM1QixJQUFJLFdBQVcsR0FBVyxFQUFFLENBQUM7UUFDN0IsSUFBSSxVQUFVLEdBQVcsRUFBRSxDQUFDO1FBQzVCLElBQUksTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdkIsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QixXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQ25CLFVBQVUsR0FBRyxJQUFJLENBQUM7U0FDbkI7YUFBSyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzdCLFVBQVUsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsV0FBVyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDNUUsVUFBVSxHQUFHLElBQUksQ0FBQztTQUNuQjthQUFLLElBQUksTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDN0IsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2QixXQUFXLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUM1RSxVQUFVLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUM1RTtRQUVILElBQUksZ0JBQWdCLEdBQVcsR0FBRyxVQUFVLElBQUksV0FBVyxJQUFJLFVBQVUsY0FBYyxDQUFDO1FBQ3hGLElBQUksRUFBRSxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2xDLElBQUksU0FBUyxHQUFXLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDL0MsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsZUFBZSxDQUFDLEtBQVUsRUFBRSxTQUFpQixlQUFlO1FBQzFELElBQUksTUFBTSxHQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksbUJBQVEsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQztTQUN2RDtRQUNELE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUM3QyxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxTQUFTLENBQUMsS0FBVSxFQUFFLFNBQWlCLGVBQWU7UUFDcEQsSUFBSSxnQkFBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLE9BQU8sT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZCO2FBQU07WUFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLG1CQUFRLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDdEQsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsTUFBTSxDQUFDLElBQWEsRUFBRSxNQUFjO1FBQ2xDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDVCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLG1CQUFRLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDdkQ7SUFDSCxDQUFDO0NBQ0Y7QUFsSkQsOEJBa0pDIiwiZmlsZSI6ImNvbW1vbi92YWxpZGF0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FwaUVycm9yfSBmcm9tIFwiLi9hcGllcnJvclwiO1xuaW1wb3J0IHtpc0Jvb2xlYW59IGZyb20gXCJ1dGlsXCI7XG5pbXBvcnQgKiBhcyBtb21lbnQgZnJvbSBcIm1vbWVudFwiO1xuXG5cbi8qKlxuICogVXRpbGl0eSB0byB2YWxpZGF0ZSBpbnB1dCBwYXJhbWV0ZXJzIGFuZCB1c2VkIGJ5IENvbnRyb2xsZXIuXG4gKi9cbmV4cG9ydCBjbGFzcyBWYWxpZGF0b3Ige1xuICBlcnJvcnM6IEFycmF5PEFwaUVycm9yPiA9IFtdO1xuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRydWUgaWYgZW5jb3VudGVyIGVycm9ycyB0aHJvdWdoIHJvdXRpbmUuXG4gICAqIEByZXR1cm5zIHtib29sZWFufSB0cnVlIGlmIGVuY291bnRlciBlcnJvcnMgdGhyb3VnaCByb3V0aW5lLlxuICAgKi9cbiAgaGFzRXJyb3JzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAodGhpcy5lcnJvcnMubGVuZ3RoID4gMCk7XG4gIH1cblxuICAvKipcbiAgICogQ2FzdHMgYW55IHR5cGUgdG8gbnVtYmVyIGFuZCByZWNvcmQgZXJyb3IgaWYgcmVzdWx0IGlzIG5vdCBhIG51bWJlci5cbiAgICogQHBhcmFtIHBhcmFtIEFueSB0eXBlIG9mIGlucHV0IHZhbHVlLlxuICAgKiBAcGFyYW0gcmVhc29uIEVycm9yIHJlYXNvbi5cbiAgICogQHJldHVybnMge251bWJlcn0gUmVzdWx0LCBzaG91bGQgYmUgbnVtYmVyIHR5cGUgaWYgc3VjY2Vzcy5cbiAgICovXG4gIHRvTnVtYmVyKHBhcmFtOiBhbnksIHJlYXNvbjogc3RyaW5nID0gXCJwYXJhbSBpbnZhbGlkXCIpOiBudW1iZXIge1xuICAgIGxldCByZXN1bHQ6IGFueSA9IE51bWJlcihwYXJhbSk7XG4gICAgaWYgKGlzTmFOKHJlc3VsdCkpIHtcbiAgICAgIHRoaXMuZXJyb3JzLnB1c2gobmV3IEFwaUVycm9yKHJlYXNvbiwgXCJCYWQgUmVxdWVzdFwiKSk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cblxuICAvKipcbiAgICogdHVybiBhcnJheSBzdHJpbmcgaW50byBudW1iZXIgYXJyYXlcbiAgICogQHBhcmFtIHsqfSBwYXJhbSBudW1iZXIgYXJyYXkgYXMgc3RyaW5nIGZvcm1hdCBsaWtlOiBcIlsxLDIsM11cIlxuICAgKiBAcGFyYW0ge3N0cmluZ30gW3JlYXNvbj1cInBhcmFtIGludmFsaWRcIl1cbiAgICogQHJldHVybnMge251bWJlcn1cbiAgICpcbiAgICogQG1lbWJlck9mIFZhbGlkYXRvclxuICAgKi9cbiAgdG9OdW1iZXJBcnJheShwYXJhbTogc3RyaW5nLCByZWFzb246IHN0cmluZyA9IFwicGFyYW0gaW52YWxpZFwiKTogbnVtYmVyW10ge1xuICAgIGlmIChwYXJhbSAmJiBwYXJhbS5pbmRleE9mKFwiW1wiKSA9PT0gMCAmJiBwYXJhbS5sYXN0SW5kZXhPZihcIl1cIikgPT09IHBhcmFtLmxlbmd0aCAtIDEpIHtcbiAgICAgIGxldCBzdHJBcnI6IHN0cmluZyBbXSA9IHBhcmFtLnN1YnN0cmluZygxLCBwYXJhbS5sZW5ndGggLSAxKS5zcGxpdChcIixcIik7XG4gICAgICBsZXQgbnVtQXJyOiBudW1iZXIgW10gPSBbXTtcbiAgICAgIGZvciAobGV0IHN0ciBvZiBzdHJBcnIpIHtcbiAgICAgICAgbGV0IG51bTogbnVtYmVyID0gTnVtYmVyKHN0cik7XG4gICAgICAgIGlmIChzdHIgIT09IFwiXCIgJiYgIWlzTmFOKG51bSkpIHtcbiAgICAgICAgICBudW1BcnIucHVzaChudW0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuZXJyb3JzLnB1c2gobmV3IEFwaUVycm9yKHJlYXNvbiwgXCJCYWQgUmVxdWVzdFwiKSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBudW1BcnI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZXJyb3JzLnB1c2gobmV3IEFwaUVycm9yKHJlYXNvbiwgXCJCYWQgUmVxdWVzdFwiKSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENhc3RzIGFueSB0eXBlIHRvIHN0cmluZy5cbiAgICogQHBhcmFtIHBhcmFtIEFueSB0eXBlIG9mIGlucHV0IHZhbHVlLlxuICAgKiBAcGFyYW0gcmVhc29uIEVycm9yIHJlYXNvbi5cbiAgICogQHJldHVybnMge1N0cmluZ30gUmVzdWx0LCBzaG91bGQgYmUgc3RyaW5nIHR5cGUgaWYgc3VjY2Vzcy5cbiAgICovXG4gIHRvU3RyKHBhcmFtOiBhbnksIHJlYXNvbjogc3RyaW5nID0gXCJwYXJhbSBpbnZhbGlkXCIpOiBzdHJpbmcge1xuICAgIGlmIChwYXJhbSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLmVycm9ycy5wdXNoKG5ldyBBcGlFcnJvcihyZWFzb24sIFwiQmFkIFJlcXVlc3RcIikpO1xuICAgIH1cbiAgICByZXR1cm4gU3RyaW5nKHBhcmFtKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYXN0cyBhbnkgdHlwZSB0byBkYXRlLlxuICAgKiBAcGFyYW0gcGFyYW0gQW55IHR5cGUgb2YgaW5wdXQgdmFsdWUuXG4gICAqIEBwYXJhbSByZWFzb24gRXJyb3IgcmVhc29uLlxuICAgKiBAcmV0dXJucyB7U3RyaW5nfSBSZXN1bHQsIHNob3VsZCBiZSBkYXRlIHR5cGUgaWYgc3VjY2Vzcy5cbiAgICovXG4gIHRvRGF0ZShwYXJhbTogYW55LCByZWFzb246IHN0cmluZyA9IFwicGFyYW0gaW52YWxpZFwiKTogRGF0ZSB7XG4gICAgbGV0IGRhdGVTdHI6IHN0cmluZyA9IHBhcmFtLnRvU3RyaW5nKCk7XG4gICAgbGV0IGRhdGU6IGFueSA9IG5ldyBEYXRlKGRhdGVTdHIpO1xuXG4gICAgaWYgKCFkYXRlIHx8IGRhdGUudG9TdHJpbmcoKSA9PT0gXCJJbnZhbGlkIERhdGVcIikge1xuICAgICAgdGhpcy5lcnJvcnMucHVzaChuZXcgQXBpRXJyb3IocmVhc29uLCBcIkJhZCBSZXF1ZXN0XCIpKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgbGV0IHN0ckFycjogc3RyaW5nIFtdID0gcGFyYW0uc3Vic3RyaW5nKDAsIGRhdGVTdHIubGVuZ3RoKS5zcGxpdChcIi1cIik7XG4gICAgICBmb3IgKGxldCBzdHIgb2Ygc3RyQXJyKSB7XG4gICAgICAgIGlmIChpc05hTihOdW1iZXIoc3RyKSkgfHwgTnVtYmVyKHN0cikgPT09IDApIHtcbiAgICAgICAgICB0aGlzLmVycm9ycy5wdXNoKG5ldyBBcGlFcnJvcihyZWFzb24sIFwiQmFkIFJlcXVlc3RcIikpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgbGV0IHllYXJTdHJpbmc6IHN0cmluZyA9IFwiXCI7XG4gICAgICBsZXQgbW9udGhTdHJpbmc6IHN0cmluZyA9IFwiXCI7XG4gICAgICBsZXQgZGF0ZVN0cmluZzogc3RyaW5nID0gXCJcIjtcbiAgICAgIGlmIChzdHJBcnIubGVuZ3RoID09PSAxKSB7XG4gICAgICAgIHllYXJTdHJpbmcgPSBzdHJBcnJbMF07XG4gICAgICAgIG1vbnRoU3RyaW5nID0gXCIwMVwiO1xuICAgICAgICBkYXRlU3RyaW5nID0gXCIwMVwiO1xuICAgICAgfWVsc2UgaWYgKHN0ckFyci5sZW5ndGggPT09IDIpIHtcbiAgICAgICAgeWVhclN0cmluZyA9IHN0ckFyclswXTtcbiAgICAgICAgbW9udGhTdHJpbmcgPSAoTnVtYmVyKHN0ckFyclsxXSkgPiA5KSA/IHN0ckFyclsxXSA6IGAwJHtOdW1iZXIoc3RyQXJyWzFdKX1gO1xuICAgICAgICBkYXRlU3RyaW5nID0gXCIwMVwiO1xuICAgICAgfWVsc2UgaWYgKHN0ckFyci5sZW5ndGggPT09IDMpIHtcbiAgICAgICAgeWVhclN0cmluZyA9IHN0ckFyclswXTtcbiAgICAgICAgbW9udGhTdHJpbmcgPSAoTnVtYmVyKHN0ckFyclsxXSkgPiA5KSA/IHN0ckFyclsxXSA6IGAwJHtOdW1iZXIoc3RyQXJyWzFdKX1gO1xuICAgICAgICBkYXRlU3RyaW5nID0gKE51bWJlcihzdHJBcnJbMl0pID4gOSkgPyBzdHJBcnJbMl0gOiBgMCR7TnVtYmVyKHN0ckFyclsyXSl9YDtcbiAgICAgIH1cblxuICAgIGxldCBkYXRhRm9ybWF0U3RyaW5nOiBzdHJpbmcgPSBgJHt5ZWFyU3RyaW5nfS0ke21vbnRoU3RyaW5nfS0ke2RhdGVTdHJpbmd9VDAwOjAwOjAwKzAwYDtcbiAgICBsZXQgbW8gPSBtb21lbnQoZGF0YUZvcm1hdFN0cmluZyk7XG4gICAgbGV0IHRpbWVzdGFtcDogbnVtYmVyID0gTnVtYmVyKG1vLmZvcm1hdChcIlhcIikpO1xuICAgIHJldHVybiBuZXcgRGF0ZSh0aW1lc3RhbXAgKiAxMDAwKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYXN0cyBhbnkgdHlwZSB0byB1bml4IHRpbWVzdGFtcC5cbiAgICogQHBhcmFtIHBhcmFtIEFueSB0eXBlIG9mIGlucHV0IHZhbHVlLlxuICAgKiBAcGFyYW0gcmVhc29uIEVycm9yIHJlYXNvbi5cbiAgICogQHJldHVybnMge1N0cmluZ30gUmVzdWx0LCBzaG91bGQgYmUgbnVtYmVyKGludCkgaWYgc3VjY2Vzcy5cbiAgICovXG4gIHRvVW5peFRpbWVzdGFtcChwYXJhbTogYW55LCByZWFzb246IHN0cmluZyA9IFwicGFyYW0gaW52YWxpZFwiKTogbnVtYmVyIHtcbiAgICBsZXQgcmVzdWx0OiBhbnkgPSBuZXcgRGF0ZShwYXJhbSk7XG4gICAgaWYgKCFyZXN1bHQgfHwgaXNOYU4ocmVzdWx0KSkge1xuICAgICAgdGhpcy5lcnJvcnMucHVzaChuZXcgQXBpRXJyb3IocmVhc29uLCBcIkJhZCBSZXF1ZXN0XCIpKTtcbiAgICB9XG4gICAgcmVzdWx0ID0gTWF0aC5mbG9vcihyZXN1bHQuZ2V0VGltZSgpIC8gMTAwMCk7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYXN0cyBhbnkgdHlwZSB0byBib29sZWFuLlxuICAgKi9cbiAgdG9Cb29sZWFuKHBhcmFtOiBhbnksIHJlYXNvbjogc3RyaW5nID0gXCJwYXJhbSBpbnZhbGlkXCIpOiBib29sZWFuIHtcbiAgICBpZiAoaXNCb29sZWFuKHBhcmFtKSkge1xuICAgICAgcmV0dXJuIEJvb2xlYW4ocGFyYW0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmVycm9ycy5wdXNoKG5ldyBBcGlFcnJvcihyZWFzb24sIFwiQmFkIFJlcXVlc3RcIikpO1xuICAgICAgcmV0dXJuIHBhcmFtO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBBc3NlcnRzIGNvbmRpdGlvbiBpcyB0cnVlLCBvdGhlcndpc2UgaXQgd2lsbCByZWNvcmRzIHRoZSBlcnJvci5cbiAgICogQHBhcmFtIGNvbmQgQ29uZGl0aW9uLlxuICAgKiBAcGFyYW0gcmVhc29uIEVycm9yIHJlYXNvbi5cbiAgICovXG4gIGFzc2VydChjb25kOiBib29sZWFuLCByZWFzb246IHN0cmluZyk6IHZvaWQge1xuICAgIGlmICghY29uZCkge1xuICAgICAgdGhpcy5lcnJvcnMucHVzaChuZXcgQXBpRXJyb3IocmVhc29uLCBcIkJhZCBSZXF1ZXN0XCIpKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==
