export declare class SQL {
    getUser(offset: number, limit: number): string;
    getUserCount(): string;
    getUserHealthInformation(telephone: string, realName: string, startTime: number, endTime: number, offset: number, limit: number): string;
    getUserHealthInformationCount(telephone: string, realName: string, startTime: number, endTime: number): string;
    getHtmlPage(title: string, type: string, isOpen: number, startTime: number, endTime: number, offset: number, limit: number): string;
    getHtmlPageCount(title: string, type: string, isOpen: number, startTime: number, endTime: number): string;
    getPvCount(): string;
    getBanner(offset: number, limit: number): string;
    getBannerCount(): string;
}
