import { Cluster, Redis } from "ioredis";
export declare class RedisHelper {
    private static instance_;
    private redisClient_;
    private isClusterMode;
    constructor();
    static getInstance(): RedisHelper;
    static setInstance(instance: RedisHelper): void;
    getClient(): Redis | Cluster;
    getDataByKey(key: string): Promise<string>;
    setDataWithKey(key: string, content: string, expiredMinutes?: number, forever?: boolean): Promise<string>;
    setKeyEffectivityDate(key: string, content: string, expiredMinutes?: number): Promise<string>;
    lLenListByKey(key: string): Promise<number>;
    rPushBufferDataByKey(key: string, content: string): Promise<number>;
    lPopBufferDataWithKey(key: string): Promise<string>;
    updateDataWithKey(key: string, content: string, expiredMinutes?: number): Promise<string>;
    clearDataWithKey(key: string): Promise<number>;
    getKeysByPattern(pattern: string): Promise<string[]>;
    private static getExpiredTimestamp_;
}
