export declare class RandomUtils {
    static generateToken(): string;
    static randomNum(size: number): string;
    static randomStr(size: number): string;
    static getUuid(): string;
    static getShortId(): string;
    static randomWord(randomFlag: boolean, min: number, max?: number): string;
}
