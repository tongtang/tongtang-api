"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlPageService = void 0;
const sql_1 = require("../common/sql");
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny = global;
class HtmlPageService {
    constructor(obj = null) {
        if (obj)
            this.data = _.clone(obj);
        this.model = sequelize.models.htmlpage;
    }
    findOneById(id) {
        return this.model.findOne({
            attributes: ["id", "title", "type", "pv", ["image_url", "imageUrl"], "author", "content", ["creation_at", "creationAt"], ["html_script_fragment", "htmlScriptFragment"], ["page_number", "pageNumber"], ["is_open", "isOpen"]],
            where: { id: id }
        });
    }
    findPCOneById(id) {
        return this.model.findOne({
            attributes: ["id", "title", "type", "pv", ["image_url", "imageUrl"], "author", "content", ["creation_at", "creationAt"], ["html_script_fragment", "htmlScriptFragment"], ["page_number", "pageNumber"], ["is_open", "isOpen"]],
            where: { id: id }
        });
    }
    findOneByPageNumber(pageNumer) {
        return this.model.findOne({
            attributes: ["id", "title", "type", "pv", ["image_url", "imageUrl"], "author", "content", ["creation_at", "creationAt"], ["html_script_fragment", "htmlScriptFragment"], ["page_number", "pageNumber"], ["is_open", "isOpen"]],
            where: { pageNumer: pageNumer }
        });
    }
    findAll(type) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.model.find({
                attributes: ["id", "title", "type", "pv", ["image_url", "imageUrl"], "author", "content", ["creation_at", "creationAt"], ["html_script_fragment", "htmlScriptFragment"], ["page_number", "pageNumber"], ["is_open", "isOpen"]],
                where: { isDeleted: 0, isOpen: 1, type: type }
            });
        });
    }
    findPCAll(pageIndex, pageSize) {
        return __awaiter(this, void 0, void 0, function* () {
            const sql = new sql_1.SQL();
            let htmlpages = [];
            let count = 0;
            let limit = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
            let offset = limit * (Number(pageIndex) - 1);
            count = (yield globalAny.db.query(sql.getHtmlPageCount(), { type: sequelize.QueryTypes.SELECT, bind: {} }))[0].count;
            htmlpages = yield globalAny.db.query(sql.getHtmlPage(offset, limit), { type: sequelize.QueryTypes.SELECT, bind: {} });
            return { count: count, rows: htmlpages };
        });
    }
    create(imageUrl, title, type, author, content, creationAt, htmlScriptFragment, pageNumber) {
        return this.model.create({
            title: title,
            type: type,
            imageUrl: imageUrl,
            author: author,
            content: content,
            creationAt: creationAt,
            htmlScriptFragment: htmlScriptFragment,
            pageNumber: pageNumber
        });
    }
    updateById(id, title, type, imageUrl, author, content, creationAt, htmlScriptFragment, pageNumber) {
        return this.model.update({
            title: title,
            type: type,
            imageUrl: imageUrl,
            author: author,
            content: content,
            creationAt: creationAt,
            htmlScriptFragment: htmlScriptFragment,
            pageNumber: pageNumber
        }, { where: { id: id } });
    }
    addPV(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `update html_page set pv = pv + 1 where id = $id`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
        });
    }
    isOpen(id, isOpen) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `update html_page set is_open = $isOpen where id = $id`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id, isOpen: isOpen } });
        });
    }
    deleteById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `update html_page set is_deleted = 1 where id = $id`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
        });
    }
    getHtmlPageCodeList() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                { code: "HTMLTYPE001", name: "母婴健康" },
                { code: "HTMLTYPE002", name: "就医问药" },
                { code: "HTMLTYPE003", name: "饮食营养" },
                { code: "HTMLTYPE004", name: "睡眠管理" },
                { code: "HTMLTYPE005", name: "减重健身" },
                { code: "HTMLTYPE006", name: "科技前沿" }
            ];
        });
    }
}
exports.HtmlPageService = HtmlPageService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UvaHRtbHBhZ2VzZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLHVDQUFvQztBQUNwQyxNQUFNLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDdkMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzFCLE1BQU0sU0FBUyxHQUFRLE1BQU0sQ0FBQztBQUU5QixNQUFhLGVBQWU7SUFJeEIsWUFBWSxNQUFXLElBQUk7UUFDdkIsSUFBSSxHQUFHO1lBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDM0MsQ0FBQztJQUVELFdBQVcsQ0FBQyxFQUFVO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDdEIsVUFBVSxFQUFFLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzlOLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUU7U0FDcEIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGFBQWEsQ0FBQyxFQUFVO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDdEIsVUFBVSxFQUFFLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzlOLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUU7U0FDcEIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1CQUFtQixDQUFDLFNBQWlCO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDdEIsVUFBVSxFQUFFLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzlOLEtBQUssRUFBRSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUU7U0FDbEMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVLLE9BQU8sQ0FBQyxJQUFZOztZQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNuQixVQUFVLEVBQUUsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsRUFBRSxDQUFDLHNCQUFzQixFQUFFLG9CQUFvQixDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQzlOLEtBQUssRUFBRSxFQUFFLFNBQVMsRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFDO2FBQ2hELENBQUMsQ0FBQztRQUNQLENBQUM7S0FBQTtJQUVLLFNBQVMsQ0FBQyxTQUFpQixFQUFFLFFBQWdCOztZQUMvQyxNQUFNLEdBQUcsR0FBRyxJQUFJLFNBQUcsRUFBRSxDQUFDO1lBQ3RCLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUNuQixJQUFJLEtBQUssR0FBVyxDQUFDLENBQUM7WUFDdEIsSUFBSSxLQUFLLEdBQVcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDOUYsSUFBSSxNQUFNLEdBQVcsS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3JELEtBQUssR0FBRyxDQUFDLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEVBQ3BHLEVBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ2QsU0FBUyxHQUFHLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEVBQy9HLEVBQUMsQ0FBQyxDQUFDO1lBQ0osT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDO1FBQzdDLENBQUM7S0FBQTtJQUVELE1BQU0sQ0FBQyxRQUFnQixFQUFFLEtBQWEsRUFBRSxJQUFZLEVBQUUsTUFBYyxFQUFFLE9BQWUsRUFBRSxVQUFrQixFQUFFLGtCQUEwQixFQUFFLFVBQWtCO1FBQ3JKLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDckIsS0FBSyxFQUFFLEtBQUs7WUFDWixJQUFJLEVBQUUsSUFBSTtZQUNWLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLE9BQU87WUFDaEIsVUFBVSxFQUFFLFVBQVU7WUFDdEIsa0JBQWtCLEVBQUUsa0JBQWtCO1lBQ3RDLFVBQVUsRUFBRSxVQUFVO1NBQ3pCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxVQUFVLENBQUMsRUFBVSxFQUFFLEtBQWEsRUFBRSxJQUFZLEVBQUUsUUFBZ0IsRUFBRyxNQUFjLEVBQUUsT0FBZSxFQUFFLFVBQWtCLEVBQUUsa0JBQTBCLEVBQUUsVUFBa0I7UUFDdEssT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUNyQixLQUFLLEVBQUUsS0FBSztZQUNaLElBQUksRUFBRSxJQUFJO1lBQ1YsUUFBUSxFQUFFLFFBQVE7WUFDbEIsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsT0FBTztZQUNoQixVQUFVLEVBQUUsVUFBVTtZQUN0QixrQkFBa0IsRUFBRSxrQkFBa0I7WUFDdEMsVUFBVSxFQUFFLFVBQVU7U0FDekIsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVLLEtBQUssQ0FBQyxFQUFVOztZQUNsQixJQUFJLEdBQUcsR0FBVyxpREFBaUQsQ0FBQztZQUNwRSxPQUFPLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUMsRUFBRSxDQUFDLENBQUM7UUFDakcsQ0FBQztLQUFBO0lBRUssTUFBTSxDQUFDLEVBQVUsRUFBRSxNQUFjOztZQUNuQyxJQUFJLEdBQUcsR0FBVyx1REFBdUQsQ0FBQztZQUMxRSxPQUFPLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBQyxFQUFFLENBQUMsQ0FBQztRQUNqSCxDQUFDO0tBQUE7SUFFSyxVQUFVLENBQUMsRUFBVTs7WUFDdkIsSUFBSSxHQUFHLEdBQVcsb0RBQW9ELENBQUM7WUFDdkUsT0FBTyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ2xHLENBQUM7S0FBQTtJQUVLLG1CQUFtQjs7WUFDckIsT0FBTztnQkFDSCxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRTtnQkFDckMsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7Z0JBQ3JDLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFO2dCQUNyQyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRTtnQkFDckMsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7Z0JBQ3JDLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFO2FBQ3hDLENBQUM7UUFDTixDQUFDO0tBQUE7Q0FFSjtBQXRHRCwwQ0FzR0MiLCJmaWxlIjoic2VydmljZS9odG1scGFnZXNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTUUwgfSBmcm9tIFwiLi4vY29tbW9uL3NxbFwiO1xuY29uc3Qgc2VxdWVsaXplID0gcmVxdWlyZShcInNlcXVlbGl6ZVwiKTtcbmxldCBfID0gcmVxdWlyZShcImxvZGFzaFwiKTtcbmNvbnN0IGdsb2JhbEFueTogYW55ID0gZ2xvYmFsO1xuXG5leHBvcnQgY2xhc3MgSHRtbFBhZ2VTZXJ2aWNlIHtcbiAgICBwcml2YXRlIGRhdGE6IGFueTtcbiAgICBwcml2YXRlIG1vZGVsOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihvYmo6IGFueSA9IG51bGwpIHtcbiAgICAgICAgaWYgKG9iaikgdGhpcy5kYXRhID0gXy5jbG9uZShvYmopO1xuICAgICAgICB0aGlzLm1vZGVsID0gc2VxdWVsaXplLm1vZGVscy5odG1scGFnZTtcbiAgICB9XG5cbiAgICBmaW5kT25lQnlJZChpZDogc3RyaW5nKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm1vZGVsLmZpbmRPbmUoe1xuICAgICAgICAgICAgYXR0cmlidXRlczogW1wiaWRcIiwgXCJ0aXRsZVwiLCBcInR5cGVcIiwgXCJwdlwiLCBbXCJpbWFnZV91cmxcIiwgXCJpbWFnZVVybFwiXSwgXCJhdXRob3JcIiwgXCJjb250ZW50XCIsIFtcImNyZWF0aW9uX2F0XCIsIFwiY3JlYXRpb25BdFwiXSwgW1wiaHRtbF9zY3JpcHRfZnJhZ21lbnRcIiwgXCJodG1sU2NyaXB0RnJhZ21lbnRcIl0sIFtcInBhZ2VfbnVtYmVyXCIsIFwicGFnZU51bWJlclwiXSwgW1wiaXNfb3BlblwiLCBcImlzT3BlblwiXV0sXG4gICAgICAgICAgICB3aGVyZTogeyBpZDogaWQgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBmaW5kUENPbmVCeUlkKGlkOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwuZmluZE9uZSh7XG4gICAgICAgICAgICBhdHRyaWJ1dGVzOiBbXCJpZFwiLCBcInRpdGxlXCIsIFwidHlwZVwiLCBcInB2XCIsIFtcImltYWdlX3VybFwiLCBcImltYWdlVXJsXCJdLCBcImF1dGhvclwiLCBcImNvbnRlbnRcIiwgW1wiY3JlYXRpb25fYXRcIiwgXCJjcmVhdGlvbkF0XCJdLCBbXCJodG1sX3NjcmlwdF9mcmFnbWVudFwiLCBcImh0bWxTY3JpcHRGcmFnbWVudFwiXSwgW1wicGFnZV9udW1iZXJcIiwgXCJwYWdlTnVtYmVyXCJdLCBbXCJpc19vcGVuXCIsIFwiaXNPcGVuXCJdXSxcbiAgICAgICAgICAgIHdoZXJlOiB7IGlkOiBpZCB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGZpbmRPbmVCeVBhZ2VOdW1iZXIocGFnZU51bWVyOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwuZmluZE9uZSh7XG4gICAgICAgICAgICBhdHRyaWJ1dGVzOiBbXCJpZFwiLCBcInRpdGxlXCIsIFwidHlwZVwiLCBcInB2XCIsIFtcImltYWdlX3VybFwiLCBcImltYWdlVXJsXCJdLCBcImF1dGhvclwiLCBcImNvbnRlbnRcIiwgW1wiY3JlYXRpb25fYXRcIiwgXCJjcmVhdGlvbkF0XCJdLCBbXCJodG1sX3NjcmlwdF9mcmFnbWVudFwiLCBcImh0bWxTY3JpcHRGcmFnbWVudFwiXSwgW1wicGFnZV9udW1iZXJcIiwgXCJwYWdlTnVtYmVyXCJdLCBbXCJpc19vcGVuXCIsIFwiaXNPcGVuXCJdXSxcbiAgICAgICAgICAgIHdoZXJlOiB7IHBhZ2VOdW1lcjogcGFnZU51bWVyIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgZmluZEFsbCh0eXBlOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwuZmluZCh7XG4gICAgICAgICAgICBhdHRyaWJ1dGVzOiBbXCJpZFwiLCBcInRpdGxlXCIsIFwidHlwZVwiLCBcInB2XCIsIFtcImltYWdlX3VybFwiLCBcImltYWdlVXJsXCJdLCBcImF1dGhvclwiLCBcImNvbnRlbnRcIiwgW1wiY3JlYXRpb25fYXRcIiwgXCJjcmVhdGlvbkF0XCJdLCBbXCJodG1sX3NjcmlwdF9mcmFnbWVudFwiLCBcImh0bWxTY3JpcHRGcmFnbWVudFwiXSwgW1wicGFnZV9udW1iZXJcIiwgXCJwYWdlTnVtYmVyXCJdLCBbXCJpc19vcGVuXCIsIFwiaXNPcGVuXCJdXSxcbiAgICAgICAgICAgIHdoZXJlOiB7IGlzRGVsZXRlZDogMCwgaXNPcGVuOiAxLCB0eXBlOiB0eXBlfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBhc3luYyBmaW5kUENBbGwocGFnZUluZGV4OiBudW1iZXIsIHBhZ2VTaXplOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3Qgc3FsID0gbmV3IFNRTCgpO1xuICAgICAgICBsZXQgaHRtbHBhZ2VzID0gW107XG4gICAgICAgIGxldCBjb3VudDogbnVtYmVyID0gMDtcbiAgICAgICAgbGV0IGxpbWl0OiBudW1iZXIgPSAoTnVtYmVyKHBhZ2VTaXplKSA9PT0gMCB8fCBOdW1iZXIocGFnZVNpemUpID4gNTApID8gNTAgOiBOdW1iZXIocGFnZVNpemUpO1xuICAgICAgICBsZXQgb2Zmc2V0OiBudW1iZXIgPSBsaW1pdCAqIChOdW1iZXIocGFnZUluZGV4KSAtIDEpO1xuICAgICAgICBjb3VudCA9IChhd2FpdCBnbG9iYWxBbnkuZGIucXVlcnkoc3FsLmdldEh0bWxQYWdlQ291bnQoKSwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHtcbiAgICAgICAgfX0pKVswXS5jb3VudDtcbiAgICAgICAgaHRtbHBhZ2VzID0gYXdhaXQgZ2xvYmFsQW55LmRiLnF1ZXJ5KHNxbC5nZXRIdG1sUGFnZShvZmZzZXQsIGxpbWl0KSwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHtcbiAgICAgICAgfX0pO1xuICAgICAgICByZXR1cm4geyBjb3VudDogY291bnQsIHJvd3M6IGh0bWxwYWdlcyB9O1xuICAgIH1cblxuICAgIGNyZWF0ZShpbWFnZVVybDogc3RyaW5nLCB0aXRsZTogc3RyaW5nLCB0eXBlOiBzdHJpbmcsIGF1dGhvcjogc3RyaW5nLCBjb250ZW50OiBzdHJpbmcsIGNyZWF0aW9uQXQ6IHN0cmluZywgaHRtbFNjcmlwdEZyYWdtZW50OiBzdHJpbmcsIHBhZ2VOdW1iZXI6IHN0cmluZykge1xuICAgICAgICByZXR1cm4gdGhpcy5tb2RlbC5jcmVhdGUoe1xuICAgICAgICAgICAgdGl0bGU6IHRpdGxlLFxuICAgICAgICAgICAgdHlwZTogdHlwZSxcbiAgICAgICAgICAgIGltYWdlVXJsOiBpbWFnZVVybCxcbiAgICAgICAgICAgIGF1dGhvcjogYXV0aG9yLFxuICAgICAgICAgICAgY29udGVudDogY29udGVudCxcbiAgICAgICAgICAgIGNyZWF0aW9uQXQ6IGNyZWF0aW9uQXQsXG4gICAgICAgICAgICBodG1sU2NyaXB0RnJhZ21lbnQ6IGh0bWxTY3JpcHRGcmFnbWVudCxcbiAgICAgICAgICAgIHBhZ2VOdW1iZXI6IHBhZ2VOdW1iZXJcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdXBkYXRlQnlJZChpZDogc3RyaW5nLCB0aXRsZTogc3RyaW5nLCB0eXBlOiBzdHJpbmcsIGltYWdlVXJsOiBzdHJpbmcgLCBhdXRob3I6IHN0cmluZywgY29udGVudDogc3RyaW5nLCBjcmVhdGlvbkF0OiBudW1iZXIsIGh0bWxTY3JpcHRGcmFnbWVudDogc3RyaW5nLCBwYWdlTnVtYmVyOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwudXBkYXRlKHtcbiAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcbiAgICAgICAgICAgIHR5cGU6IHR5cGUsXG4gICAgICAgICAgICBpbWFnZVVybDogaW1hZ2VVcmwsXG4gICAgICAgICAgICBhdXRob3I6IGF1dGhvcixcbiAgICAgICAgICAgIGNvbnRlbnQ6IGNvbnRlbnQsXG4gICAgICAgICAgICBjcmVhdGlvbkF0OiBjcmVhdGlvbkF0LFxuICAgICAgICAgICAgaHRtbFNjcmlwdEZyYWdtZW50OiBodG1sU2NyaXB0RnJhZ21lbnQsXG4gICAgICAgICAgICBwYWdlTnVtYmVyOiBwYWdlTnVtYmVyXG4gICAgICAgIH0sIHsgd2hlcmU6IHsgaWQ6IGlkIH0gfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgYWRkUFYoaWQ6IHN0cmluZykge1xuICAgICAgICBsZXQgc3FsOiBzdHJpbmcgPSBgdXBkYXRlIGh0bWxfcGFnZSBzZXQgcHYgPSBwdiArIDEgd2hlcmUgaWQgPSAkaWRgO1xuICAgICAgICByZXR1cm4gYXdhaXQgZ2xvYmFsQW55LmRiLnF1ZXJ5KHNxbCwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHsgaWQ6IGlkfSB9KTtcbiAgICB9XG5cbiAgICBhc3luYyBpc09wZW4oaWQ6IHN0cmluZywgaXNPcGVuOiBudW1iZXIpIHtcbiAgICAgICAgbGV0IHNxbDogc3RyaW5nID0gYHVwZGF0ZSBodG1sX3BhZ2Ugc2V0IGlzX29wZW4gPSAkaXNPcGVuIHdoZXJlIGlkID0gJGlkYDtcbiAgICAgICAgcmV0dXJuIGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeShzcWwsIHsgdHlwZTogc2VxdWVsaXplLlF1ZXJ5VHlwZXMuU0VMRUNULCBiaW5kOiB7IGlkOiBpZCwgaXNPcGVuOiBpc09wZW59IH0pO1xuICAgIH1cblxuICAgIGFzeW5jIGRlbGV0ZUJ5SWQoaWQ6IHN0cmluZykge1xuICAgICAgICBsZXQgc3FsOiBzdHJpbmcgPSBgdXBkYXRlIGh0bWxfcGFnZSBzZXQgaXNfZGVsZXRlZCA9IDEgd2hlcmUgaWQgPSAkaWRgO1xuICAgICAgICByZXR1cm4gYXdhaXQgZ2xvYmFsQW55LmRiLnF1ZXJ5KHNxbCwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHsgaWQ6IGlkIH0gfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgZ2V0SHRtbFBhZ2VDb2RlTGlzdCgpIHtcbiAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgIHsgY29kZTogXCJIVE1MVFlQRTAwMVwiLCBuYW1lOiBcIuavjeWptOWBpeW6t1wiIH0sXG4gICAgICAgICAgICB7IGNvZGU6IFwiSFRNTFRZUEUwMDJcIiwgbmFtZTogXCLlsLHljLvpl67oja9cIiB9LFxuICAgICAgICAgICAgeyBjb2RlOiBcIkhUTUxUWVBFMDAzXCIsIG5hbWU6IFwi6aWu6aOf6JCl5YW7XCIgfSxcbiAgICAgICAgICAgIHsgY29kZTogXCJIVE1MVFlQRTAwNFwiLCBuYW1lOiBcIuedoeecoOeuoeeQhlwiIH0sXG4gICAgICAgICAgICB7IGNvZGU6IFwiSFRNTFRZUEUwMDVcIiwgbmFtZTogXCLlh4/ph43lgaXouqtcIiB9LFxuICAgICAgICAgICAgeyBjb2RlOiBcIkhUTUxUWVBFMDA2XCIsIG5hbWU6IFwi56eR5oqA5YmN5rK/XCIgfVxuICAgICAgICBdO1xuICAgIH1cblxufSJdfQ==
