export declare class Response {
}
export declare class ResponseSuccess extends Response {
    private code;
    private message;
    private result;
    constructor(result: any, message?: string, code?: number);
    toJSON(): any;
}
export declare class ResponseParamsError extends Response {
    private code;
    private message;
    private errors;
    constructor(errors: any[], message?: string, code?: number);
    toJSON(): any;
}
export declare class AuthErrorResponse extends Response {
    private code;
    private message;
    /**
     * 构造函数
     * @param message 要返回的错误信息
     * @param code response code
     */
    constructor(message: string, code?: number);
    /**
     * @description 生成授权丢失信息
     * @returns 授权丢失信息
     * @author gaoqiang@gagogroup.com
     */
    static missingAuthToken(): AuthErrorResponse;
    /**
     * @description 生成授权错误信息
     * @returns 授权错误信息
     * @author gaoqiang@gagogroup.com
     */
    static authRequired(): AuthErrorResponse;
    /**
     * json格式化
     */
    toJSON(): any;
}
export declare class BadRequestResponse extends Response {
    private code;
    private message;
    constructor(message?: string, code?: number);
    toJSON(): any;
}
export declare class ResponseServiceError extends Response {
    private code;
    private message;
    constructor(message?: string, code?: number);
    toJSON(): any;
}
export declare class ResponseNotFoundError extends Response {
    private code;
    private message;
    constructor(message?: string, code?: number);
    toJSON(): any;
}
export declare class ResponseMissingTokenError extends Response {
    private code;
    private message;
    constructor(message?: string, code?: number);
    toJSON(): any;
}
export declare class ResponseMissingLoginError extends Response {
    private code;
    private message;
    constructor(message?: string, code?: number);
    toJSON(): any;
}
export declare class ResponseSuccessNoResult extends Response {
    private code;
    private message;
    constructor(result: any, message?: string, code?: number);
    toJSON(): any;
}
