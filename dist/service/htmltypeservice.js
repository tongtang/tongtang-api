"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlTypeService = void 0;
const fs = require("fs");
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny = global;
class HtmlTypeService {
    constructor(obj = null) {
        if (obj)
            this.data = _.clone(obj);
        this.model = sequelize.models.htmltype;
    }
    findOneById(id) {
        return this.model.findOne({
            attributes: ["id", "code", "name", ["is_open", "isOpen"], "weight", ["pic_url", "picUrl"], ["pic_url_new", "picUrlNew"]],
            where: { id: id }
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `select id as code, name, weight, pic_url as picUrl, pic_url_new as picUrlNew from html_type where is_deleted = 0 and is_open = 1 order by weight desc`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: {} });
        });
    }
    create(code, name, weight, picUrl, picUrlNew) {
        return this.model.create({
            code: code,
            name: name,
            weight: weight ? weight : 0,
            picUrl: picUrl,
            picUrlNew: picUrlNew
        });
    }
    updateById(id, code, name, weight, picUrl, picUrlNew) {
        return this.model.update({
            code: code,
            name: name,
            weight: weight,
            picUrl: picUrl,
            picUrlNew: picUrlNew
        }, { where: { id: id } });
    }
    isOpen(id, isOpen) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.model.update({
                isOpen: isOpen
            }, { where: { id: id } });
        });
    }
    deleteById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `update html_type set is_deleted = 1 where id = $id`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
        });
    }
    getHtmlPageCodeList() {
        return __awaiter(this, void 0, void 0, function* () {
            return [
                { code: "HTMLTYPE001", name: "母婴健康" },
                { code: "HTMLTYPE002", name: "就医问药" },
                { code: "HTMLTYPE003", name: "饮食营养" },
                { code: "HTMLTYPE004", name: "睡眠管理" },
                { code: "HTMLTYPE005", name: "减重健身" },
                { code: "HTMLTYPE006", name: "科技前沿" },
                { code: "HTMLTYPE007", name: "视频专区" }
            ];
        });
    }
    cleanDivision(divisionData) {
        return __awaiter(this, void 0, void 0, function* () {
            let sqlArray = [];
            for (let i = 1; i < divisionData.length; i++) {
                let insertSql = "";
                let division = divisionData[i];
                let provincecode = division[1] !== "" ? division[1].substring(0, 2) : null;
                let provincename = division[2] !== "" ? division[2] : null;
                let citycode = division[3] !== "" ? division[3].substring(0, 4) : null;
                let cityname = division[4] !== "" ? division[4] : null;
                let countycode = division[5] !== "" ? division[5].substring(0, 6) : null;
                let countyname = division[6] !== "" ? division[6] : null;
                let towncode = division[7] !== "" ? division[7].substring(0, 9) : null;
                let townname = division[8] !== "" ? division[8] : null;
                let villagecode = division[9] !== "" ? division[9].substring(0, 12) : null;
                let villagename = division[10] !== "" ? division[10] : null;
                let code = "";
                let name = "";
                if (villagecode) {
                    code = villagecode;
                    name = villagename;
                }
                else if (towncode) {
                    code = towncode;
                    name = townname;
                }
                else if (countycode) {
                    code = countycode;
                    name = countyname;
                }
                else if (citycode) {
                    code = citycode;
                    name = cityname;
                }
                else if (provincecode) {
                    code = provincecode;
                    name = provincename;
                }
                let sql = `insert into division_core(code , name , province_code, province_name, city_code, city_name, county_code, county_name, town_code, town_name, village_code, village_name) value (${code}, '${name}', ${provincecode}, '${provincename}', ${citycode}, '${cityname}', ${countycode}, '${countyname}', ${towncode}, '${townname}', ${villagecode}, '${villagename}');`;
                sqlArray.push(sql);
            }
            console.log("sqlArray.length: " + sqlArray.length);
            let sqlstring = "";
            for (let i = 0; i < sqlArray.length; i++) {
                sqlstring = sqlstring + sqlArray[i];
            }
            fs.createWriteStream("./upload/division001.sql").write(sqlstring);
        });
    }
}
exports.HtmlTypeService = HtmlTypeService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UvaHRtbHR5cGVzZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUNBLHlCQUF5QjtBQUN6QixNQUFNLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDdkMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzFCLE1BQU0sU0FBUyxHQUFRLE1BQU0sQ0FBQztBQUU5QixNQUFhLGVBQWU7SUFJeEIsWUFBWSxNQUFXLElBQUk7UUFDdkIsSUFBSSxHQUFHO1lBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDM0MsQ0FBQztJQUVELFdBQVcsQ0FBQyxFQUFVO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDdEIsVUFBVSxFQUFFLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQ3hILEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUU7U0FDcEIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVLLE9BQU87O1lBQ1QsSUFBSSxHQUFHLEdBQVcsdUpBQXVKLENBQUM7WUFDMUssT0FBTyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMxRixDQUFDO0tBQUE7SUFFRCxNQUFNLENBQUMsSUFBWSxFQUFFLElBQVksRUFBRSxNQUFjLEVBQUUsTUFBYyxFQUFFLFNBQWlCO1FBQ2hGLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDckIsSUFBSSxFQUFFLElBQUk7WUFDVixJQUFJLEVBQUUsSUFBSTtZQUNWLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQixNQUFNLEVBQUUsTUFBTTtZQUNkLFNBQVMsRUFBRSxTQUFTO1NBQ3ZCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxVQUFVLENBQUMsRUFBVSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsTUFBYyxFQUFFLE1BQWMsRUFBRSxTQUFpQjtRQUNoRyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1lBQ3JCLElBQUksRUFBRSxJQUFJO1lBQ1YsSUFBSSxFQUFFLElBQUk7WUFDVixNQUFNLEVBQUUsTUFBTTtZQUNkLE1BQU0sRUFBRSxNQUFNO1lBQ2QsU0FBUyxFQUFFLFNBQVM7U0FDdkIsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVLLE1BQU0sQ0FBQyxFQUFVLEVBQUUsTUFBYzs7WUFDbkMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztnQkFDckIsTUFBTSxFQUFFLE1BQU07YUFDakIsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDOUIsQ0FBQztLQUFBO0lBRUssVUFBVSxDQUFDLEVBQVU7O1lBQ3ZCLElBQUksR0FBRyxHQUFXLG9EQUFvRCxDQUFDO1lBQ3ZFLE9BQU8sTUFBTSxTQUFTLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNsRyxDQUFDO0tBQUE7SUFFSyxtQkFBbUI7O1lBQ3JCLE9BQU87Z0JBQ0gsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7Z0JBQ3JDLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFO2dCQUNyQyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRTtnQkFDckMsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7Z0JBQ3JDLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFO2dCQUNyQyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRTtnQkFDckMsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7YUFDeEMsQ0FBQztRQUNOLENBQUM7S0FBQTtJQUVLLGFBQWEsQ0FBQyxZQUFpQjs7WUFDakMsSUFBSSxRQUFRLEdBQWtCLEVBQUUsQ0FBQztZQUNqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDMUMsSUFBSSxTQUFTLEdBQVcsRUFBRSxDQUFDO2dCQUMzQixJQUFJLFFBQVEsR0FBUSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLElBQUksWUFBWSxHQUFXLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25GLElBQUksWUFBWSxHQUFXLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNuRSxJQUFJLFFBQVEsR0FBVyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMvRSxJQUFJLFFBQVEsR0FBVyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDL0QsSUFBSSxVQUFVLEdBQVcsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDakYsSUFBSSxVQUFVLEdBQVcsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2pFLElBQUksUUFBUSxHQUFXLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQy9FLElBQUksUUFBUSxHQUFXLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMvRCxJQUFJLFdBQVcsR0FBVyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNuRixJQUFJLFdBQVcsR0FBVyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDcEUsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO2dCQUNkLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztnQkFDZCxJQUFJLFdBQVcsRUFBRTtvQkFDYixJQUFJLEdBQUcsV0FBVyxDQUFDO29CQUNuQixJQUFJLEdBQUcsV0FBVyxDQUFDO2lCQUN0QjtxQkFBTSxJQUFJLFFBQVEsRUFBRTtvQkFDakIsSUFBSSxHQUFHLFFBQVEsQ0FBQztvQkFDaEIsSUFBSSxHQUFHLFFBQVEsQ0FBQztpQkFDbkI7cUJBQU0sSUFBSSxVQUFVLEVBQUU7b0JBQ25CLElBQUksR0FBRyxVQUFVLENBQUM7b0JBQ2xCLElBQUksR0FBRyxVQUFVLENBQUM7aUJBQ3JCO3FCQUFNLElBQUksUUFBUSxFQUFFO29CQUNqQixJQUFJLEdBQUcsUUFBUSxDQUFDO29CQUNoQixJQUFJLEdBQUcsUUFBUSxDQUFDO2lCQUNuQjtxQkFBTSxJQUFJLFlBQVksRUFBRTtvQkFDckIsSUFBSSxHQUFHLFlBQVksQ0FBQztvQkFDcEIsSUFBSSxHQUFHLFlBQVksQ0FBQztpQkFDdkI7Z0JBQ0QsSUFBSSxHQUFHLEdBQVcsa0xBQWtMLElBQUksTUFBTSxJQUFJLE1BQU0sWUFBWSxNQUFNLFlBQVksTUFBTSxRQUFRLE1BQU0sUUFBUSxNQUFNLFVBQVUsTUFBTSxVQUFVLE1BQU0sUUFBUSxNQUFNLFFBQVEsTUFBTSxXQUFXLE1BQU0sV0FBVyxLQUFLLENBQUM7Z0JBQ3RYLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDdEI7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNuRCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7WUFDbkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3RDLFNBQVMsR0FBRyxTQUFTLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3ZDO1lBQ0QsRUFBRSxDQUFDLGlCQUFpQixDQUFDLDBCQUEwQixDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3RFLENBQUM7S0FBQTtDQUdKO0FBN0dELDBDQTZHQyIsImZpbGUiOiJzZXJ2aWNlL2h0bWx0eXBlc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNRTCB9IGZyb20gXCIuLi9jb21tb24vc3FsXCI7XG5pbXBvcnQgKiBhcyBmcyBmcm9tIFwiZnNcIjtcbmNvbnN0IHNlcXVlbGl6ZSA9IHJlcXVpcmUoXCJzZXF1ZWxpemVcIik7XG5sZXQgXyA9IHJlcXVpcmUoXCJsb2Rhc2hcIik7XG5jb25zdCBnbG9iYWxBbnk6IGFueSA9IGdsb2JhbDtcblxuZXhwb3J0IGNsYXNzIEh0bWxUeXBlU2VydmljZSB7XG4gICAgcHJpdmF0ZSBkYXRhOiBhbnk7XG4gICAgcHJpdmF0ZSBtb2RlbDogYW55O1xuXG4gICAgY29uc3RydWN0b3Iob2JqOiBhbnkgPSBudWxsKSB7XG4gICAgICAgIGlmIChvYmopIHRoaXMuZGF0YSA9IF8uY2xvbmUob2JqKTtcbiAgICAgICAgdGhpcy5tb2RlbCA9IHNlcXVlbGl6ZS5tb2RlbHMuaHRtbHR5cGU7XG4gICAgfVxuXG4gICAgZmluZE9uZUJ5SWQoaWQ6IHN0cmluZykge1xuICAgICAgICByZXR1cm4gdGhpcy5tb2RlbC5maW5kT25lKHtcbiAgICAgICAgICAgIGF0dHJpYnV0ZXM6IFtcImlkXCIsIFwiY29kZVwiLCBcIm5hbWVcIiwgW1wiaXNfb3BlblwiLCBcImlzT3BlblwiXSwgXCJ3ZWlnaHRcIiwgW1wicGljX3VybFwiLCBcInBpY1VybFwiXSwgW1wicGljX3VybF9uZXdcIiwgXCJwaWNVcmxOZXdcIl1dLFxuICAgICAgICAgICAgd2hlcmU6IHsgaWQ6IGlkIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgZmluZEFsbCgpIHtcbiAgICAgICAgbGV0IHNxbDogc3RyaW5nID0gYHNlbGVjdCBpZCBhcyBjb2RlLCBuYW1lLCB3ZWlnaHQsIHBpY191cmwgYXMgcGljVXJsLCBwaWNfdXJsX25ldyBhcyBwaWNVcmxOZXcgZnJvbSBodG1sX3R5cGUgd2hlcmUgaXNfZGVsZXRlZCA9IDAgYW5kIGlzX29wZW4gPSAxIG9yZGVyIGJ5IHdlaWdodCBkZXNjYDtcbiAgICAgICAgcmV0dXJuIGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeShzcWwsIHsgdHlwZTogc2VxdWVsaXplLlF1ZXJ5VHlwZXMuU0VMRUNULCBiaW5kOiB7fSB9KTtcbiAgICB9XG5cbiAgICBjcmVhdGUoY29kZTogc3RyaW5nLCBuYW1lOiBzdHJpbmcsIHdlaWdodDogbnVtYmVyLCBwaWNVcmw6IHN0cmluZywgcGljVXJsTmV3OiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwuY3JlYXRlKHtcbiAgICAgICAgICAgIGNvZGU6IGNvZGUsXG4gICAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgICAgd2VpZ2h0OiB3ZWlnaHQgPyB3ZWlnaHQgOiAwLFxuICAgICAgICAgICAgcGljVXJsOiBwaWNVcmwsXG4gICAgICAgICAgICBwaWNVcmxOZXc6IHBpY1VybE5ld1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICB1cGRhdGVCeUlkKGlkOiBzdHJpbmcsIGNvZGU6IHN0cmluZywgbmFtZTogc3RyaW5nLCB3ZWlnaHQ6IG51bWJlciwgcGljVXJsOiBzdHJpbmcsIHBpY1VybE5ldzogc3RyaW5nKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm1vZGVsLnVwZGF0ZSh7XG4gICAgICAgICAgICBjb2RlOiBjb2RlLFxuICAgICAgICAgICAgbmFtZTogbmFtZSxcbiAgICAgICAgICAgIHdlaWdodDogd2VpZ2h0LFxuICAgICAgICAgICAgcGljVXJsOiBwaWNVcmwsXG4gICAgICAgICAgICBwaWNVcmxOZXc6IHBpY1VybE5ld1xuICAgICAgICB9LCB7IHdoZXJlOiB7IGlkOiBpZCB9IH0pO1xuICAgIH1cblxuICAgIGFzeW5jIGlzT3BlbihpZDogc3RyaW5nLCBpc09wZW46IG51bWJlcikge1xuICAgICAgICByZXR1cm4gdGhpcy5tb2RlbC51cGRhdGUoe1xuICAgICAgICAgICAgaXNPcGVuOiBpc09wZW5cbiAgICAgICAgfSwgeyB3aGVyZTogeyBpZDogaWQgfSB9KTtcbiAgICB9XG5cbiAgICBhc3luYyBkZWxldGVCeUlkKGlkOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IHNxbDogc3RyaW5nID0gYHVwZGF0ZSBodG1sX3R5cGUgc2V0IGlzX2RlbGV0ZWQgPSAxIHdoZXJlIGlkID0gJGlkYDtcbiAgICAgICAgcmV0dXJuIGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeShzcWwsIHsgdHlwZTogc2VxdWVsaXplLlF1ZXJ5VHlwZXMuU0VMRUNULCBiaW5kOiB7IGlkOiBpZCB9IH0pO1xuICAgIH1cblxuICAgIGFzeW5jIGdldEh0bWxQYWdlQ29kZUxpc3QoKSB7XG4gICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICB7IGNvZGU6IFwiSFRNTFRZUEUwMDFcIiwgbmFtZTogXCLmr43lqbTlgaXlurdcIiB9LFxuICAgICAgICAgICAgeyBjb2RlOiBcIkhUTUxUWVBFMDAyXCIsIG5hbWU6IFwi5bCx5Yy76Zeu6I2vXCIgfSxcbiAgICAgICAgICAgIHsgY29kZTogXCJIVE1MVFlQRTAwM1wiLCBuYW1lOiBcIumlrumjn+iQpeWFu1wiIH0sXG4gICAgICAgICAgICB7IGNvZGU6IFwiSFRNTFRZUEUwMDRcIiwgbmFtZTogXCLnnaHnnKDnrqHnkIZcIiB9LFxuICAgICAgICAgICAgeyBjb2RlOiBcIkhUTUxUWVBFMDA1XCIsIG5hbWU6IFwi5YeP6YeN5YGl6LqrXCIgfSxcbiAgICAgICAgICAgIHsgY29kZTogXCJIVE1MVFlQRTAwNlwiLCBuYW1lOiBcIuenkeaKgOWJjeayv1wiIH0sXG4gICAgICAgICAgICB7IGNvZGU6IFwiSFRNTFRZUEUwMDdcIiwgbmFtZTogXCLop4bpopHkuJPljLpcIiB9XG4gICAgICAgIF07XG4gICAgfVxuXG4gICAgYXN5bmMgY2xlYW5EaXZpc2lvbihkaXZpc2lvbkRhdGE6IGFueSkge1xuICAgICAgICBsZXQgc3FsQXJyYXk6IEFycmF5PHN0cmluZz4gPSBbXTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPCBkaXZpc2lvbkRhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGxldCBpbnNlcnRTcWw6IHN0cmluZyA9IFwiXCI7XG4gICAgICAgICAgICBsZXQgZGl2aXNpb246IGFueSA9IGRpdmlzaW9uRGF0YVtpXTtcbiAgICAgICAgICAgIGxldCBwcm92aW5jZWNvZGU6IHN0cmluZyA9IGRpdmlzaW9uWzFdICE9PSBcIlwiID8gZGl2aXNpb25bMV0uc3Vic3RyaW5nKDAsIDIpIDogbnVsbDtcbiAgICAgICAgICAgIGxldCBwcm92aW5jZW5hbWU6IHN0cmluZyA9IGRpdmlzaW9uWzJdICE9PSBcIlwiID8gZGl2aXNpb25bMl0gOiBudWxsO1xuICAgICAgICAgICAgbGV0IGNpdHljb2RlOiBzdHJpbmcgPSBkaXZpc2lvblszXSAhPT0gXCJcIiA/IGRpdmlzaW9uWzNdLnN1YnN0cmluZygwLCA0KSA6IG51bGw7XG4gICAgICAgICAgICBsZXQgY2l0eW5hbWU6IHN0cmluZyA9IGRpdmlzaW9uWzRdICE9PSBcIlwiID8gZGl2aXNpb25bNF0gOiBudWxsO1xuICAgICAgICAgICAgbGV0IGNvdW50eWNvZGU6IHN0cmluZyA9IGRpdmlzaW9uWzVdICE9PSBcIlwiID8gZGl2aXNpb25bNV0uc3Vic3RyaW5nKDAsIDYpIDogbnVsbDtcbiAgICAgICAgICAgIGxldCBjb3VudHluYW1lOiBzdHJpbmcgPSBkaXZpc2lvbls2XSAhPT0gXCJcIiA/IGRpdmlzaW9uWzZdIDogbnVsbDtcbiAgICAgICAgICAgIGxldCB0b3duY29kZTogc3RyaW5nID0gZGl2aXNpb25bN10gIT09IFwiXCIgPyBkaXZpc2lvbls3XS5zdWJzdHJpbmcoMCwgOSkgOiBudWxsO1xuICAgICAgICAgICAgbGV0IHRvd25uYW1lOiBzdHJpbmcgPSBkaXZpc2lvbls4XSAhPT0gXCJcIiA/IGRpdmlzaW9uWzhdIDogbnVsbDtcbiAgICAgICAgICAgIGxldCB2aWxsYWdlY29kZTogc3RyaW5nID0gZGl2aXNpb25bOV0gIT09IFwiXCIgPyBkaXZpc2lvbls5XS5zdWJzdHJpbmcoMCwgMTIpIDogbnVsbDtcbiAgICAgICAgICAgIGxldCB2aWxsYWdlbmFtZTogc3RyaW5nID0gZGl2aXNpb25bMTBdICE9PSBcIlwiID8gZGl2aXNpb25bMTBdIDogbnVsbDtcbiAgICAgICAgICAgIGxldCBjb2RlID0gXCJcIjtcbiAgICAgICAgICAgIGxldCBuYW1lID0gXCJcIjtcbiAgICAgICAgICAgIGlmICh2aWxsYWdlY29kZSkge1xuICAgICAgICAgICAgICAgIGNvZGUgPSB2aWxsYWdlY29kZTtcbiAgICAgICAgICAgICAgICBuYW1lID0gdmlsbGFnZW5hbWU7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRvd25jb2RlKSB7XG4gICAgICAgICAgICAgICAgY29kZSA9IHRvd25jb2RlO1xuICAgICAgICAgICAgICAgIG5hbWUgPSB0b3dubmFtZTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoY291bnR5Y29kZSkge1xuICAgICAgICAgICAgICAgIGNvZGUgPSBjb3VudHljb2RlO1xuICAgICAgICAgICAgICAgIG5hbWUgPSBjb3VudHluYW1lO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChjaXR5Y29kZSkge1xuICAgICAgICAgICAgICAgIGNvZGUgPSBjaXR5Y29kZTtcbiAgICAgICAgICAgICAgICBuYW1lID0gY2l0eW5hbWU7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHByb3ZpbmNlY29kZSkge1xuICAgICAgICAgICAgICAgIGNvZGUgPSBwcm92aW5jZWNvZGU7XG4gICAgICAgICAgICAgICAgbmFtZSA9IHByb3ZpbmNlbmFtZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBzcWw6IHN0cmluZyA9IGBpbnNlcnQgaW50byBkaXZpc2lvbl9jb3JlKGNvZGUgLCBuYW1lICwgcHJvdmluY2VfY29kZSwgcHJvdmluY2VfbmFtZSwgY2l0eV9jb2RlLCBjaXR5X25hbWUsIGNvdW50eV9jb2RlLCBjb3VudHlfbmFtZSwgdG93bl9jb2RlLCB0b3duX25hbWUsIHZpbGxhZ2VfY29kZSwgdmlsbGFnZV9uYW1lKSB2YWx1ZSAoJHtjb2RlfSwgJyR7bmFtZX0nLCAke3Byb3ZpbmNlY29kZX0sICcke3Byb3ZpbmNlbmFtZX0nLCAke2NpdHljb2RlfSwgJyR7Y2l0eW5hbWV9JywgJHtjb3VudHljb2RlfSwgJyR7Y291bnR5bmFtZX0nLCAke3Rvd25jb2RlfSwgJyR7dG93bm5hbWV9JywgJHt2aWxsYWdlY29kZX0sICcke3ZpbGxhZ2VuYW1lfScpO2A7XG4gICAgICAgICAgICBzcWxBcnJheS5wdXNoKHNxbCk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc29sZS5sb2coXCJzcWxBcnJheS5sZW5ndGg6IFwiICsgc3FsQXJyYXkubGVuZ3RoKTtcbiAgICAgICAgbGV0IHNxbHN0cmluZyA9IFwiXCI7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc3FsQXJyYXkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHNxbHN0cmluZyA9IHNxbHN0cmluZyArIHNxbEFycmF5W2ldO1xuICAgICAgICB9XG4gICAgICAgIGZzLmNyZWF0ZVdyaXRlU3RyZWFtKFwiLi91cGxvYWQvZGl2aXNpb24wMDEuc3FsXCIpLndyaXRlKHNxbHN0cmluZyk7XG4gICAgfVxuXG4gICAgLy8gZXhwb3J0XG59Il19
