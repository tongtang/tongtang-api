export declare class UserHealthInformationService {
    private data;
    private model;
    constructor(obj?: any);
    findOneById(id: string): any;
    findOneByOrignId(originId: string, origin: number): any;
    findPCAll(telephone: string, realName: string, startTime: number, endTime: number, pageIndex: number, pageSize: number): Promise<{
        count: number;
        rows: any;
    }>;
    create(realName: string, sex: number, age: number, telephone: string, idCard: string, healthReportUrl: string, height: number, weight: number, waistline: number, hipline: number, sbp: number, dbp: number, rbg: number, fpg: number, hpg: number, ua: number, tc: number, tg: number, hdlc: number, ldlc: number, origin: number, originId: string): any;
    updateById(id: string, realName: string, sex: number, age: number, telephone: string, idCard: string, healthReportUrl: string, height: number, weight: number, waistline: number, hipline: number, sbp: number, dbp: number, rbg: number, fpg: number, hpg: number, ua: number, tc: number, tg: number, hdlc: number, ldlc: number, origin: number, originId: string): any;
    deleteById(id: string): Promise<any>;
    export(startTime: number, endTime: number): Promise<any>;
}
