"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const sql_1 = require("../common/sql");
const random_utils_1 = require("../util/random-utils");
const redishelper_1 = require("../common/redishelper");
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny = global;
class UserService {
    constructor(obj = null) {
        if (obj)
            this.data = _.clone(obj);
        this.model = sequelize.models.user;
    }
    findOne(id) {
        return this.model.findOne({
            attributes: ["username", "password"],
            where: { id: id }
        });
    }
    findOneByUserName(username) {
        return this.model.findOne({
            attributes: ["id", "username", "password"],
            where: { username: username }
        });
    }
    findAll(pageIndex, pageSize) {
        return __awaiter(this, void 0, void 0, function* () {
            const sql = new sql_1.SQL();
            let userHealthInformations = [];
            let count = 0;
            let limit = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
            let offset = limit * (Number(pageIndex) - 1);
            count = (yield globalAny.db.query(sql.getUserCount(), { type: sequelize.QueryTypes.SELECT, bind: {} }))[0].count;
            userHealthInformations = yield globalAny.db.query(sql.getUser(offset, limit), { type: sequelize.QueryTypes.SELECT, bind: {} });
            return { count: count, rows: userHealthInformations };
        });
    }
    setTokenInRedis(userInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            let tokenTime = 600;
            // 生成随机token
            let token = random_utils_1.RandomUtils.generateToken();
            let redisHelper = redishelper_1.RedisHelper.getInstance();
            yield redisHelper.setDataWithKey("USER:" + token, JSON.stringify(userInfo), tokenTime / 60);
            return {
                id: userInfo.id,
                username: userInfo.username,
                token: token
            };
        });
    }
    logout(token) {
        return __awaiter(this, void 0, void 0, function* () {
            let redisHelper = redishelper_1.RedisHelper.getInstance();
            let redisContent = yield redisHelper.getDataByKey("USER:" + token);
            if (redisContent) {
                yield redisHelper.clearDataWithKey("USER:" + token);
            }
        });
    }
}
exports.UserService = UserService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UvdXNlcnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQW9DO0FBQ3BDLHVEQUFvRDtBQUNwRCx1REFBb0Q7QUFDcEQsTUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3ZDLElBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMxQixNQUFNLFNBQVMsR0FBUSxNQUFNLENBQUM7QUFFOUIsTUFBYSxXQUFXO0lBSXRCLFlBQVksTUFBVyxJQUFJO1FBQ3pCLElBQUksR0FBRztZQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ3JDLENBQUM7SUFFRCxPQUFPLENBQUMsRUFBVTtRQUNoQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBQ3hCLFVBQVUsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLENBQUM7WUFDcEMsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRTtTQUNsQixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsUUFBZ0I7UUFDaEMsT0FBUSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztZQUN6QixVQUFVLEVBQUUsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQztZQUMxQyxLQUFLLEVBQUUsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFO1NBQzlCLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFSyxPQUFPLENBQUMsU0FBaUIsRUFBRSxRQUFnQjs7WUFDL0MsTUFBTSxHQUFHLEdBQUcsSUFBSSxTQUFHLEVBQUUsQ0FBQztZQUN0QixJQUFJLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztZQUNoQyxJQUFJLEtBQUssR0FBVyxDQUFDLENBQUM7WUFDdEIsSUFBSSxLQUFLLEdBQVcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDOUYsSUFBSSxNQUFNLEdBQVcsS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3JELEtBQUssR0FBRyxDQUFDLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxFQUNoRyxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNkLHNCQUFzQixHQUFHLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEVBQ3hILEVBQUMsQ0FBQyxDQUFDO1lBQ0osT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLHNCQUFzQixFQUFFLENBQUM7UUFDeEQsQ0FBQztLQUFBO0lBRUssZUFBZSxDQUFDLFFBQWE7O1lBQ2pDLElBQUksU0FBUyxHQUFXLEdBQUcsQ0FBQztZQUM1QixZQUFZO1lBQ1osSUFBSSxLQUFLLEdBQUcsMEJBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUN4QyxJQUFJLFdBQVcsR0FBZ0IseUJBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6RCxNQUFNLFdBQVcsQ0FBQyxjQUFjLENBQUMsT0FBTyxHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLFNBQVMsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUM1RixPQUFPO2dCQUNMLEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDZixRQUFRLEVBQUUsUUFBUSxDQUFDLFFBQVE7Z0JBQzNCLEtBQUssRUFBRSxLQUFLO2FBQ2IsQ0FBQztRQUNKLENBQUM7S0FBQTtJQUVLLE1BQU0sQ0FBQyxLQUFhOztZQUN4QixJQUFJLFdBQVcsR0FBZ0IseUJBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6RCxJQUFJLFlBQVksR0FBVyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFDO1lBQzNFLElBQUksWUFBWSxFQUFFO2dCQUNoQixNQUFNLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUM7YUFDckQ7UUFDSCxDQUFDO0tBQUE7Q0FDRjtBQXhERCxrQ0F3REMiLCJmaWxlIjoic2VydmljZS91c2Vyc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNRTCB9IGZyb20gXCIuLi9jb21tb24vc3FsXCI7XG5pbXBvcnQgeyBSYW5kb21VdGlscyB9IGZyb20gIFwiLi4vdXRpbC9yYW5kb20tdXRpbHNcIjtcbmltcG9ydCB7IFJlZGlzSGVscGVyIH0gZnJvbSBcIi4uL2NvbW1vbi9yZWRpc2hlbHBlclwiO1xuY29uc3Qgc2VxdWVsaXplID0gcmVxdWlyZShcInNlcXVlbGl6ZVwiKTtcbmxldCBfID0gcmVxdWlyZShcImxvZGFzaFwiKTtcbmNvbnN0IGdsb2JhbEFueTogYW55ID0gZ2xvYmFsO1xuXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xuICBwcml2YXRlIGRhdGE6IGFueTtcbiAgcHJpdmF0ZSBtb2RlbDogYW55O1xuXG4gIGNvbnN0cnVjdG9yKG9iajogYW55ID0gbnVsbCkge1xuICAgIGlmIChvYmopIHRoaXMuZGF0YSA9IF8uY2xvbmUob2JqKTtcbiAgICB0aGlzLm1vZGVsID0gc2VxdWVsaXplLm1vZGVscy51c2VyO1xuICB9XG5cbiAgZmluZE9uZShpZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIHRoaXMubW9kZWwuZmluZE9uZSh7XG4gICAgICBhdHRyaWJ1dGVzOiBbXCJ1c2VybmFtZVwiLCBcInBhc3N3b3JkXCJdLFxuICAgICAgd2hlcmU6IHsgaWQ6IGlkIH1cbiAgICB9KTtcbiAgfVxuXG4gIGZpbmRPbmVCeVVzZXJOYW1lKHVzZXJuYW1lOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gIHRoaXMubW9kZWwuZmluZE9uZSh7XG4gICAgICBhdHRyaWJ1dGVzOiBbXCJpZFwiLCBcInVzZXJuYW1lXCIsIFwicGFzc3dvcmRcIl0sXG4gICAgICB3aGVyZTogeyB1c2VybmFtZTogdXNlcm5hbWUgfVxuICAgIH0pO1xuICB9XG5cbiAgYXN5bmMgZmluZEFsbChwYWdlSW5kZXg6IG51bWJlciwgcGFnZVNpemU6IG51bWJlcikge1xuICAgIGNvbnN0IHNxbCA9IG5ldyBTUUwoKTtcbiAgICBsZXQgdXNlckhlYWx0aEluZm9ybWF0aW9ucyA9IFtdO1xuICAgIGxldCBjb3VudDogbnVtYmVyID0gMDtcbiAgICBsZXQgbGltaXQ6IG51bWJlciA9IChOdW1iZXIocGFnZVNpemUpID09PSAwIHx8IE51bWJlcihwYWdlU2l6ZSkgPiA1MCkgPyA1MCA6IE51bWJlcihwYWdlU2l6ZSk7XG4gICAgbGV0IG9mZnNldDogbnVtYmVyID0gbGltaXQgKiAoTnVtYmVyKHBhZ2VJbmRleCkgLSAxKTtcbiAgICBjb3VudCA9IChhd2FpdCBnbG9iYWxBbnkuZGIucXVlcnkoc3FsLmdldFVzZXJDb3VudCgpLCB7IHR5cGU6IHNlcXVlbGl6ZS5RdWVyeVR5cGVzLlNFTEVDVCwgYmluZDoge1xuICAgIH19KSlbMF0uY291bnQ7XG4gICAgdXNlckhlYWx0aEluZm9ybWF0aW9ucyA9IGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeShzcWwuZ2V0VXNlcihvZmZzZXQsIGxpbWl0KSwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHtcbiAgICB9fSk7XG4gICAgcmV0dXJuIHsgY291bnQ6IGNvdW50LCByb3dzOiB1c2VySGVhbHRoSW5mb3JtYXRpb25zIH07XG4gIH1cblxuICBhc3luYyBzZXRUb2tlbkluUmVkaXModXNlckluZm86IGFueSkge1xuICAgIGxldCB0b2tlblRpbWU6IG51bWJlciA9IDYwMDtcbiAgICAvLyDnlJ/miJDpmo/mnLp0b2tlblxuICAgIGxldCB0b2tlbiA9IFJhbmRvbVV0aWxzLmdlbmVyYXRlVG9rZW4oKTtcbiAgICBsZXQgcmVkaXNIZWxwZXI6IFJlZGlzSGVscGVyID0gUmVkaXNIZWxwZXIuZ2V0SW5zdGFuY2UoKTtcbiAgICBhd2FpdCByZWRpc0hlbHBlci5zZXREYXRhV2l0aEtleShcIlVTRVI6XCIgKyB0b2tlbiwgSlNPTi5zdHJpbmdpZnkodXNlckluZm8pLCB0b2tlblRpbWUgLyA2MCk7XG4gICAgcmV0dXJuIHtcbiAgICAgIGlkOiB1c2VySW5mby5pZCxcbiAgICAgIHVzZXJuYW1lOiB1c2VySW5mby51c2VybmFtZSxcbiAgICAgIHRva2VuOiB0b2tlblxuICAgIH07XG4gIH1cblxuICBhc3luYyBsb2dvdXQodG9rZW46IHN0cmluZykge1xuICAgIGxldCByZWRpc0hlbHBlcjogUmVkaXNIZWxwZXIgPSBSZWRpc0hlbHBlci5nZXRJbnN0YW5jZSgpO1xuICAgIGxldCByZWRpc0NvbnRlbnQ6IHN0cmluZyA9IGF3YWl0IHJlZGlzSGVscGVyLmdldERhdGFCeUtleShcIlVTRVI6XCIgKyB0b2tlbik7XG4gICAgaWYgKHJlZGlzQ29udGVudCkge1xuICAgICAgYXdhaXQgcmVkaXNIZWxwZXIuY2xlYXJEYXRhV2l0aEtleShcIlVTRVI6XCIgKyB0b2tlbik7XG4gICAgfVxuICB9XG59Il19
