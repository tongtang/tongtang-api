export declare class HtmlTypeService {
    private data;
    private model;
    constructor(obj?: any);
    findOneById(id: string): any;
    findAll(): Promise<any>;
    create(code: string, name: string, weight: number, picUrl: string, picUrlNew: string): any;
    updateById(id: string, code: string, name: string, weight: number, picUrl: string, picUrlNew: string): any;
    isOpen(id: string, isOpen: number): Promise<any>;
    deleteById(id: string): Promise<any>;
    getHtmlPageCodeList(): Promise<{
        code: string;
        name: string;
    }[]>;
    cleanDivision(divisionData: any): Promise<void>;
}
