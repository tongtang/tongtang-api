export declare class UserService {
    private data;
    private model;
    constructor(obj?: any);
    findOne(id: string): any;
    findOneByUserName(username: string): any;
    findAll(pageIndex: number, pageSize: number): Promise<{
        count: number;
        rows: any;
    }>;
    setTokenInRedis(userInfo: any): Promise<{
        id: any;
        username: any;
        token: string;
    }>;
    logout(token: string): Promise<void>;
}
