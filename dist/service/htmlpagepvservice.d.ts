export declare class HtmlPagePvService {
    private data;
    private model;
    constructor(obj?: any);
    create(date: number, pv: number, htmlPageId: string): any;
    addPV(id: string): Promise<any>;
    getHtmlPagePvById(id: string, startTime: number, endTime: number): Promise<any>;
}
