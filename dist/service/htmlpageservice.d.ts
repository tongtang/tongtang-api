export declare class HtmlPageService {
    private data;
    private model;
    constructor(obj?: any);
    findOneById(id: string): any;
    findPCOneById(id: string): any;
    findOneByPageNumber(pageNumer: string): any;
    findAll(type: string): Promise<any>;
    findAllPagePv(): Promise<any>;
    findPCAll(title: string, type: string, isOpen: number, startTime: number, endTime: number, pageIndex: number, pageSize: number): Promise<{
        count: number;
        pvCount: number;
        rows: any;
    }>;
    create(imageUrl: string, title: string, comment: string, type: string, author: string, content: string, creationAt: string, htmlScriptFragment: string, pageNumber: string, weight: number, isVideo: number): any;
    updateById(id: string, title: string, comment: string, type: string, imageUrl: string, author: string, content: string, creationAt: number, htmlScriptFragment: string, pageNumber: string, weight: number, isVideo: number): any;
    addPV(id: string): Promise<any>;
    isOpen(id: string, isOpen: number): Promise<any>;
    deleteById(id: string): Promise<any>;
    getHtmlPageCodeList(): Promise<{
        code: string;
        name: string;
    }[]>;
}
