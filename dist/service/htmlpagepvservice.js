"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HtmlPagePvService = void 0;
const sequelize = require("sequelize");
let _ = require("lodash");
const moment = require("moment");
const globalAny = global;
class HtmlPagePvService {
    constructor(obj = null) {
        if (obj)
            this.data = _.clone(obj);
        this.model = sequelize.models.htmlpagepv;
    }
    create(date, pv, htmlPageId) {
        return this.model.create({
            pv: pv,
            date: date,
            htmlPageId: htmlPageId
        });
    }
    addPV(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const date = Number(moment(new Date()).format("YYYYMMDD"));
            let queryHmlPagePv = `select html_page_id as htmlPageId from html_page_pv where date = ${date} and html_page_id = '${id}' and is_deleted =0`;
            let result = yield globalAny.db.query(queryHmlPagePv, { type: sequelize.QueryTypes.SELECT, bind: {} });
            if (result.length > 0) {
                let htmlPageId = result[0].htmlPageId;
                let updateSql = `update html_page_pv set pv = pv + 1 where html_page_id = $htmlPageId and date = ${date}; `;
                return yield globalAny.db.query(updateSql, { type: sequelize.QueryTypes.UPDATE, bind: { htmlPageId: htmlPageId } });
            }
            else {
                return yield this.create(date, 1, id);
            }
        });
    }
    getHtmlPagePvById(id, startTime, endTime) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `select html_page_id as htmlPageId, date, pv from html_page_pv where html_page_id = '${id}' and is_deleted = 0`;
            if (startTime) {
                sql += ` and date >= ${startTime}`;
            }
            if (endTime) {
                sql += ` and date <= ${endTime}`;
            }
            sql += ` order by date;`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: {} });
        });
    }
}
exports.HtmlPagePvService = HtmlPagePvService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UvaHRtbHBhZ2VwdnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBRUEsTUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3ZDLElBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMxQixpQ0FBa0M7QUFDbEMsTUFBTSxTQUFTLEdBQVEsTUFBTSxDQUFDO0FBRTlCLE1BQWEsaUJBQWlCO0lBSTFCLFlBQVksTUFBVyxJQUFJO1FBQ3ZCLElBQUksR0FBRztZQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO0lBQzdDLENBQUM7SUFFRCxNQUFNLENBQUMsSUFBWSxFQUFFLEVBQVUsRUFBRSxVQUFrQjtRQUMvQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1lBQ3JCLEVBQUUsRUFBRSxFQUFFO1lBQ04sSUFBSSxFQUFFLElBQUk7WUFDVixVQUFVLEVBQUUsVUFBVTtTQUN6QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUssS0FBSyxDQUFDLEVBQVU7O1lBQ2xCLE1BQU0sSUFBSSxHQUFXLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ25FLElBQUksY0FBYyxHQUFXLG9FQUFvRSxJQUFJLHdCQUF3QixFQUFFLHFCQUFxQixDQUFDO1lBQ3JKLElBQUksTUFBTSxHQUFRLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzVHLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ25CLElBQUksVUFBVSxHQUFXLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7Z0JBQzlDLElBQUksU0FBUyxHQUFXLG1GQUFtRixJQUFJLElBQUksQ0FBQztnQkFDcEgsT0FBTyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZIO2lCQUFNO2dCQUNILE9BQU8sTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDekM7UUFFTCxDQUFDO0tBQUE7SUFFSyxpQkFBaUIsQ0FBQyxFQUFVLEVBQUUsU0FBaUIsRUFBRSxPQUFlOztZQUNsRSxJQUFJLEdBQUcsR0FBVyx1RkFBdUYsRUFBRSxzQkFBc0IsQ0FBQztZQUNsSSxJQUFJLFNBQVMsRUFBRTtnQkFDWCxHQUFHLElBQUksZ0JBQWdCLFNBQVMsRUFBRSxDQUFDO2FBQ3RDO1lBQ0QsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsR0FBRyxJQUFJLGdCQUFnQixPQUFPLEVBQUUsQ0FBQzthQUNwQztZQUNELEdBQUcsSUFBSSxpQkFBaUIsQ0FBQztZQUN6QixPQUFPLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzFGLENBQUM7S0FBQTtDQUNKO0FBMUNELDhDQTBDQyIsImZpbGUiOiJzZXJ2aWNlL2h0bWxwYWdlcHZzZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTk9XIH0gZnJvbSBcInNlcXVlbGl6ZVwiO1xuaW1wb3J0IHsgU1FMIH0gZnJvbSBcIi4uL2NvbW1vbi9zcWxcIjtcbmNvbnN0IHNlcXVlbGl6ZSA9IHJlcXVpcmUoXCJzZXF1ZWxpemVcIik7XG5sZXQgXyA9IHJlcXVpcmUoXCJsb2Rhc2hcIik7XG5pbXBvcnQgbW9tZW50ID0gcmVxdWlyZShcIm1vbWVudFwiKTtcbmNvbnN0IGdsb2JhbEFueTogYW55ID0gZ2xvYmFsO1xuXG5leHBvcnQgY2xhc3MgSHRtbFBhZ2VQdlNlcnZpY2Uge1xuICAgIHByaXZhdGUgZGF0YTogYW55O1xuICAgIHByaXZhdGUgbW9kZWw6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKG9iajogYW55ID0gbnVsbCkge1xuICAgICAgICBpZiAob2JqKSB0aGlzLmRhdGEgPSBfLmNsb25lKG9iaik7XG4gICAgICAgIHRoaXMubW9kZWwgPSBzZXF1ZWxpemUubW9kZWxzLmh0bWxwYWdlcHY7XG4gICAgfVxuXG4gICAgY3JlYXRlKGRhdGU6IG51bWJlciwgcHY6IG51bWJlciwgaHRtbFBhZ2VJZDogc3RyaW5nKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm1vZGVsLmNyZWF0ZSh7XG4gICAgICAgICAgICBwdjogcHYsXG4gICAgICAgICAgICBkYXRlOiBkYXRlLFxuICAgICAgICAgICAgaHRtbFBhZ2VJZDogaHRtbFBhZ2VJZFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBhc3luYyBhZGRQVihpZDogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IGRhdGU6IG51bWJlciA9IE51bWJlcihtb21lbnQobmV3IERhdGUoKSkuZm9ybWF0KFwiWVlZWU1NRERcIikpO1xuICAgICAgICBsZXQgcXVlcnlIbWxQYWdlUHY6IHN0cmluZyA9IGBzZWxlY3QgaHRtbF9wYWdlX2lkIGFzIGh0bWxQYWdlSWQgZnJvbSBodG1sX3BhZ2VfcHYgd2hlcmUgZGF0ZSA9ICR7ZGF0ZX0gYW5kIGh0bWxfcGFnZV9pZCA9ICcke2lkfScgYW5kIGlzX2RlbGV0ZWQgPTBgO1xuICAgICAgICBsZXQgcmVzdWx0OiBhbnkgPSBhd2FpdCBnbG9iYWxBbnkuZGIucXVlcnkocXVlcnlIbWxQYWdlUHYsIHsgdHlwZTogc2VxdWVsaXplLlF1ZXJ5VHlwZXMuU0VMRUNULCBiaW5kOiB7fSB9KTtcbiAgICAgICAgaWYgKHJlc3VsdC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBsZXQgaHRtbFBhZ2VJZDogbnVtYmVyID0gcmVzdWx0WzBdLmh0bWxQYWdlSWQ7XG4gICAgICAgICAgICBsZXQgdXBkYXRlU3FsOiBzdHJpbmcgPSBgdXBkYXRlIGh0bWxfcGFnZV9wdiBzZXQgcHYgPSBwdiArIDEgd2hlcmUgaHRtbF9wYWdlX2lkID0gJGh0bWxQYWdlSWQgYW5kIGRhdGUgPSAke2RhdGV9OyBgO1xuICAgICAgICAgICAgcmV0dXJuIGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeSh1cGRhdGVTcWwsIHsgdHlwZTogc2VxdWVsaXplLlF1ZXJ5VHlwZXMuVVBEQVRFLCBiaW5kOiB7IGh0bWxQYWdlSWQ6IGh0bWxQYWdlSWQgfSB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBhd2FpdCB0aGlzLmNyZWF0ZShkYXRlLCAxLCBpZCk7XG4gICAgICAgIH1cblxuICAgIH1cblxuICAgIGFzeW5jIGdldEh0bWxQYWdlUHZCeUlkKGlkOiBzdHJpbmcsIHN0YXJ0VGltZTogbnVtYmVyLCBlbmRUaW1lOiBudW1iZXIpIHtcbiAgICAgICAgbGV0IHNxbDogc3RyaW5nID0gYHNlbGVjdCBodG1sX3BhZ2VfaWQgYXMgaHRtbFBhZ2VJZCwgZGF0ZSwgcHYgZnJvbSBodG1sX3BhZ2VfcHYgd2hlcmUgaHRtbF9wYWdlX2lkID0gJyR7aWR9JyBhbmQgaXNfZGVsZXRlZCA9IDBgO1xuICAgICAgICBpZiAoc3RhcnRUaW1lKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgZGF0ZSA+PSAke3N0YXJ0VGltZX1gO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlbmRUaW1lKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgZGF0ZSA8PSAke2VuZFRpbWV9YDtcbiAgICAgICAgfVxuICAgICAgICBzcWwgKz0gYCBvcmRlciBieSBkYXRlO2A7XG4gICAgICAgIHJldHVybiBhd2FpdCBnbG9iYWxBbnkuZGIucXVlcnkoc3FsLCB7IHR5cGU6IHNlcXVlbGl6ZS5RdWVyeVR5cGVzLlNFTEVDVCwgYmluZDoge30gfSk7XG4gICAgfVxufSJdfQ==
