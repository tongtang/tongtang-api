export declare class BannerService {
    private data;
    private model;
    constructor(obj?: any);
    findAll(pageIndex: number, pageSize: number): Promise<{
        count: number;
        rows: any;
    }>;
    create(title: string, type: number, author: string, imageUrl: string, skipLink: string, htmlPageNumber: string): any;
    updateById(id: string, title: string, type: number, author: string, imageUrl: string, skipLink: string, htmlPageNumber: string): any;
    deleteById(id: string): Promise<any>;
}
