"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserHealthInformationService = void 0;
const sql_1 = require("../common/sql");
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny = global;
class UserHealthInformationService {
    constructor(obj = null) {
        if (obj)
            this.data = _.clone(obj);
        this.model = sequelize.models.userHealthInformation;
    }
    findOneById(id) {
        return this.model.findOne({
            attributes: ["id", ["real_name", "realName"], "sex", "age", "telephone", ["id_card", "idCard"], ["health_report_url", "healthReportUrl"], "height", "weight", "waistline", "hipline", "sbp", "dbp", "rbg", "fpg", "hpg", "ua", "tc", "tg", "hdlc", "ldlc", "origin", ["origin_id", "originId"]],
            where: { id: id }
        });
    }
    findOneByOrignId(originId, origin) {
        console.log("orignId:" + originId);
        return this.model.findOne({
            attributes: ["id", ["real_name", "realName"], "sex", "age", "telephone", ["id_card", "idCard"], ["health_report_url", "healthReportUrl"], "height", "weight", "waistline", "hipline", "sbp", "dbp", "rbg", "fpg", "hpg", "ua", "tc", "tg", "hdlc", "ldlc", "origin", ["origin_id", "originId"]],
            where: { origin_id: originId, origin: origin }
        });
    }
    findPCAll(telephone, realName, startTime, endTime, pageIndex, pageSize) {
        return __awaiter(this, void 0, void 0, function* () {
            const sql = new sql_1.SQL();
            let userHealthInformations = [];
            let count = 0;
            let limit = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
            let offset = limit * (Number(pageIndex) - 1);
            count = (yield globalAny.db.query(sql.getUserHealthInformationCount(telephone, realName, startTime, endTime), {
                type: sequelize.QueryTypes.SELECT, bind: {
                    realName: `%${realName}%`,
                    telephone: telephone
                }
            }))[0].count;
            userHealthInformations = yield globalAny.db.query(sql.getUserHealthInformation(telephone, realName, startTime, endTime, offset, limit), {
                type: sequelize.QueryTypes.SELECT, bind: {
                    realName: `%${realName}%`,
                    telephone: telephone
                }
            });
            console.log("userHealthInformations", userHealthInformations);
            return { count: count, rows: userHealthInformations };
        });
    }
    create(realName, sex, age, telephone, idCard, healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, originId) {
        return this.model.create({
            realName: realName,
            sex: sex,
            age: age,
            telephone: telephone,
            idCard: idCard,
            healthReportUrl: healthReportUrl,
            height: height,
            weight: weight,
            waistline: waistline,
            hipline: hipline,
            sbp: sbp,
            dbp: dbp,
            rbg: rbg,
            fpg: fpg,
            hpg: hpg,
            ua: ua,
            tc: tc,
            tg: tg,
            hdlc: hdlc,
            ldlc: ldlc,
            origin: origin,
            originId: originId,
        });
    }
    updateById(id, realName, sex, age, telephone, idCard, healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, originId) {
        return this.model.update({
            realName: realName,
            sex: sex,
            age: age,
            telephone: telephone,
            idCard: idCard,
            healthReportUrl: healthReportUrl,
            height: height,
            weight: weight,
            waistline: waistline,
            hipline: hipline,
            sbp: sbp,
            dbp: dbp,
            rbg: rbg,
            fpg: fpg,
            hpg: hpg,
            ua: ua,
            tc: tc,
            tg: tg,
            hdlc: hdlc,
            ldlc: ldlc,
            origin: origin,
            originId: originId
        }, { where: { id: id } });
    }
    deleteById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `update user_health_information set is_deleted = 1 where id = $id`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
        });
    }
    export(startTime, endTime) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `select real_name as realName, sex, age, telephone, id_card as  idCard, health_report_url as healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, created_at as createdAt from user_health_information where is_deleted = 0`;
            if (startTime > 1) {
                sql += ` and   created_at >= $startTime`;
            }
            if (endTime > 1) {
                sql += ` and created_at <= $endTime`;
            }
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { startTime: startTime, endTime: endTime } });
        });
    }
}
exports.UserHealthInformationService = UserHealthInformationService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UvdXNlci1oZWFsdGgtaW5mb3JtYXRpb24tc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBb0M7QUFDcEMsTUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3ZDLElBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMxQixNQUFNLFNBQVMsR0FBUSxNQUFNLENBQUM7QUFFOUIsTUFBYSw0QkFBNEI7SUFJckMsWUFBWSxNQUFXLElBQUk7UUFDdkIsSUFBSSxHQUFHO1lBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQztJQUN4RCxDQUFDO0lBRUQsV0FBVyxDQUFDLEVBQVU7UUFDbEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztZQUN0QixVQUFVLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQUUsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxpQkFBaUIsQ0FBQyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQy9SLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUU7U0FDcEIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdCQUFnQixDQUFDLFFBQWdCLEVBQUUsTUFBYztRQUM3QyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQztRQUNuQyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBQ3RCLFVBQVUsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLG1CQUFtQixFQUFFLGlCQUFpQixDQUFDLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDL1IsS0FBSyxFQUFFLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFO1NBQ2pELENBQUMsQ0FBQztJQUNQLENBQUM7SUFFSyxTQUFTLENBQUMsU0FBaUIsRUFBRSxRQUFnQixFQUFFLFNBQWlCLEVBQUUsT0FBZSxFQUFFLFNBQWlCLEVBQUUsUUFBZ0I7O1lBQ3hILE1BQU0sR0FBRyxHQUFHLElBQUksU0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxzQkFBc0IsR0FBRyxFQUFFLENBQUM7WUFDaEMsSUFBSSxLQUFLLEdBQVcsQ0FBQyxDQUFDO1lBQ3RCLElBQUksS0FBSyxHQUFXLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlGLElBQUksTUFBTSxHQUFXLEtBQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNyRCxLQUFLLEdBQUcsQ0FBQyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDMUcsSUFBSSxFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRTtvQkFDckMsUUFBUSxFQUFFLElBQUksUUFBUSxHQUFHO29CQUN6QixTQUFTLEVBQUUsU0FBUztpQkFDdkI7YUFDSixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDYixzQkFBc0IsR0FBRyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNwSSxJQUFJLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFO29CQUNyQyxRQUFRLEVBQUUsSUFBSSxRQUFRLEdBQUc7b0JBQ3pCLFNBQVMsRUFBRSxTQUFTO2lCQUN2QjthQUNKLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztZQUM5RCxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsc0JBQXNCLEVBQUUsQ0FBQztRQUMxRCxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQUMsUUFBZ0IsRUFBRSxHQUFXLEVBQUUsR0FBVyxFQUFFLFNBQWlCLEVBQUUsTUFBYyxFQUFFLGVBQXVCLEVBQUUsTUFBYyxFQUFFLE1BQWMsRUFBRSxTQUFpQixFQUFFLE9BQWUsRUFBRSxHQUFXLEVBQUUsR0FBVyxFQUFFLEdBQVcsRUFBRSxHQUFXLEVBQUUsR0FBVyxFQUFFLEVBQVUsRUFBRSxFQUFVLEVBQUUsRUFBVSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsTUFBYyxFQUFFLFFBQWdCO1FBQ2hWLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDckIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsR0FBRyxFQUFFLEdBQUc7WUFDUixHQUFHLEVBQUUsR0FBRztZQUNSLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLE1BQU0sRUFBRSxNQUFNO1lBQ2QsZUFBZSxFQUFFLGVBQWU7WUFDaEMsTUFBTSxFQUFFLE1BQU07WUFDZCxNQUFNLEVBQUUsTUFBTTtZQUNkLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLEdBQUcsRUFBRSxHQUFHO1lBQ1IsR0FBRyxFQUFFLEdBQUc7WUFDUixHQUFHLEVBQUUsR0FBRztZQUNSLEdBQUcsRUFBRSxHQUFHO1lBQ1IsR0FBRyxFQUFFLEdBQUc7WUFDUixFQUFFLEVBQUUsRUFBRTtZQUNOLEVBQUUsRUFBRSxFQUFFO1lBQ04sRUFBRSxFQUFFLEVBQUU7WUFDTixJQUFJLEVBQUUsSUFBSTtZQUNWLElBQUksRUFBRSxJQUFJO1lBQ1YsTUFBTSxFQUFFLE1BQU07WUFDZCxRQUFRLEVBQUUsUUFBUTtTQUVyQixDQUFDLENBQUM7SUFDUCxDQUFDO0lBSUQsVUFBVSxDQUFDLEVBQVUsRUFBRSxRQUFnQixFQUFFLEdBQVcsRUFBRSxHQUFXLEVBQUUsU0FBaUIsRUFBRSxNQUFjLEVBQUUsZUFBdUIsRUFBRSxNQUFjLEVBQUUsTUFBYyxFQUFFLFNBQWlCLEVBQUUsT0FBZSxFQUFFLEdBQVcsRUFBRSxHQUFXLEVBQUUsR0FBVyxFQUFFLEdBQVcsRUFBRSxHQUFXLEVBQUUsRUFBVSxFQUFFLEVBQVUsRUFBRSxFQUFVLEVBQUUsSUFBWSxFQUFFLElBQVksRUFBRSxNQUFjLEVBQUUsUUFBZ0I7UUFDaFcsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUNyQixRQUFRLEVBQUUsUUFBUTtZQUNsQixHQUFHLEVBQUUsR0FBRztZQUNSLEdBQUcsRUFBRSxHQUFHO1lBQ1IsU0FBUyxFQUFFLFNBQVM7WUFDcEIsTUFBTSxFQUFFLE1BQU07WUFDZCxlQUFlLEVBQUUsZUFBZTtZQUNoQyxNQUFNLEVBQUUsTUFBTTtZQUNkLE1BQU0sRUFBRSxNQUFNO1lBQ2QsU0FBUyxFQUFFLFNBQVM7WUFDcEIsT0FBTyxFQUFFLE9BQU87WUFDaEIsR0FBRyxFQUFFLEdBQUc7WUFDUixHQUFHLEVBQUUsR0FBRztZQUNSLEdBQUcsRUFBRSxHQUFHO1lBQ1IsR0FBRyxFQUFFLEdBQUc7WUFDUixHQUFHLEVBQUUsR0FBRztZQUNSLEVBQUUsRUFBRSxFQUFFO1lBQ04sRUFBRSxFQUFFLEVBQUU7WUFDTixFQUFFLEVBQUUsRUFBRTtZQUNOLElBQUksRUFBRSxJQUFJO1lBQ1YsSUFBSSxFQUFFLElBQUk7WUFDVixNQUFNLEVBQUUsTUFBTTtZQUNkLFFBQVEsRUFBRSxRQUFRO1NBQ3JCLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFSyxVQUFVLENBQUMsRUFBVTs7WUFDdkIsSUFBSSxHQUFHLEdBQVcsa0VBQWtFLENBQUM7WUFDckYsT0FBTyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ2xHLENBQUM7S0FBQTtJQUVLLE1BQU0sQ0FBQyxTQUFpQixFQUFFLE9BQWU7O1lBQzNDLElBQUksR0FBRyxHQUFXLHFSQUFxUixDQUFDO1lBQ3hTLElBQUksU0FBUyxHQUFHLENBQUMsRUFBRTtnQkFDZixHQUFHLElBQUksaUNBQWlDLENBQUM7YUFDNUM7WUFDRCxJQUFJLE9BQU8sR0FBRyxDQUFDLEVBQUU7Z0JBQ2IsR0FBRyxJQUFJLDZCQUE2QixDQUFDO2FBQ3hDO1lBQ0QsT0FBTyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDbEksQ0FBQztLQUFBO0NBQ0o7QUF0SEQsb0VBc0hDIiwiZmlsZSI6InNlcnZpY2UvdXNlci1oZWFsdGgtaW5mb3JtYXRpb24tc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNRTCB9IGZyb20gXCIuLi9jb21tb24vc3FsXCI7XG5jb25zdCBzZXF1ZWxpemUgPSByZXF1aXJlKFwic2VxdWVsaXplXCIpO1xubGV0IF8gPSByZXF1aXJlKFwibG9kYXNoXCIpO1xuY29uc3QgZ2xvYmFsQW55OiBhbnkgPSBnbG9iYWw7XG5cbmV4cG9ydCBjbGFzcyBVc2VySGVhbHRoSW5mb3JtYXRpb25TZXJ2aWNlIHtcbiAgICBwcml2YXRlIGRhdGE6IGFueTtcbiAgICBwcml2YXRlIG1vZGVsOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihvYmo6IGFueSA9IG51bGwpIHtcbiAgICAgICAgaWYgKG9iaikgdGhpcy5kYXRhID0gXy5jbG9uZShvYmopO1xuICAgICAgICB0aGlzLm1vZGVsID0gc2VxdWVsaXplLm1vZGVscy51c2VySGVhbHRoSW5mb3JtYXRpb247XG4gICAgfVxuXG4gICAgZmluZE9uZUJ5SWQoaWQ6IHN0cmluZykge1xuICAgICAgICByZXR1cm4gdGhpcy5tb2RlbC5maW5kT25lKHtcbiAgICAgICAgICAgIGF0dHJpYnV0ZXM6IFtcImlkXCIsIFtcInJlYWxfbmFtZVwiLCBcInJlYWxOYW1lXCJdLCBcInNleFwiLCBcImFnZVwiLCBcInRlbGVwaG9uZVwiLCBbXCJpZF9jYXJkXCIsIFwiaWRDYXJkXCJdLCBbXCJoZWFsdGhfcmVwb3J0X3VybFwiLCBcImhlYWx0aFJlcG9ydFVybFwiXSwgXCJoZWlnaHRcIiwgXCJ3ZWlnaHRcIiwgXCJ3YWlzdGxpbmVcIiwgXCJoaXBsaW5lXCIsIFwic2JwXCIsIFwiZGJwXCIsIFwicmJnXCIsIFwiZnBnXCIsIFwiaHBnXCIsIFwidWFcIiwgXCJ0Y1wiLCBcInRnXCIsIFwiaGRsY1wiLCBcImxkbGNcIiwgXCJvcmlnaW5cIiwgW1wib3JpZ2luX2lkXCIsIFwib3JpZ2luSWRcIl1dLFxuICAgICAgICAgICAgd2hlcmU6IHsgaWQ6IGlkIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZmluZE9uZUJ5T3JpZ25JZChvcmlnaW5JZDogc3RyaW5nLCBvcmlnaW46IG51bWJlcikge1xuICAgICAgICBjb25zb2xlLmxvZyhcIm9yaWduSWQ6XCIgKyBvcmlnaW5JZCk7XG4gICAgICAgIHJldHVybiB0aGlzLm1vZGVsLmZpbmRPbmUoe1xuICAgICAgICAgICAgYXR0cmlidXRlczogW1wiaWRcIiwgW1wicmVhbF9uYW1lXCIsIFwicmVhbE5hbWVcIl0sIFwic2V4XCIsIFwiYWdlXCIsIFwidGVsZXBob25lXCIsIFtcImlkX2NhcmRcIiwgXCJpZENhcmRcIl0sIFtcImhlYWx0aF9yZXBvcnRfdXJsXCIsIFwiaGVhbHRoUmVwb3J0VXJsXCJdLCBcImhlaWdodFwiLCBcIndlaWdodFwiLCBcIndhaXN0bGluZVwiLCBcImhpcGxpbmVcIiwgXCJzYnBcIiwgXCJkYnBcIiwgXCJyYmdcIiwgXCJmcGdcIiwgXCJocGdcIiwgXCJ1YVwiLCBcInRjXCIsIFwidGdcIiwgXCJoZGxjXCIsIFwibGRsY1wiLCBcIm9yaWdpblwiLCBbXCJvcmlnaW5faWRcIiwgXCJvcmlnaW5JZFwiXV0sXG4gICAgICAgICAgICB3aGVyZTogeyBvcmlnaW5faWQ6IG9yaWdpbklkLCBvcmlnaW46IG9yaWdpbiB9XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFzeW5jIGZpbmRQQ0FsbCh0ZWxlcGhvbmU6IHN0cmluZywgcmVhbE5hbWU6IHN0cmluZywgc3RhcnRUaW1lOiBudW1iZXIsIGVuZFRpbWU6IG51bWJlciwgcGFnZUluZGV4OiBudW1iZXIsIHBhZ2VTaXplOiBudW1iZXIpIHtcbiAgICAgICAgY29uc3Qgc3FsID0gbmV3IFNRTCgpO1xuICAgICAgICBsZXQgdXNlckhlYWx0aEluZm9ybWF0aW9ucyA9IFtdO1xuICAgICAgICBsZXQgY291bnQ6IG51bWJlciA9IDA7XG4gICAgICAgIGxldCBsaW1pdDogbnVtYmVyID0gKE51bWJlcihwYWdlU2l6ZSkgPT09IDAgfHwgTnVtYmVyKHBhZ2VTaXplKSA+IDUwKSA/IDUwIDogTnVtYmVyKHBhZ2VTaXplKTtcbiAgICAgICAgbGV0IG9mZnNldDogbnVtYmVyID0gbGltaXQgKiAoTnVtYmVyKHBhZ2VJbmRleCkgLSAxKTtcbiAgICAgICAgY291bnQgPSAoYXdhaXQgZ2xvYmFsQW55LmRiLnF1ZXJ5KHNxbC5nZXRVc2VySGVhbHRoSW5mb3JtYXRpb25Db3VudCh0ZWxlcGhvbmUsIHJlYWxOYW1lLCBzdGFydFRpbWUsIGVuZFRpbWUpLCB7XG4gICAgICAgICAgICB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHtcbiAgICAgICAgICAgICAgICByZWFsTmFtZTogYCUke3JlYWxOYW1lfSVgLFxuICAgICAgICAgICAgICAgIHRlbGVwaG9uZTogdGVsZXBob25lXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pKVswXS5jb3VudDtcbiAgICAgICAgdXNlckhlYWx0aEluZm9ybWF0aW9ucyA9IGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeShzcWwuZ2V0VXNlckhlYWx0aEluZm9ybWF0aW9uKHRlbGVwaG9uZSwgcmVhbE5hbWUsIHN0YXJ0VGltZSwgZW5kVGltZSwgb2Zmc2V0LCBsaW1pdCksIHtcbiAgICAgICAgICAgIHR5cGU6IHNlcXVlbGl6ZS5RdWVyeVR5cGVzLlNFTEVDVCwgYmluZDoge1xuICAgICAgICAgICAgICAgIHJlYWxOYW1lOiBgJSR7cmVhbE5hbWV9JWAsXG4gICAgICAgICAgICAgICAgdGVsZXBob25lOiB0ZWxlcGhvbmVcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwidXNlckhlYWx0aEluZm9ybWF0aW9uc1wiLCB1c2VySGVhbHRoSW5mb3JtYXRpb25zKTtcbiAgICAgICAgcmV0dXJuIHsgY291bnQ6IGNvdW50LCByb3dzOiB1c2VySGVhbHRoSW5mb3JtYXRpb25zIH07XG4gICAgfVxuXG4gICAgY3JlYXRlKHJlYWxOYW1lOiBzdHJpbmcsIHNleDogbnVtYmVyLCBhZ2U6IG51bWJlciwgdGVsZXBob25lOiBzdHJpbmcsIGlkQ2FyZDogc3RyaW5nLCBoZWFsdGhSZXBvcnRVcmw6IHN0cmluZywgaGVpZ2h0OiBudW1iZXIsIHdlaWdodDogbnVtYmVyLCB3YWlzdGxpbmU6IG51bWJlciwgaGlwbGluZTogbnVtYmVyLCBzYnA6IG51bWJlciwgZGJwOiBudW1iZXIsIHJiZzogbnVtYmVyLCBmcGc6IG51bWJlciwgaHBnOiBudW1iZXIsIHVhOiBudW1iZXIsIHRjOiBudW1iZXIsIHRnOiBudW1iZXIsIGhkbGM6IG51bWJlciwgbGRsYzogbnVtYmVyLCBvcmlnaW46IG51bWJlciwgb3JpZ2luSWQ6IHN0cmluZykge1xuICAgICAgICByZXR1cm4gdGhpcy5tb2RlbC5jcmVhdGUoe1xuICAgICAgICAgICAgcmVhbE5hbWU6IHJlYWxOYW1lLFxuICAgICAgICAgICAgc2V4OiBzZXgsXG4gICAgICAgICAgICBhZ2U6IGFnZSxcbiAgICAgICAgICAgIHRlbGVwaG9uZTogdGVsZXBob25lLFxuICAgICAgICAgICAgaWRDYXJkOiBpZENhcmQsXG4gICAgICAgICAgICBoZWFsdGhSZXBvcnRVcmw6IGhlYWx0aFJlcG9ydFVybCxcbiAgICAgICAgICAgIGhlaWdodDogaGVpZ2h0LFxuICAgICAgICAgICAgd2VpZ2h0OiB3ZWlnaHQsXG4gICAgICAgICAgICB3YWlzdGxpbmU6IHdhaXN0bGluZSxcbiAgICAgICAgICAgIGhpcGxpbmU6IGhpcGxpbmUsXG4gICAgICAgICAgICBzYnA6IHNicCxcbiAgICAgICAgICAgIGRicDogZGJwLFxuICAgICAgICAgICAgcmJnOiByYmcsXG4gICAgICAgICAgICBmcGc6IGZwZyxcbiAgICAgICAgICAgIGhwZzogaHBnLFxuICAgICAgICAgICAgdWE6IHVhLFxuICAgICAgICAgICAgdGM6IHRjLFxuICAgICAgICAgICAgdGc6IHRnLFxuICAgICAgICAgICAgaGRsYzogaGRsYyxcbiAgICAgICAgICAgIGxkbGM6IGxkbGMsXG4gICAgICAgICAgICBvcmlnaW46IG9yaWdpbixcbiAgICAgICAgICAgIG9yaWdpbklkOiBvcmlnaW5JZCxcblxuICAgICAgICB9KTtcbiAgICB9XG5cblxuXG4gICAgdXBkYXRlQnlJZChpZDogc3RyaW5nLCByZWFsTmFtZTogc3RyaW5nLCBzZXg6IG51bWJlciwgYWdlOiBudW1iZXIsIHRlbGVwaG9uZTogc3RyaW5nLCBpZENhcmQ6IHN0cmluZywgaGVhbHRoUmVwb3J0VXJsOiBzdHJpbmcsIGhlaWdodDogbnVtYmVyLCB3ZWlnaHQ6IG51bWJlciwgd2Fpc3RsaW5lOiBudW1iZXIsIGhpcGxpbmU6IG51bWJlciwgc2JwOiBudW1iZXIsIGRicDogbnVtYmVyLCByYmc6IG51bWJlciwgZnBnOiBudW1iZXIsIGhwZzogbnVtYmVyLCB1YTogbnVtYmVyLCB0YzogbnVtYmVyLCB0ZzogbnVtYmVyLCBoZGxjOiBudW1iZXIsIGxkbGM6IG51bWJlciwgb3JpZ2luOiBudW1iZXIsIG9yaWdpbklkOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwudXBkYXRlKHtcbiAgICAgICAgICAgIHJlYWxOYW1lOiByZWFsTmFtZSxcbiAgICAgICAgICAgIHNleDogc2V4LFxuICAgICAgICAgICAgYWdlOiBhZ2UsXG4gICAgICAgICAgICB0ZWxlcGhvbmU6IHRlbGVwaG9uZSxcbiAgICAgICAgICAgIGlkQ2FyZDogaWRDYXJkLFxuICAgICAgICAgICAgaGVhbHRoUmVwb3J0VXJsOiBoZWFsdGhSZXBvcnRVcmwsXG4gICAgICAgICAgICBoZWlnaHQ6IGhlaWdodCxcbiAgICAgICAgICAgIHdlaWdodDogd2VpZ2h0LFxuICAgICAgICAgICAgd2Fpc3RsaW5lOiB3YWlzdGxpbmUsXG4gICAgICAgICAgICBoaXBsaW5lOiBoaXBsaW5lLFxuICAgICAgICAgICAgc2JwOiBzYnAsXG4gICAgICAgICAgICBkYnA6IGRicCxcbiAgICAgICAgICAgIHJiZzogcmJnLFxuICAgICAgICAgICAgZnBnOiBmcGcsXG4gICAgICAgICAgICBocGc6IGhwZyxcbiAgICAgICAgICAgIHVhOiB1YSxcbiAgICAgICAgICAgIHRjOiB0YyxcbiAgICAgICAgICAgIHRnOiB0ZyxcbiAgICAgICAgICAgIGhkbGM6IGhkbGMsXG4gICAgICAgICAgICBsZGxjOiBsZGxjLFxuICAgICAgICAgICAgb3JpZ2luOiBvcmlnaW4sXG4gICAgICAgICAgICBvcmlnaW5JZDogb3JpZ2luSWRcbiAgICAgICAgfSwgeyB3aGVyZTogeyBpZDogaWQgfSB9KTtcbiAgICB9XG5cbiAgICBhc3luYyBkZWxldGVCeUlkKGlkOiBzdHJpbmcpIHtcbiAgICAgICAgbGV0IHNxbDogc3RyaW5nID0gYHVwZGF0ZSB1c2VyX2hlYWx0aF9pbmZvcm1hdGlvbiBzZXQgaXNfZGVsZXRlZCA9IDEgd2hlcmUgaWQgPSAkaWRgO1xuICAgICAgICByZXR1cm4gYXdhaXQgZ2xvYmFsQW55LmRiLnF1ZXJ5KHNxbCwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHsgaWQ6IGlkIH0gfSk7XG4gICAgfVxuXG4gICAgYXN5bmMgZXhwb3J0KHN0YXJ0VGltZTogbnVtYmVyLCBlbmRUaW1lOiBudW1iZXIpIHtcbiAgICAgICAgbGV0IHNxbDogc3RyaW5nID0gYHNlbGVjdCByZWFsX25hbWUgYXMgcmVhbE5hbWUsIHNleCwgYWdlLCB0ZWxlcGhvbmUsIGlkX2NhcmQgYXMgIGlkQ2FyZCwgaGVhbHRoX3JlcG9ydF91cmwgYXMgaGVhbHRoUmVwb3J0VXJsLCBoZWlnaHQsIHdlaWdodCwgd2Fpc3RsaW5lLCBoaXBsaW5lLCBzYnAsIGRicCwgcmJnLCBmcGcsIGhwZywgdWEsIHRjLCB0ZywgaGRsYywgbGRsYywgb3JpZ2luLCBjcmVhdGVkX2F0IGFzIGNyZWF0ZWRBdCBmcm9tIHVzZXJfaGVhbHRoX2luZm9ybWF0aW9uIHdoZXJlIGlzX2RlbGV0ZWQgPSAwYDtcbiAgICAgICAgaWYgKHN0YXJ0VGltZSA+IDEpIHtcbiAgICAgICAgICAgIHNxbCArPSBgIGFuZCAgIGNyZWF0ZWRfYXQgPj0gJHN0YXJ0VGltZWA7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGVuZFRpbWUgPiAxKSB7XG4gICAgICAgICAgICBzcWwgKz0gYCBhbmQgY3JlYXRlZF9hdCA8PSAkZW5kVGltZWA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeShzcWwsIHsgdHlwZTogc2VxdWVsaXplLlF1ZXJ5VHlwZXMuU0VMRUNULCBiaW5kOiB7IHN0YXJ0VGltZTogc3RhcnRUaW1lLCBlbmRUaW1lOiBlbmRUaW1lIH0gfSk7XG4gICAgfVxufSJdfQ==
