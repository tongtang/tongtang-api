"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BannerService = void 0;
const sql_1 = require("../common/sql");
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny = global;
class BannerService {
    constructor(obj = null) {
        if (obj)
            this.data = _.clone(obj);
        this.model = sequelize.models.banner;
    }
    findAll(pageIndex, pageSize) {
        return __awaiter(this, void 0, void 0, function* () {
            const sql = new sql_1.SQL();
            let banners = [];
            let count = 0;
            let limit = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
            let offset = limit * (Number(pageIndex) - 1);
            count = (yield globalAny.db.query(sql.getBannerCount(), { type: sequelize.QueryTypes.SELECT }))[0].count;
            banners = yield globalAny.db.query(sql.getBanner(offset, limit), { type: sequelize.QueryTypes.SELECT });
            return { count: count, rows: banners };
        });
    }
    create(title, type, author, imageUrl, skipLink, htmlPageNumber) {
        return this.model.create({
            title: title,
            type: type,
            author: author,
            imageUrl: imageUrl,
            skipLink: skipLink,
            htmlPageNumber: htmlPageNumber
        });
    }
    updateById(id, title, type, author, imageUrl, skipLink, htmlPageNumber) {
        return this.model.update({
            title: title,
            type: type,
            author: author,
            imageUrl: imageUrl,
            skipLink: skipLink,
            htmlPageNumber: htmlPageNumber
        }, { where: { id: id } });
    }
    deleteById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let sql = `update banner set is_deleted = 1 where id = $id`;
            return yield globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
        });
    }
}
exports.BannerService = BannerService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlcnZpY2UvYmFubmVyc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBb0M7QUFDcEMsTUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3ZDLElBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMxQixNQUFNLFNBQVMsR0FBUSxNQUFNLENBQUM7QUFFOUIsTUFBYSxhQUFhO0lBSXRCLFlBQVksTUFBVyxJQUFJO1FBQ3ZCLElBQUksR0FBRztZQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ3pDLENBQUM7SUFFSyxPQUFPLENBQUMsU0FBaUIsRUFBRSxRQUFnQjs7WUFDN0MsTUFBTSxHQUFHLEdBQUcsSUFBSSxTQUFHLEVBQUUsQ0FBQztZQUN0QixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDakIsSUFBSSxLQUFLLEdBQVcsQ0FBQyxDQUFDO1lBQ3RCLElBQUksS0FBSyxHQUFXLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlGLElBQUksTUFBTSxHQUFXLEtBQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNyRCxLQUFLLEdBQUcsQ0FBQyxNQUFNLFNBQVMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDekcsT0FBTyxHQUFHLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1lBQ3hHLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsQ0FBQztRQUMzQyxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQUMsS0FBYSxFQUFFLElBQVksRUFBRSxNQUFjLEVBQUUsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLGNBQXNCO1FBQzFHLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDckIsS0FBSyxFQUFFLEtBQUs7WUFDWixJQUFJLEVBQUUsSUFBSTtZQUNWLE1BQU0sRUFBRSxNQUFNO1lBQ2QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsY0FBYyxFQUFFLGNBQWM7U0FDakMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFVBQVUsQ0FBQyxFQUFVLEVBQUUsS0FBYSxFQUFFLElBQVksRUFBRSxNQUFjLEVBQUUsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLGNBQXNCO1FBQzFILE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDckIsS0FBSyxFQUFFLEtBQUs7WUFDWixJQUFJLEVBQUUsSUFBSTtZQUNWLE1BQU0sRUFBRSxNQUFNO1lBQ2QsUUFBUSxFQUFFLFFBQVE7WUFDbEIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsY0FBYyxFQUFFLGNBQWM7U0FDakMsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVLLFVBQVUsQ0FBQyxFQUFVOztZQUN2QixJQUFJLEdBQUcsR0FBVyxpREFBaUQsQ0FBQztZQUNwRSxPQUFPLE1BQU0sU0FBUyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDbEcsQ0FBQztLQUFBO0NBRUo7QUEvQ0Qsc0NBK0NDIiwiZmlsZSI6InNlcnZpY2UvYmFubmVyc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNRTCB9IGZyb20gXCIuLi9jb21tb24vc3FsXCI7XG5jb25zdCBzZXF1ZWxpemUgPSByZXF1aXJlKFwic2VxdWVsaXplXCIpO1xubGV0IF8gPSByZXF1aXJlKFwibG9kYXNoXCIpO1xuY29uc3QgZ2xvYmFsQW55OiBhbnkgPSBnbG9iYWw7XG5cbmV4cG9ydCBjbGFzcyBCYW5uZXJTZXJ2aWNlIHtcbiAgICBwcml2YXRlIGRhdGE6IGFueTtcbiAgICBwcml2YXRlIG1vZGVsOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihvYmo6IGFueSA9IG51bGwpIHtcbiAgICAgICAgaWYgKG9iaikgdGhpcy5kYXRhID0gXy5jbG9uZShvYmopO1xuICAgICAgICB0aGlzLm1vZGVsID0gc2VxdWVsaXplLm1vZGVscy5iYW5uZXI7XG4gICAgfVxuXG4gICAgYXN5bmMgZmluZEFsbChwYWdlSW5kZXg6IG51bWJlciwgcGFnZVNpemU6IG51bWJlcikge1xuICAgICAgICBjb25zdCBzcWwgPSBuZXcgU1FMKCk7XG4gICAgICAgIGxldCBiYW5uZXJzID0gW107XG4gICAgICAgIGxldCBjb3VudDogbnVtYmVyID0gMDtcbiAgICAgICAgbGV0IGxpbWl0OiBudW1iZXIgPSAoTnVtYmVyKHBhZ2VTaXplKSA9PT0gMCB8fCBOdW1iZXIocGFnZVNpemUpID4gNTApID8gNTAgOiBOdW1iZXIocGFnZVNpemUpO1xuICAgICAgICBsZXQgb2Zmc2V0OiBudW1iZXIgPSBsaW1pdCAqIChOdW1iZXIocGFnZUluZGV4KSAtIDEpO1xuICAgICAgICBjb3VudCA9IChhd2FpdCBnbG9iYWxBbnkuZGIucXVlcnkoc3FsLmdldEJhbm5lckNvdW50KCksIHsgdHlwZTogc2VxdWVsaXplLlF1ZXJ5VHlwZXMuU0VMRUNUIH0pKVswXS5jb3VudDtcbiAgICAgICAgYmFubmVycyA9IGF3YWl0IGdsb2JhbEFueS5kYi5xdWVyeShzcWwuZ2V0QmFubmVyKG9mZnNldCwgbGltaXQpLCB7IHR5cGU6IHNlcXVlbGl6ZS5RdWVyeVR5cGVzLlNFTEVDVCB9KTtcbiAgICAgICAgcmV0dXJuIHsgY291bnQ6IGNvdW50LCByb3dzOiBiYW5uZXJzIH07XG4gICAgfVxuXG4gICAgY3JlYXRlKHRpdGxlOiBzdHJpbmcsIHR5cGU6IG51bWJlciwgYXV0aG9yOiBzdHJpbmcsIGltYWdlVXJsOiBzdHJpbmcsIHNraXBMaW5rOiBzdHJpbmcsIGh0bWxQYWdlTnVtYmVyOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwuY3JlYXRlKHtcbiAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcbiAgICAgICAgICAgIHR5cGU6IHR5cGUsXG4gICAgICAgICAgICBhdXRob3I6IGF1dGhvcixcbiAgICAgICAgICAgIGltYWdlVXJsOiBpbWFnZVVybCxcbiAgICAgICAgICAgIHNraXBMaW5rOiBza2lwTGluayxcbiAgICAgICAgICAgIGh0bWxQYWdlTnVtYmVyOiBodG1sUGFnZU51bWJlclxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICB1cGRhdGVCeUlkKGlkOiBzdHJpbmcsIHRpdGxlOiBzdHJpbmcsIHR5cGU6IG51bWJlciwgYXV0aG9yOiBzdHJpbmcsIGltYWdlVXJsOiBzdHJpbmcsIHNraXBMaW5rOiBzdHJpbmcsIGh0bWxQYWdlTnVtYmVyOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubW9kZWwudXBkYXRlKHtcbiAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcbiAgICAgICAgICAgIHR5cGU6IHR5cGUsXG4gICAgICAgICAgICBhdXRob3I6IGF1dGhvcixcbiAgICAgICAgICAgIGltYWdlVXJsOiBpbWFnZVVybCxcbiAgICAgICAgICAgIHNraXBMaW5rOiBza2lwTGluayxcbiAgICAgICAgICAgIGh0bWxQYWdlTnVtYmVyOiBodG1sUGFnZU51bWJlclxuICAgICAgICB9LCB7IHdoZXJlOiB7IGlkOiBpZCB9IH0pO1xuICAgIH1cblxuICAgIGFzeW5jIGRlbGV0ZUJ5SWQoaWQ6IHN0cmluZykge1xuICAgICAgICBsZXQgc3FsOiBzdHJpbmcgPSBgdXBkYXRlIGJhbm5lciBzZXQgaXNfZGVsZXRlZCA9IDEgd2hlcmUgaWQgPSAkaWRgO1xuICAgICAgICByZXR1cm4gYXdhaXQgZ2xvYmFsQW55LmRiLnF1ZXJ5KHNxbCwgeyB0eXBlOiBzZXF1ZWxpemUuUXVlcnlUeXBlcy5TRUxFQ1QsIGJpbmQ6IHsgaWQ6IGlkIH0gfSk7XG4gICAgfVxuXG59Il19
