import * as express from "express";
import {OssController} from "../../controller/osscontroller";
import {UserController} from "../../controller/usercontroller";
import {BannerController} from "../../controller/bannercontroller";
import {HtmlPageController} from "../../controller/htmlpagecontroller";
import {HtmlTypeController} from "../../controller/htmltypecontroller";
import {UserHealthInformationController} from "../../controller/user-health-information-controller";


let v1: express.Router = express.Router();
// userHealthInformation
v1.get("/userHealthInformation/id", UserHealthInformationController.findOneById);
v1.get("/userHealthInformation/originId", UserHealthInformationController.findOneByOrignId);
v1.post("/userHealthInformation/create", UserHealthInformationController.create);
v1.post("/userHealthInformation/update/id", UserHealthInformationController.updateById);

// htmlpage
v1.get("/htmlpages", HtmlPageController.findAll);
v1.get("/htmlpage/id", HtmlPageController.findOneById);
v1.get("/htmlpage/pagenumber", HtmlPageController.findOneByPageNumber);
v1.get("/htmlpage/codelist", HtmlTypeController.findAll);

// htmltype
v1.get("/htmltypes", HtmlTypeController.findAll);

// oss
v1.get("/ossToken", OssController.getOssToken);


export {v1};

