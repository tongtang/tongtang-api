import * as express from "express";
import {OssController} from "../../controller/osscontroller";
import {UserController} from "../../controller/usercontroller";
import {BannerController} from "../../controller/bannercontroller";
import {HtmlPageController} from "../../controller/htmlpagecontroller";
import {HtmlPagePvController} from "../../controller/htmlpagepvcontroller";
import {HtmlTypeController} from "../../controller/htmltypecontroller";
import {UserHealthInformationController} from "../../controller/user-health-information-controller";
import { v1 } from "../../../dist/routers/api/v1";
let multer = require("multer");
// 上传文件目录
const UPLOAD_IMAGE_RESOURCE_MIDDLEWARE = multer({
  dest: "upload",
  limits: {fieldSize: 536870912000}
});

let p1: express.Router = express.Router();
// user
p1.get("/users", UserController.findAll);
p1.post("/user/login", UserController.login);
p1.post("/user/logout", UserController.logout);

// userHealthInformation
p1.get("/userHealthInformations", UserHealthInformationController.findPCAll);
p1.get("/userHealthInformation/id", UserHealthInformationController.findOneById);
p1.get("/userHealthInformation/originId", UserHealthInformationController.findOneByOrignId);
p1.post("/userHealthInformation/create", UserHealthInformationController.create);
p1.post("/userHealthInformation/update/id", UserHealthInformationController.updateById);
p1.post("/userHealthInformation/del/id", UserHealthInformationController.deleteById);
p1.get("/userHealthInformation/export", UserHealthInformationController.export);

// htmlpage
p1.get("/htmlpages", HtmlPageController.findPCAll);
p1.get("/htmlpage/id", HtmlPageController.findPCOneById);
p1.get("/htmlpage/pagenumber", HtmlPageController.findOneByPageNumber);
p1.post("/htmlpage/create", HtmlPageController.create);
p1.post("/htmlpage/update/id", HtmlPageController.updateById);
p1.post("/htmlppage/del/id", HtmlPageController.deleteById);
p1.get("/htmlpage/codelist", HtmlPageController.getHtmlPageCodeList);
p1.post("/htmlpage/isOpen", HtmlPageController.isOpen);
p1.get("/htmlpage/pv/export", HtmlPageController.export);

// htmltype
p1.get("/htmltypes", HtmlTypeController.findAll);
p1.get("/htmltype/id", HtmlTypeController.findOneById);
p1.post("/htmltype/create", HtmlTypeController.create);
p1.post("/htmltype/update/id", HtmlTypeController.updateById);
p1.post("/htmltype/isOpen", HtmlTypeController.isOpen);

// htmlpagepv
p1.get("/htmlpagepv/pv/id", HtmlPagePvController.getHtmlPagePvById);


// oss
p1.get("/ossToken", OssController.getOssToken);

// division
p1.post("/cleanDivision", UPLOAD_IMAGE_RESOURCE_MIDDLEWARE.single("resource"), HtmlTypeController.cleanDivision);


export {p1};

