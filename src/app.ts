import * as express from "express";
import * as url from "url";
import * as bodyParser from "body-parser";
import * as sequelize from "sequelize";
import * as timeout from "connect-timeout";
import { v1 } from "./routers/api/v1";
import { p1 } from "./routers/pc/v1";
import { AppLogger } from "./logger/logger";
import { fetchOptions } from "./middleware/fetch-options";
import { requestTimeout } from "./middleware/request-timeout";
import { requestDad } from "./middleware/request-dad";
import { requestLog } from "./middleware/request-log";
import { ApplicationContext } from "./common/applicationcontext";
import { RedisHelper } from "./common/redishelper";
import { AuthErrorResponse } from "./base/base-response";
const config = require("config");
const sequelizeConnction = require("./common/models")();
// 获取node运行环境
let NODE_ENV: string = process.env.NODE_ENV || "default";
// 初始化运行环境和端口
ApplicationContext.setInstance(ApplicationContext.getInstance().init(NODE_ENV));
console.log("ApplicationContext", ApplicationContext.getRedisConfig());

export let app: express.Application = express();
// 设置超时时间
app.use(timeout("600s"));
// 跨域快速返回
app.use(fetchOptions);
// 处理超时
app.use(requestTimeout);
// for parsing application/json
app.use(bodyParser.json({ limit: "20mb" }));
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api/v1/health", async function (req: express.Request, res: express.Response) {
    res.json({
        data: {
            code: 200,
            data: true
        }
    });
    res.end();
});
const healthArr: string[] = [
    "/user/login",
    "/userHealthInformations",
    "/htmlpages",
    "/htmltype/create",
    "/htmltype/update/id",
    "/htmltype/isOpen",
    "/htmltypes",
    "/userHealthInformation/export"
];
// login check
app.use("/pc/v1", async function (req: express.Request, res: express.Response, next: express.NextFunction) {
    let pathname: string = req.path;
    console.log("pathname:" + pathname);
    for (let url of healthArr) {
        if (url === pathname) {
            next();
            return;
        }
    }
    let token = req.headers.token;
    if (!token) {
        res.json(AuthErrorResponse.missingAuthToken());
        return;
    }
    const user: any = await RedisHelper.getInstance().getDataByKey("USER:" + token);
    if (!user) {
        res.json(AuthErrorResponse.missingAuthToken());
        return;
    }
    req.headers.userInfo = JSON.parse(user);
    next();
});

// 业务路由
app.use("/api/v1", v1);
app.use("/pc/v1", p1);
// 所有路由都未匹配（404）
app.use("*", requestDad);
// request log
app.use(requestLog);
//  程序启动
const globalAny: any = global;
globalAny.db = sequelizeConnction;
globalAny.db.query("select name from system where code = 'manage';", { type: sequelize.QueryTypes.SELECT })
    .then((data: any) => {
        console.log("data", data[0]);
        app.listen(3000, () => {
            console.info(`app start ${process.env.HOST_IP}:3000`);
        });
    }, (err: any) => {
        console.error("数据库连接失败：", err);
    });


// uncaughtException 避免程序崩溃
process.on("uncaughtException", function (err) {
    AppLogger.error("未知异常:" + err);
});

process.on("unhandledRejection", function (err, promise) {
    AppLogger.error("未知异常:", err, promise);
});