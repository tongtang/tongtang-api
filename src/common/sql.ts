export class SQL {
    getUser(offset: number, limit: number) {
        let sql: string = `select username, password from
        user where is_deleted = 0`;
        sql += ` limit ${offset} ,${limit}`;
        return sql;
    }

    getUserCount() {
        let sql = `select count(*) as count from  user where is_deleted = 0 `;
        return sql;
    }

    getUserHealthInformation(telephone: string, realName: string, startTime: number, endTime: number, offset: number, limit: number) {
        let sql: string = `select id, real_name as realName, sex, age, telephone, id_card as idCard, health_report_url as healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, origin_id as originId, created_at as createdAt from
        user_health_information where is_deleted = 0`;
        if (telephone && telephone !== "") {
            sql += ` and telephone = $telephone`;
        }
        if (realName && realName !== "") {
            sql += ` and real_name like $realName`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        sql += ` order by created_at  desc  limit ${offset} ,${limit}`;
        return sql;
    }

    getUserHealthInformationCount(telephone: string, realName: string, startTime: number, endTime: number) {
        let sql = `select count(*) as count from  user_health_information where is_deleted = 0 `;
        if (telephone && telephone !== "") {
            sql += ` and telephone = $telephone`;
        }
        if (realName && realName !== "") {
            sql += ` and real_name like $realName`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        return sql;
    }

    getHtmlPage(title: string, type: string, isOpen: number, startTime: number, endTime: number, offset: number, limit: number) {
        let sql: string = `select id, title, comment, type, is_open as isOpen, pv, image_url as imageUrl, author, creation_at as creationAt, html_script_fragment as htmlScriptFragment, page_number as pageNumber, content, weight from
        html_page where is_deleted = 0`;
        if (title && title !== "") {
            sql += ` and title like $title`;
        }
        if (type && type !== "") {
            sql += ` and type = $type`;
        }
        if (isOpen === 0 || isOpen === 1) {
            sql += ` and is_open = $isOpen`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        sql += ` order by weight  desc limit ${offset} ,${limit}`;
        return sql;
    }

    getHtmlPageCount(title: string, type: string, isOpen: number, startTime: number, endTime: number) {
        let sql = `select count(*) as count from html_page where is_deleted = 0`;
        if (title && title !== "") {
            sql += ` and title like $title`;
        }
        if (type && type !== "") {
            sql += ` and type = $type`;
        }
        if (isOpen === 0 || isOpen === 1) {
            sql += ` and is_open = $isOpen`;
        }
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        return sql;
    }

    getPvCount() {
        let sql = `select sum(pv) as count from html_page where is_deleted = 0`;
        return sql;
    }

    getBanner(offset: number, limit: number) {
        let sql: string = `select id, real_name as realName from
        banner  where is_deleted = 0 `;
        sql += ` limit ${offset} ,${limit}`;
        return sql;
    }

    getBannerCount() {
        let sql = `select count(*) as count from  banner where is_deleted = 0`;
        return sql;
    }
}
