const path = require("path");
const fs = require("fs");
const glob = require("glob");
let _ = require("lodash");
import { PasswordUtils } from "./password";
const sequelize = require("sequelize");
const NODE_ENV: string = process.env.NODE_ENV || "default";
const database = JSON.parse(fs.readFileSync(`config/${NODE_ENV}.json`).toString());
const seq = database.sequelize;
const sequelizes = new sequelize(seq.database, seq.username, PasswordUtils.decipherDatabase(seq.password), seq.options);
const models: any = {};
const defines = glob.sync("*.js", {
    root: "",
    cwd: "dist/model/"
});
defines.forEach(function (define: any) {
    const  model = sequelizes.import(path.resolve("dist/model/" + define));
    models[model.name] = model;
});

sequelize.models = models;
module.exports = function () {
    return sequelizes;
};