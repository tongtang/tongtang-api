import * as fs from "fs";
import * as path from "path";
import { RedisConfig } from "../types/config-types";
import { PasswordUtils } from "../common/password";
export class ApplicationContext {
  private static instance: ApplicationContext;

  private static ENV: string;
  private static PORT: number;
  private static GAGOPROXY: string;

  init(env: string): ApplicationContext {
    ApplicationContext.ENV = env;
    return this;
  }

  static getInstance(): ApplicationContext {
    return this.instance || (this.instance = new this());
  }

  static setInstance(app: ApplicationContext): void {
    this.instance = app;
  }

  static getApplicationENV(): string {
    return ApplicationContext.ENV;
  }

  static getApplicationPort(): number {
    return ApplicationContext.PORT;
  }

  static getGagoProxy(): string {
    return ApplicationContext.GAGOPROXY;
  }

  /*
  * 获取微服务中的自定义配置文件
  * */
  static getMicroServiceConfig(): any {
    let env: string = this.getApplicationENV();
    return JSON.parse(fs.readFileSync(`config/${env}.json`).toString());
  }

  /*
* 获取redis配置
* */
  static getRedisConfig(): RedisConfig {
    let microServiceConfig: any = this.getMicroServiceConfig();
    let redisConfig: RedisConfig = microServiceConfig["database"]["redis"];
    // 集群模式配置
    if (redisConfig.isClusterMode) {
      return {
        isClusterMode: redisConfig.isClusterMode,
        clusterNode: redisConfig.clusterNode,
        clusterOptions: {
          redisOptions: {
            password: PasswordUtils.decipherDatabase(redisConfig.password)
          }
        }
      };
    }
    return {
      host: redisConfig.host,
      password: PasswordUtils.decipherDatabase(redisConfig.password),
      port: redisConfig.port
    };
  }

}


