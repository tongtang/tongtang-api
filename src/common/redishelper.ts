import {ApplicationContext} from "../common/applicationcontext";
import { RedisConfig } from "../types/config-types";
import { ClusterNode, Cluster, ClusterOptions, Redis, ScanStreamOption } from "ioredis";
import * as redis from "ioredis";
/*
* redis 辅助类
* */
export class RedisHelper {
  private static instance_: RedisHelper;
  private redisClient_: Redis | Cluster;
  private isClusterMode: boolean;

  constructor() {
    let redisConfig: RedisConfig = ApplicationContext.getRedisConfig();
    this.isClusterMode = redisConfig.isClusterMode;
    if (redisConfig.isClusterMode) {
      this.redisClient_ = new Cluster(redisConfig.clusterNode, redisConfig.clusterOptions);
    } else {
      this.redisClient_ = new redis(redisConfig);
    }
  }

  static getInstance(): RedisHelper {
    if (!RedisHelper.instance_) {
      RedisHelper.instance_ = new RedisHelper();
    }
    return RedisHelper.instance_;
  }

  static setInstance(instance: RedisHelper): void {
    RedisHelper.instance_ = instance;
  }

  getClient(): Redis | Cluster {
    return this.redisClient_;
  }

  /*
  * 根据key读取数据
  * @author gaoqiang@gagogroup.com
  * */
  async getDataByKey(key: string): Promise<string> {
    return this.redisClient_.get(key);
  }

    /*
    * 根据key保存数据
    * @author gaoqiang@gagogroup.com
    * */
  async setDataWithKey(key: string, content: string, expiredMinutes: number = 10, forever: boolean = false): Promise<string> {
    if (!forever) {
      let time: number = RedisHelper.getExpiredTimestamp_(expiredMinutes);
      return this.redisClient_.set(key, content, "EX", time);
    } else {
      return this.redisClient_.set(key, content);
    }
  }

    /*
    * 根据key保存数据 设置有效时间  过后redis删除
    * @author gaoqiang@gagogroup.com
    * */
   async setKeyEffectivityDate(key: string, content: string, expiredMinutes: number = 10): Promise<string> {
      return this.redisClient_.set(key, content, "EX", expiredMinutes * 60);
  }


  /*
  * 根据key读取数据
  * @author gaoqiang@gagogroup.com
  * */
  async lLenListByKey(key: string): Promise<number> {
    return await this.redisClient_.llen(key);
  }

  /*
  * 根据key读取数据
  * @author gaoqiang@gagogroup.com
  * */
 async rPushBufferDataByKey(key: string, content: string): Promise<number> {
  return await this.redisClient_.rpushBuffer(key, new Buffer(content));
 }

    /*
    * 根据key保存数据
    * @author gaoqiang@gagogroup.com
    * */
  async lPopBufferDataWithKey(key: string): Promise<string> {
    let buffer: Buffer = await this.redisClient_.lpopBuffer(key);
    if (buffer) {
      return buffer.toString();
    }
    return null;
  }

  /*
  * 根据key更新数据和过期时间
  * @author gaoqiang@gagogroup.com
  * */
  async updateDataWithKey(key: string, content: string, expiredMinutes: number = 60): Promise<string> {
    let time: number = RedisHelper.getExpiredTimestamp_(expiredMinutes);
    return this.redisClient_.set(key, content, "EX", time);
  }

  /*
  * 根据key清除数据
  * @author gaoqiang@gagogroup.com
  * */
  async clearDataWithKey(key: string): Promise<number> {
    return this.redisClient_.del(key);
  }

  /*
  * 根据正则模糊读取key数据
  * @author gaoqiang@gagogroup.com
  * */
  async getKeysByPattern(pattern: string): Promise<string[]> {
    let keys: string[] = [];
    if (this.isClusterMode) {
      await this.redisClient_.keys("elb@tpi@connect@test");
      // 集群
      // @ts-ignore
      let redisNodes: Redis[] = this.redisClient_.nodes();
      for (let i = 0; i < redisNodes.length; i++) {
        let redisClient: Redis = redisNodes[i];
        let replication: string = await redisClient.info("Replication");
        // 是否为主节点
        let isMaster: boolean = !(replication.indexOf("role:slave") > -1);
        if (isMaster) {
          let ks: string[] = await redisClient.keys(pattern);
          keys.push(...ks);
        }
      }
      return keys;
    } else {
      // 单实例
      keys = await this.redisClient_.keys(pattern);
    }
    return keys;
  }

  /*
  * 过期时间:当前时间+x分
  * @author gaoqiang@gagogroup.com
  * */
  private static getExpiredTimestamp_(minute: number): number {
    let today: Date = new Date();
    today.setMinutes(today.getMinutes() + minute);
    return Math.floor(today.getTime() / 1000);
  }

}
