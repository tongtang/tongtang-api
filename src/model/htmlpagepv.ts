module.exports = function (sequelize: any, dataTypes: any) {
    const model = sequelize.define("htmlpagepv", {
        id: {
            type: dataTypes.STRING,
            primaryKey: true,
            defaultValue: dataTypes.UUIDV4,
            comment: "主键ID",
            field: "id",
        },
        pv: {
            type: dataTypes.INTEGER,
            field: "pv",
            comment: "访问次数"
        },
        date: {
            type: dataTypes.INTEGER,
            field: "date",
            comment: "访问时间"
        },
        htmlPageId: {
            type: dataTypes.INTEGER,
            field: "html_page_id",
            comment: "页面ID"
        },
        createdAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "created_at",
            comment: "创建时间"
        },
        updateAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "updated_at",
            comment: "更新时间时间"
        },
        isDeleted: {
            type: dataTypes.BOOLEAN,
            defaultValue: 0,
            field: "is_deleted",
            comment: "是否删除"
        }
    }, {
        timestamps: false,
        tableName: "html_page_pv"
    });
    return model;
};