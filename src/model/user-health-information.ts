module.exports = function (sequelize: any, dataTypes: any) {
    const model = sequelize.define("userHealthInformation", {
        id: {
            type: dataTypes.STRING,
            primaryKey: true,
            defaultValue: dataTypes.UUIDV4,
            comment: "用户健康信息ID",
            field: "id",
        },
        realName: {
            type: dataTypes.STRING,
            field: "real_name",
            comment: "用户真实名称"
        },
        sex: {
            type: dataTypes.INTEGER,
            field: "sex",
            comment: "性别"
        },
        age: {
            type: dataTypes.INTEGER,
            field: "age",
            comment: "年龄"
        },
        telephone: {
            type: dataTypes.STRING,
            field: "telephone",
            comment: "电话号码"
        },
        idCard: {
            type: dataTypes.STRING,
            field: "id_card",
            comment: "身份证号码"
        },
        healthReportUrl: {
            type: dataTypes.STRING,
            field: "health_report_url",
            comment: "健康信息照片"
        },
        height: {
            type: dataTypes.FLOAT,
            field: "height",
            comment: "身高"
        },
        weight: {
            type: dataTypes.FLOAT,
            field: "weight",
            comment: "体重"
        },
        waistline: {
            type: dataTypes.FLOAT,
            field: "waistline",
            comment: "腰围"
        },
        hipline: {
            type: dataTypes.FLOAT,
            field: "hipline",
            comment: "臀围"
        },
        sbp: {
            type: dataTypes.FLOAT,
            field: "sbp",
            comment: "收缩压"
        },
        dbp: {
            type: dataTypes.FLOAT,
            field: "dbp",
            comment: "舒张压"
        },
        rbg: {
            type: dataTypes.FLOAT,
            field: "rbg",
            comment: "随机血糖"
        },
        fpg: {
            type: dataTypes.FLOAT,
            field: "fpg",
            comment: "空腹血糖"
        },
        hpg: {
            type: dataTypes.FLOAT,
            field: "hpg",
            comment: "餐后2小时血糖"
        },
        ua: {
            type: dataTypes.FLOAT,
            field: "ua",
            comment: "尿酸"
        },
        tc: {
            type: dataTypes.FLOAT,
            field: "tc",
            comment: "总胆固醇"
        },
        tg: {
            type: dataTypes.FLOAT,
            field: "tg",
            comment: "甘油三酯"
        },
        hdlc: {
            type: dataTypes.FLOAT,
            field: "hdlc",
            comment: "高密度脂蛋白胆固醇"
        },
        ldlc: {
            type: dataTypes.FLOAT,
            field: "ldlc",
            comment: "低密度脂蛋白胆固醇"
        },
        origin: {
            type: dataTypes.INTEGER,
            field: "origin",
            comment: "数据来源"
        },
        originId: {
            type: dataTypes.STRING,
            field: "origin_id",
            comment: "数据来源标示ID"
        },
        createdAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "created_at",
            comment: "创建时间"
        },
        updateAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "updated_at",
            comment: "更新时间时间"
        },
        isDeleted: {
            type: dataTypes.BOOLEAN,
            defaultValue: 0,
            field: "is_deleted",
            comment: "是否删除"
        }
    }, {
        timestamps: false,
        tableName: "user_health_information"
    });
    return model;
};