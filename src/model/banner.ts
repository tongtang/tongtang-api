module.exports = function (sequelize: any, dataTypes: any) {
    const model = sequelize.define("banner", {
        id: {
            type: dataTypes.STRING,
            primaryKey: true,
            defaultValue: dataTypes.UUIDV4,
            comment: "bannerID",
            field: "id",
        },
        title: {
            type: dataTypes.STRING,
            field: "title",
            comment: "banner标题"
        },
        type: {
            type: dataTypes.INTEGER,
            field: "type",
            comment: "banner分类"
        },
        author: {
            type: dataTypes.STRING,
            field: "author",
            comment: "作者"
        },
        imageUrl: {
            type: dataTypes.STRING,
            field: "image_url",
            comment: "图片路径"
        },
        skipLink: {
            type: dataTypes.STRING,
            field: "skip_link",
            comment: "banner跳转链接"
        },
        htmlPageNumber: {
            type: dataTypes.STRING,
            field: "html_page_number",
            comment: "用户名称"
        },
        createdAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "created_at",
            comment: "创建时间"
        },
        updateAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "updated_at",
            comment: "更新时间时间"
        },
        isDeleted: {
            type: dataTypes.BOOLEAN,
            defaultValue: 0,
            field: "is_deleted",
            comment: "是否删除"
        }
    }, {
            timestamps: false,
            tableName: "banner"
        });
    return model;
};