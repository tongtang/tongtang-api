module.exports = function (sequelize: any, dataTypes: any) {
    const model = sequelize.define("htmlpage", {
        id: {
            type: dataTypes.STRING,
            primaryKey: true,
            defaultValue: dataTypes.UUIDV4,
            comment: "主键ID",
            field: "id",
        },
        title: {
            type: dataTypes.STRING,
            field: "title",
            comment: "标题"
        },
        comment: {
            type: dataTypes.STRING,
            field: "comment",
            comment: "页标备注"
        },
        type: {
            type: dataTypes.STRING,
            field: "type",
            comment: "页面类型"
        },
        imageUrl: {
            type: dataTypes.STRING,
            field: "image_url",
            comment: "页面照片"
        },
        author: {
            type: dataTypes.STRING,
            field: "author",
            comment: "作者"
        },
        isVideo: {
            type: dataTypes.INTEGER,
            field: "is_video",
            comment: "是否是视频文章"
        },
        content: {
            type: dataTypes.STRING,
            field: "content",
            comment: "页面文本内容"
        },
        weight: {
            type: dataTypes.INTEGER,
            field: "weight",
            comment: "排序权重"
        },
        creationAt: {
            type: dataTypes.INTEGER,
            field: "creation_at",
            defaultValue: new Date().getTime() / 1000,
            comment: "创造时间"
        },
        pv: {
            type: dataTypes.INTEGER,
            field: "pv",
            defaultValue: 0,
            comment: "访问次数"
        },
        htmlScriptFragment: {
            type: dataTypes.STRING,
            field: "html_script_fragment",
            comment: "页面html脚本片段"
        },
        pageNumber: {
            type: dataTypes.STRING,
            field: "page_number",
            comment: "页面编码"
        },
        isOpen: {
            type: dataTypes.BOOLEAN,
            defaultValue: 1,
            field: "is_open",
            comment: "是否删除"
        },
        createdAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "created_at",
            comment: "创建时间"
        },
        updateAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "updated_at",
            comment: "更新时间时间"
        },
        isDeleted: {
            type: dataTypes.BOOLEAN,
            defaultValue: 0,
            field: "is_deleted",
            comment: "是否删除"
        }
    }, {
        timestamps: false,
        tableName: "html_page"
    });
    return model;
};