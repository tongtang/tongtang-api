module.exports = function (sequelize: any, dataTypes: any) {
    const model = sequelize.define("htmltype", {
        id: {
            type: dataTypes.STRING,
            primaryKey: true,
            defaultValue: dataTypes.UUIDV4,
            comment: "主键ID",
            field: "id",
        },
        weight: {
            type: dataTypes.INTEGER,
            field: "weight",
            defaultValue: 0,
            comment: "比重"
        },
        code: {
            type: dataTypes.STRING,
            field: "code",
            comment: "页面类型编码"
        },
        name: {
            type: dataTypes.STRING,
            field: "name",
            comment: "页面类型名称"
        },
        isOpen: {
            type: dataTypes.BOOLEAN,
            defaultValue: 1,
            field: "is_open",
            comment: "是否上架"
        },
        picUrl: {
            type: dataTypes.STRING,
            field: "pic_url",
            comment: "类型图片"
        },
        picUrlNew: {
            type: dataTypes.STRING,
            field: "pic_url_new",
            comment: "图片"
        },
        createdAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "created_at",
            comment: "创建时间"
        },
        updateAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "updated_at",
            comment: "更新时间时间"
        },
        isDeleted: {
            type: dataTypes.BOOLEAN,
            defaultValue: 0,
            field: "is_deleted",
            comment: "是否删除"
        }
    }, {
        timestamps: false,
        tableName: "html_type"
    });
    return model;
};