module.exports = function (sequelize: any, dataTypes: any) {
    const model = sequelize.define("user", {
        id: {
            type: dataTypes.STRING,
            primaryKey: true,
            defaultValue: dataTypes.UUIDV4,
            comment: "用户ID",
            field: "id",
        },
        username: {
            type: dataTypes.STRING,
            field: "username",
            comment: "用户名称"
        },
        password: {
            type: dataTypes.STRING,
            field: "password",
            comment: "密码"
        },
        createdAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "created_at",
            comment: "创建时间"
        },
        updateAt: {
            type: dataTypes.DATE,
            defaultValue: new Date(),
            field: "updated_at",
            comment: "更新时间时间"
        },
        isDeleted: {
            type: dataTypes.BOOLEAN,
            defaultValue: 0,
            field: "is_deleted",
            comment: "是否删除"
        }
    }, {
            timestamps: false,
            tableName: "user"
        });
    return model;
};