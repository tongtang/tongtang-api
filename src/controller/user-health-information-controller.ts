import * as express from "express";
import { UserHealthInformationService } from "../service/user-health-information-service";
import { ResponseServiceError, BadRequestResponse, ResponseSuccess } from "../base/base-response";
const nodeExcel = require("excel-export");
export class UserHealthInformationController {
    public static async findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const userHealthInformationService: any = new UserHealthInformationService();
            const userHealthInformation: any = await userHealthInformationService.findOneById(id);
            res.json(new ResponseSuccess(userHealthInformation, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async findOneByOrignId(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const originId: string = req.query.originId;
        const origin: number = req.query.origin;
        try {
            const userHealthInformationService: any = new UserHealthInformationService();
            const userHealthInformation: any = await userHealthInformationService.findOneByOrignId(originId, origin);
            res.json(new ResponseSuccess(userHealthInformation, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const pageIndex: number = req.query["pageIndex"] || 1;
            const pageSize: number = req.query["pageSize"] || 10;
            const telephone: string = req.query["telephone"];
            const realName: string = req.query["realName"];
            const userHealthInformationService: any = new UserHealthInformationService();
            const districts: any = await userHealthInformationService.findAll(telephone, realName, pageIndex, pageSize);
            res.json(new ResponseSuccess(districts, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async findPCAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const pageIndex: number = req.query["pageIndex"] || 1;
            const pageSize: number = req.query["pageSize"] || 10;
            const telephone: string = req.query["telephone"];
            const realName: string = req.query["realName"];
            const startTime: number = req.query["startTime"];
            const endTime: number = req.query["endTime"];
            const userHealthInformationService: any = new UserHealthInformationService();
            // 获取总访问
            const districts: any = await userHealthInformationService.findPCAll(telephone, realName, startTime, endTime, pageIndex, pageSize);
            res.json(new ResponseSuccess(districts, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async create(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const realName: string = req.body["realName"];
        const sex: number = req.body["sex"]; // 0: 女  1: 男
        const age: number = req.body["age"];
        const telephone: string = req.body["telephone"];
        const idCard: string = req.body["idCard"];
        const healthReportUrl: string = req.body["healthReportUrl"];
        const height: number = req.body["height"];
        const weight: number = req.body["weight"];
        const waistline: number = req.body["waistline"];
        const hipline: number = req.body["hipline"];
        const sbp: number = req.body["sbp"];
        const dbp: number = req.body["dbp"];
        const rbg: number = req.body["rbg"];
        const fpg: number = req.body["fpg"];
        const hpg: number = req.body["hpg"];
        const ua: number = req.body["ua"];
        const tc: number = req.body["tc"];
        const tg: number = req.body["tg"];
        const hdlc: number = req.body["hdlc"];
        const ldlc: number = req.body["ldlc"];
        const origin: number = req.body["origin"]; // 1 : 大地
        const originId: string = req.body["originId"];
        try {
            const userHealthInformationService: any = new UserHealthInformationService();
            const userHealthInformation: any = await userHealthInformationService.create(realName, sex, age, telephone, idCard, healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, originId);
            console.log("userHealthInformation", userHealthInformation);
            res.json(new ResponseSuccess({}, "CREATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.body["id"];
        const realName: string = req.body["realName"];
        const sex: number = req.body["sex"]; // 0: 女  1: 男
        const age: number = req.body["age"];
        const telephone: string = req.body["telephone"];
        const idCard: string = req.body["idCard"];
        const healthReportUrl: string = req.body["healthReportUrl"];
        const height: number = req.body["height"];
        const weight: number = req.body["weight"];
        const waistline: number = req.body["waistline"];
        const hipline: number = req.body["hipline"];
        const sbp: number = req.body["sbp"];
        const dbp: number = req.body["dbp"];
        const rbg: number = req.body["rbg"];
        const fpg: number = req.body["fpg"];
        const hpg: number = req.body["hpg"];
        const ua: number = req.body["ua"];
        const tc: number = req.body["tc"];
        const tg: number = req.body["tg"];
        const hdlc: number = req.body["hdlc"];
        const ldlc: number = req.body["ldlc"];
        const origin: number = req.body["origin"]; // 1 : 大地
        const originId: string = req.body["originId"];
        try {
            const userHealthInformationService: any = new UserHealthInformationService();
            const userHealthInformation: any = await userHealthInformationService.updateById(id, realName, sex, age, telephone, idCard, healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, originId);
            res.json(new ResponseSuccess(userHealthInformation[0], "UPDATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }


    public static async deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const userHealthInformationService: any = new UserHealthInformationService();
            const userHealthInformation: any = await userHealthInformationService.deleteById(id);
            res.json(new ResponseSuccess(userHealthInformation, "DELETE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    // export
    public static async export(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const startTime: number = req.query["startTime"];
        const endTime: number = req.query["endTime"];
        const userHealthInformationService: any = new UserHealthInformationService();
        const resultData: any = await userHealthInformationService.export(startTime, endTime);
        console.log("resultData", resultData);
        const conf: any = {};
        conf.cols = [{
            caption: "用户真实名称",
            type: "string",
            width: 30
        }, {
            caption: "性别",
            type: "string",
            width: 30
        }, {
            caption: "年龄",
            type: "string",
            width: 30
        }, {
            caption: "电话号码",
            type: "string",
            width: 30
        }, {
            caption: "身份证号码",
            type: "string",
            width: 30
        }, {
            caption: "身高",
            type: "string",
            width: 30
        }, {
            caption: "体重",
            type: "string",
            width: 30
        }, {
            caption: "腰围",
            type: "string",
            width: 30
        }, {
            caption: "臀围",
            type: "string",
            width: 30
        }, {
            caption: "收缩压",
            type: "string",
            width: 30
        }, {
            caption: "舒张压",
            type: "string",
            width: 30
        }, {
            caption: "随机血糖",
            type: "string",
            width: 30
        }, {
            caption: "空腹血糖",
            type: "string",
            width: 30
        }, {
            caption: "餐后2小时血糖",
            type: "string",
            width: 30
        }, {
            caption: "尿酸",
            type: "string",
            width: 30
        }, {
            caption: "总胆固醇",
            type: "string",
            width: 30
        }, {
            caption: "甘油三酯",
            type: "string",
            width: 30
        }, {
            caption: "高密度脂蛋白胆固醇",
            type: "string",
            width: 30
        }, {
            caption: "低密度脂蛋白胆固醇",
            type: "string",
            width: 30
        }, {
            caption: "数据来源",
            type: "string",
            width: 30
        }, {
            caption: "填写时间",
            type: "string",
            width: 30
        }, {
            caption: "体检报告链接",
            type: "string",
            width: 100
        }];
        conf.rows = [];
        try {
            for (let i = 0; i < resultData.length; i++) {
                const userHealthInformation = resultData[i];
                const row = [];
                row.push(userHealthInformation.realName ? userHealthInformation.realName + "" : "-");
                row.push(userHealthInformation.sex === 1 ? "男" : "女");
                row.push(userHealthInformation.age ? userHealthInformation.age + "" : "-");
                row.push(userHealthInformation.telephone ? userHealthInformation.telephone + "" : "-");
                row.push(userHealthInformation.idCard ? userHealthInformation.idCard + "" : "-");
                row.push(userHealthInformation.height ? userHealthInformation.height + "" : "-");
                row.push(userHealthInformation.weight ? userHealthInformation.weight + "" : "-");
                row.push(userHealthInformation.waistline ? userHealthInformation.waistline + "" : "-");
                row.push(userHealthInformation.hipline ? userHealthInformation.hipline + "" : "-");
                row.push(userHealthInformation.sbp ? userHealthInformation.sbp + "" : "-");
                row.push(userHealthInformation.dbp ? userHealthInformation.dbp + "" : "-");
                row.push(userHealthInformation.rbg ? userHealthInformation.rbg + "" : "-");
                row.push(userHealthInformation.fpg ? userHealthInformation.fpg + "" : "-");
                row.push(userHealthInformation.hpg ? userHealthInformation.hpg + "" : "-");
                row.push(userHealthInformation.ua ? userHealthInformation.ua + "" : "-");
                row.push(userHealthInformation.tc ? userHealthInformation.tc + "" : "-");
                row.push(userHealthInformation.tg ? userHealthInformation.tg + "" : "-");
                row.push(userHealthInformation.hdlc ? userHealthInformation.hdlc + "" : "-");
                row.push(userHealthInformation.ldlc ? userHealthInformation.ldlc + "" : "-");
                row.push(userHealthInformation.origin === 1 ? "大地APP" : "-");
                row.push(userHealthInformation.createdAt ? userHealthInformation.createdAt + "" : "-");
                row.push(userHealthInformation.healthReportUrl ? userHealthInformation.healthReportUrl + "" : "-");
                conf.rows.push(row);
            }
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
        const result = nodeExcel.execute(conf);
        res.setHeader("Content-Type", "application/vnd.openxmlformats");
        res.setHeader("Content-Disposition", "attachment; filename=" + "data.xlsx");
        res.end(result, "binary");
        return;
    }
}