import * as express from "express";
import { BannerService } from "../service/bannerservice";
import { ResponseServiceError, ResponseParamsError, ResponseSuccess } from "../base/base-response";
import { json } from "sequelize";
export class BannerController {
    public static async findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const bannerService: any = new BannerService();
            const banner: any = await bannerService.findOneById(id);
            res.json(new ResponseSuccess(banner, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const pageIndex: number = req.query["pageIndex"] || 1;
            const pageSize: number = req.query["pageSize"] || 50;
            const bannerService: any = new BannerService();
            const banners: any = await bannerService.findAll(pageIndex, pageSize);
            res.json(new ResponseSuccess(banners));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async create(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const title: string = req.body["title"];
        const type: number = req.body["type"];
        const author: number = req.body["author"];
        const imageUrl: string = req.body["imageUrl"];
        const skipLink: string = req.body["skipLink"];
        const htmlPageNumber: string = req.body["htmlPageNumber"];
        try {
            const bannerService: any = new BannerService();
            const banner: any = await bannerService.create(title, type, author, imageUrl, skipLink, htmlPageNumber);
            res.json(new ResponseSuccess({}, "CREATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.body["realName"];
        const title: string = req.body["title"];
        const type: number = req.body["type"];
        const author: number = req.body["author"];
        const imageUrl: string = req.body["imageUrl"];
        const skipLink: string = req.body["skipLink"];
        const htmlPageNumber: string = req.body["htmlPageNumber"];
        try {
            const bannerService: any = new BannerService();
            const banner: any = await bannerService.updateById(id, title, type, author, imageUrl, skipLink, htmlPageNumber);
            res.json(new ResponseSuccess({}, "UPDATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }


    public static async deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const bannerService: any = new BannerService();
            const banner: json = await bannerService.deleteById(id);
            res.json(new ResponseSuccess(banner, "DELETE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }
}