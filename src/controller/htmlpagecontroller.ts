import * as express from "express";
import { Validator } from "validator-ts";
import { RequestParams } from "../types/interfaces";
import { RequestUtils } from "../util/request-utils";
import { HtmlPageService } from "../service/htmlpageservice";
import { HtmlPagePvService } from "../service/htmlpagepvservice";
import { ResponseServiceError, ResponseParamsError, ResponseSuccess } from "../base/base-response";
const nodeExcel = require("excel-export");
export class HtmlPageController {
    public static async findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const type: string = req.query["type"];
            const htmlPageService: any = new HtmlPageService();
            const htmlpages: any = await htmlPageService.findAll(type);
            res.json(new ResponseSuccess(htmlpages, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async findPCAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const title: string = req.query["title"];
            const type: string = req.query["type"];
            const isOpen: number = Number(req.query["isOpen"]);
            const startTime: number = req.query["startTime"];
            const endTime: number = req.query["endTime"];
            const pageIndex: number = req.query["pageIndex"] || 1;
            const pageSize: number = req.query["pageSize"] || 10;
            const htmlPageService: any = new HtmlPageService();
            const htmlpages: any = await htmlPageService.findPCAll(title, type, isOpen, startTime, endTime, pageIndex, pageSize);
            res.json(new ResponseSuccess(htmlpages, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.findOneById(id);
            htmlPageService.addPV(id);
            // APP端每次请求给这个页面加一次访问量
            const htmlPagePvService: any = new HtmlPagePvService();
            htmlPagePvService.addPV(id);
            res.json(new ResponseSuccess(htmlpage, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async findPCOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.findOneById(id);
            res.json(new ResponseSuccess(htmlpage, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async getHtmlPageCodeList(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpageCodeList: any = await htmlPageService.getHtmlPageCodeList();
            res.json(new ResponseSuccess(htmlpageCodeList, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }



    public static async findOneByPageNumber(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const pageNumber: string = req.query.pageNumber;
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.findOneByPageNumber(pageNumber);
            res.json(new ResponseSuccess(htmlpage, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async create(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const title: string = req.body["title"];
        const comment: string = req.body["comment"];
        const type: string = req.body["type"];
        const imageUrl: string = req.body["imageUrl"];
        const author: string = req.body["author"];
        const content: string = req.body["content"];
        const creationAt: string = req.body["creationAt"];
        const htmlScriptFragment: string = req.body["htmlScriptFragment"];
        const pageNumber: string = req.body["pageNumber"];
        const weight: number = req.body["weight"];
        const isVideo: number = req.body["isVideo"];
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.create(imageUrl, title, comment, type, author, content, creationAt, htmlScriptFragment, pageNumber, weight, isVideo);
            res.json(new ResponseSuccess({}, "CREATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.body["id"];
        const title: string = req.body["title"];
        const comment: string = req.body["comment"];
        const type: string = req.body["type"];
        const imageUrl: string = req.body["imageUrl"];
        const author: string = req.body["author"];
        const content: string = req.body["content"];
        const creationAt: string = req.body["creationAt"];
        const htmlScriptFragment: string = req.body["htmlScriptFragment"];
        const pageNumber: string = req.body["pageNumber"];
        const weight: number = req.body["weight"];
        const isVideo: number = req.body["isVideo"];
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.updateById(id, title, comment, type, imageUrl, author, content, creationAt, htmlScriptFragment, pageNumber, weight, isVideo);
            res.json(new ResponseSuccess(htmlpage[0], "UPDATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async isOpen(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.body.id;
        const isOpen: number = req.body.isOpen;
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.isOpen(id, isOpen);
            res.json(new ResponseSuccess(htmlpage[0], "ISOPEN_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }


    public static async deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.deleteById(id);
            res.json(new ResponseSuccess(htmlpage, "DELETE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async export(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const startTime: number = req.query.startTime;
        const endTime: number = req.query.endTime;
        const htmlPageService: any = new HtmlPageService();
        // 获取所有的页面
        const resultPage: any = await htmlPageService.findAllPagePv("");
        const htmlpageIds: Array<string> = [];
        for (let i = 0; i < resultPage.length; i++) {
            htmlpageIds.push(resultPage[i]["id"]);
        }
        if (htmlpageIds.length > 0) {

        }
        console.log("resultPage", resultPage);
        // const resultData: any = await htmlPageService.exportHtmlPagePV(startTime, endTime);
        // const conf: any = {};
        // conf.cols = [{
        //     caption: "页面名称",
        //     type: "string",
        //     width: 30
        // }, {
        //     caption: "访问总PV",
        //     type: "string",
        //     width: 30
        // }];
        // conf.rows = [];
        // try {
        //     for (let i = 0; i < resultData.length; i++) {
        //         const userHealthInformation = resultData[i];
        //         const row = [];
        //         row.push(userHealthInformation.realName ? userHealthInformation.realName + "" : "-");
        //         row.push(userHealthInformation.sex === 1 ? "男" : "女");
        //         conf.rows.push(row);
        //     }
        // } catch (err) {
        //     res.json(new ResponseServiceError(err));
        //     return;
        // }
        // const result = nodeExcel.execute(conf);
        // res.setHeader("Content-Type", "application/vnd.openxmlformats");
        // res.setHeader("Content-Disposition", "attachment; filename=" + "data.xlsx");
        // res.end(result, "binary");
        return;
    }
}