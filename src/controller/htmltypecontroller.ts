import * as express from "express";
import { Validator } from "validator-ts";
import { RequestParams } from "../types/interfaces";
import { RequestUtils } from "../util/request-utils";
import { HtmlTypeService } from "../service/htmltypeservice";
const xlsx = require("node-xlsx");
import { ResponseServiceError, ResponseParamsError, ResponseSuccess } from "../base/base-response";
export class HtmlTypeController {
    public static async findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const htmlTypeService: any = new HtmlTypeService();
            const htmltypes: any = await htmlTypeService.findAll();
            res.json(new ResponseSuccess(htmltypes, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async findPCAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const isOpen: number = Number(req.query["isOpen"]);
            const pageIndex: number = req.query["pageIndex"] || 1;
            const pageSize: number = req.query["pageSize"] || 10;
            const htmlTypeService: any = new HtmlTypeService();
            const htmltypes: any = await htmlTypeService.findPCAll(isOpen, pageIndex, pageSize);
            res.json(new ResponseSuccess(htmltypes, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async findOneById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const htmlTypeService: any = new HtmlTypeService();
            const htmltype: any = await htmlTypeService.findOneById(id);
            res.json(new ResponseSuccess(htmltype, "FIND_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async create(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const code: string = req.body["code"];
        const name: string = req.body["name"];
        const weight: number = req.body["weight"];
        const picUrl: string = req.body["picUrl"];
        const picUrlNew: string = req.body["picUrlNew"];
        try {
            const htmlTypeService: any = new HtmlTypeService();
            const htmltype: any = await htmlTypeService.create(code, name, weight, picUrl, picUrlNew);
            res.json(new ResponseSuccess({}, "CREATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async updateById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.body["id"];
        const code: string = req.body["code"];
        const name: string = req.body["name"];
        const weight: number = req.body["weight"];
        const picUrl: string = req.body["picUrl"];
        const picUrlNew: string = req.body["picUrlNew"];
        try {
            const htmlTypeService: any = new HtmlTypeService();
            const htmltype: any = await htmlTypeService.updateById(id, code, name, weight, picUrl, picUrlNew);
            res.json(new ResponseSuccess(htmltype[0], "UPDATE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async isOpen(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.body.id;
        const isOpen: number = req.body.isOpen;
        try {
            const htmlTypeService: any = new HtmlTypeService();
            const htmltype: any = await htmlTypeService.isOpen(id, isOpen);
            res.json(new ResponseSuccess(htmltype[0], "ISOPEN_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }


    public static async deleteById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const id: string = req.query.id;
        try {
            const htmlTypeService: any = new HtmlTypeService();
            const htmltype: any = await htmlTypeService.deleteById(id);
            res.json(new ResponseSuccess(htmltype, "DELETE_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    static async cleanDivision(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
          const path = req.file.path;
          const filename = req.file.originalname;
          if (filename.substring(filename.length - 5, filename.length) !== ".xlsx") {
          }
          const list = xlsx.parse(path);
          const districtsData = list[1].data;
          const htmlTypeService: any = new HtmlTypeService();
          const htmltype: any = await htmlTypeService.cleanDivision(districtsData);
          res.json(new ResponseSuccess({}, "CLEAN_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
      }
}