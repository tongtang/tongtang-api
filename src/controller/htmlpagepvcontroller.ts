import * as express from "express";
import { Validator } from "validator-ts";
import { RequestParams } from "../types/interfaces";
import { RequestUtils } from "../util/request-utils";
import { HtmlPageService } from "../service/htmlpageservice";
import { HtmlPagePvService } from "../service/htmlpagepvservice";
import { ResponseServiceError, ResponseParamsError, ResponseSuccess } from "../base/base-response";
export class HtmlPagePvController {
    public static async getHtmlPagePvById(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const id: string = req.query["id"];
            const startTime: string = req.query["startTime"];
            const endTime: string = req.query["endTime"];
            const htmlPageService: any = new HtmlPageService();
            const htmlpage: any = await htmlPageService.findOneById(id);
            const htmlPagePvService: any = new HtmlPagePvService();
            const result: any = await htmlPagePvService.getHtmlPagePvById(id, startTime, endTime);
            res.json(new ResponseSuccess({ "id" : id, "pv": htmlpage.pv, "list": result}, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }
}