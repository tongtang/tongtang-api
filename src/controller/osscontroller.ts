import * as express from "express";
const OSS = require("ali-oss");
const STS = OSS.STS;
const co = require("co");
const sts = new STS({
    accessKeyId: "LTAI4GBRs8RDimfuGKJrWtda",
    accessKeySecret: "mqvv4fDmRMK8r2qwADYUjA085DPWvW"
});
const rolearn = "acs:ram::1594210554081543:role/tongtangram";
const policy = {
    "Version": "1",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "oss:ListObjects",
                "oss:GetObject"
            ],
            "Resource": [
                "acs:oss:*:*:ram-test",
                "acs:oss:*:*:ram-test/*"
            ]
        }
    ]
};
import { ResponseServiceError, BadRequestResponse, ResponseSuccess } from "../base/base-response";
export class OssController {
    public static async getOssToken(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const token = await sts.assumeRole(rolearn, policy, 15 * 60, "tongtangRAM");
            res.json(new ResponseSuccess({
                token: token.credentials
            }, "GET_OSS_TOKEN_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }
}