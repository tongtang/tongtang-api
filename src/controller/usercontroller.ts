import * as express from "express";
import { ResponseServiceError, BadRequestResponse, ResponseSuccess } from "../base/base-response";
import { UserService } from "../service/userservice";
import { BaseController } from "../base/base-controller";
export class UserController extends BaseController {
    public static async findAll(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        try {
            const pageIndex: number = req.query["pageIndex"] || 1;
            const pageSize: number = req.query["pageSize"] || 10;
            const userService: any = new UserService();
            const users: any = await userService.findAll(pageIndex, pageSize);
            res.json(new ResponseSuccess(users, "FIND_SUCCESS"));
        } catch (err) {
            res.json(new ResponseServiceError(err.message));
            return;
        }
    }

    public static async login(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const username: string = req.body["username"];
        const password: string = req.body["password"];
        try {
            const userService: any = new UserService();
            const user: any = await userService.findOneByUserName(username);
            if (!user) {
                res.json(new BadRequestResponse("USER_NOT_FIND"));
                return;
            }
            if (user.password !== password) {
                res.json(new BadRequestResponse("USERNAME_OR_PASSWORD_IS_ERR"));
                return;
            }
            const loginData: any = await userService.setTokenInRedis({ id: user.id, username: username });
            res.json(new ResponseSuccess(loginData, "LOGIN_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }

    public static async logout(req: any, res: express.Response, next: express.NextFunction): Promise<void> {
        const token = req.headers.token;
        // 清除登陆记录
        try {
            if (token) {
                const userService: any = new UserService();
                await userService.logout(token);
            }
            res.json(new ResponseSuccess(true, "LOGOUT_SUCCESS"));
            return;
        } catch (err) {
            res.json(new ResponseServiceError(err));
            return;
        }
    }
}