import * as crypto from "crypto";
import * as uuid from "uuid";
import * as shortId from "shortid";

const genId = require("gen-id")("XX");

export class RandomUtils {

  /*
  * 生成随机值 token
  * */
  static generateToken() {
    return crypto.randomBytes(36).toString("hex");
  }

  /*
  * 生成随机数,只有数字
  * */
  static randomNum(size: number): string {
    let num: string = "0123456789";
    size = size || 6;
    let no_: string = "";
    for (let i: number = 0; i < size; ++i) {
      no_ += num.charAt(Math.random() * num.length);
    }
    return no_;
  }

  /*
  * 生成字符串,包含数字字母
  * */
  static randomStr(size: number): string {
    let str: string = "234579WERUPASFGHKZXCNM";   // 22个
    size = size || 4;
    let s_: string = "";
    for (let i: number = 0; i < size; ++i) {
      s_ += str.charAt(Math.random() * str.length);
    }
    return s_;
  }

  /*
   * 生成uuid随机码
   * @returns {string}
   */
  static getUuid(): string {
    return uuid.v1().split("-").join("");
  }

  /*
  * 生成短id
  * */
  static getShortId() {
    let seed: any = genId.generate();
    return shortId.generate() + (seed ? seed : "");
  }

  /*
  ** randomWord 产生任意长度随机字母数字组合
  ** randomFlag-是否任意长度 min-任意长度最小位[固定位数] max-任意长度最大位
  * */
  static randomWord(randomFlag: boolean, min: number, max: number = 0): string {
    let str = "";
    let range = min;
    let arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

    // 随机产生
    if (randomFlag) {
      range = Math.round(Math.random() * (max - min)) + min;
    }
    for (let i = 0; i < range; i ++) {
      let pos: number = Math.round(Math.random() * (arr.length - 1));
      str += arr[pos];
    }
    return str;
  }


}
