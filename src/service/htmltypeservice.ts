import { SQL } from "../common/sql";
import * as fs from "fs";
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny: any = global;

export class HtmlTypeService {
    private data: any;
    private model: any;

    constructor(obj: any = null) {
        if (obj) this.data = _.clone(obj);
        this.model = sequelize.models.htmltype;
    }

    findOneById(id: string) {
        return this.model.findOne({
            attributes: ["id", "code", "name", ["is_open", "isOpen"], "weight", ["pic_url", "picUrl"], ["pic_url_new", "picUrlNew"]],
            where: { id: id }
        });
    }

    async findAll() {
        let sql: string = `select id as code, name, weight, pic_url as picUrl, pic_url_new as picUrlNew from html_type where is_deleted = 0 and is_open = 1 order by weight desc`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: {} });
    }

    create(code: string, name: string, weight: number, picUrl: string, picUrlNew: string) {
        return this.model.create({
            code: code,
            name: name,
            weight: weight ? weight : 0,
            picUrl: picUrl,
            picUrlNew: picUrlNew
        });
    }

    updateById(id: string, code: string, name: string, weight: number, picUrl: string, picUrlNew: string) {
        return this.model.update({
            code: code,
            name: name,
            weight: weight,
            picUrl: picUrl,
            picUrlNew: picUrlNew
        }, { where: { id: id } });
    }

    async isOpen(id: string, isOpen: number) {
        return this.model.update({
            isOpen: isOpen
        }, { where: { id: id } });
    }

    async deleteById(id: string) {
        let sql: string = `update html_type set is_deleted = 1 where id = $id`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
    }

    async getHtmlPageCodeList() {
        return [
            { code: "HTMLTYPE001", name: "母婴健康" },
            { code: "HTMLTYPE002", name: "就医问药" },
            { code: "HTMLTYPE003", name: "饮食营养" },
            { code: "HTMLTYPE004", name: "睡眠管理" },
            { code: "HTMLTYPE005", name: "减重健身" },
            { code: "HTMLTYPE006", name: "科技前沿" },
            { code: "HTMLTYPE007", name: "视频专区" }
        ];
    }

    async cleanDivision(divisionData: any) {
        let sqlArray: Array<string> = [];
        for (let i = 1; i < divisionData.length; i++) {
            let insertSql: string = "";
            let division: any = divisionData[i];
            let provincecode: string = division[1] !== "" ? division[1].substring(0, 2) : null;
            let provincename: string = division[2] !== "" ? division[2] : null;
            let citycode: string = division[3] !== "" ? division[3].substring(0, 4) : null;
            let cityname: string = division[4] !== "" ? division[4] : null;
            let countycode: string = division[5] !== "" ? division[5].substring(0, 6) : null;
            let countyname: string = division[6] !== "" ? division[6] : null;
            let towncode: string = division[7] !== "" ? division[7].substring(0, 9) : null;
            let townname: string = division[8] !== "" ? division[8] : null;
            let villagecode: string = division[9] !== "" ? division[9].substring(0, 12) : null;
            let villagename: string = division[10] !== "" ? division[10] : null;
            let code = "";
            let name = "";
            if (villagecode) {
                code = villagecode;
                name = villagename;
            } else if (towncode) {
                code = towncode;
                name = townname;
            } else if (countycode) {
                code = countycode;
                name = countyname;
            } else if (citycode) {
                code = citycode;
                name = cityname;
            } else if (provincecode) {
                code = provincecode;
                name = provincename;
            }
            let sql: string = `insert into division_core(code , name , province_code, province_name, city_code, city_name, county_code, county_name, town_code, town_name, village_code, village_name) value (${code}, '${name}', ${provincecode}, '${provincename}', ${citycode}, '${cityname}', ${countycode}, '${countyname}', ${towncode}, '${townname}', ${villagecode}, '${villagename}');`;
            sqlArray.push(sql);
        }
        console.log("sqlArray.length: " + sqlArray.length);
        let sqlstring = "";
        for (let i = 0; i < sqlArray.length; i++) {
            sqlstring = sqlstring + sqlArray[i];
        }
        fs.createWriteStream("./upload/division001.sql").write(sqlstring);
    }

    // export
}