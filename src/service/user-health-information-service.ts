import { SQL } from "../common/sql";
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny: any = global;

export class UserHealthInformationService {
    private data: any;
    private model: any;

    constructor(obj: any = null) {
        if (obj) this.data = _.clone(obj);
        this.model = sequelize.models.userHealthInformation;
    }

    findOneById(id: string) {
        return this.model.findOne({
            attributes: ["id", ["real_name", "realName"], "sex", "age", "telephone", ["id_card", "idCard"], ["health_report_url", "healthReportUrl"], "height", "weight", "waistline", "hipline", "sbp", "dbp", "rbg", "fpg", "hpg", "ua", "tc", "tg", "hdlc", "ldlc", "origin", ["origin_id", "originId"]],
            where: { id: id }
        });
    }

    findOneByOrignId(originId: string, origin: number) {
        console.log("orignId:" + originId);
        return this.model.findOne({
            attributes: ["id", ["real_name", "realName"], "sex", "age", "telephone", ["id_card", "idCard"], ["health_report_url", "healthReportUrl"], "height", "weight", "waistline", "hipline", "sbp", "dbp", "rbg", "fpg", "hpg", "ua", "tc", "tg", "hdlc", "ldlc", "origin", ["origin_id", "originId"]],
            where: { origin_id: originId, origin: origin }
        });
    }

    async findPCAll(telephone: string, realName: string, startTime: number, endTime: number, pageIndex: number, pageSize: number) {
        const sql = new SQL();
        let userHealthInformations = [];
        let count: number = 0;
        let limit: number = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
        let offset: number = limit * (Number(pageIndex) - 1);
        count = (await globalAny.db.query(sql.getUserHealthInformationCount(telephone, realName, startTime, endTime), {
            type: sequelize.QueryTypes.SELECT, bind: {
                realName: `%${realName}%`,
                telephone: telephone
            }
        }))[0].count;
        userHealthInformations = await globalAny.db.query(sql.getUserHealthInformation(telephone, realName, startTime, endTime, offset, limit), {
            type: sequelize.QueryTypes.SELECT, bind: {
                realName: `%${realName}%`,
                telephone: telephone
            }
        });
        console.log("userHealthInformations", userHealthInformations);
        return { count: count, rows: userHealthInformations };
    }

    create(realName: string, sex: number, age: number, telephone: string, idCard: string, healthReportUrl: string, height: number, weight: number, waistline: number, hipline: number, sbp: number, dbp: number, rbg: number, fpg: number, hpg: number, ua: number, tc: number, tg: number, hdlc: number, ldlc: number, origin: number, originId: string) {
        return this.model.create({
            realName: realName,
            sex: sex,
            age: age,
            telephone: telephone,
            idCard: idCard,
            healthReportUrl: healthReportUrl,
            height: height,
            weight: weight,
            waistline: waistline,
            hipline: hipline,
            sbp: sbp,
            dbp: dbp,
            rbg: rbg,
            fpg: fpg,
            hpg: hpg,
            ua: ua,
            tc: tc,
            tg: tg,
            hdlc: hdlc,
            ldlc: ldlc,
            origin: origin,
            originId: originId,

        });
    }



    updateById(id: string, realName: string, sex: number, age: number, telephone: string, idCard: string, healthReportUrl: string, height: number, weight: number, waistline: number, hipline: number, sbp: number, dbp: number, rbg: number, fpg: number, hpg: number, ua: number, tc: number, tg: number, hdlc: number, ldlc: number, origin: number, originId: string) {
        return this.model.update({
            realName: realName,
            sex: sex,
            age: age,
            telephone: telephone,
            idCard: idCard,
            healthReportUrl: healthReportUrl,
            height: height,
            weight: weight,
            waistline: waistline,
            hipline: hipline,
            sbp: sbp,
            dbp: dbp,
            rbg: rbg,
            fpg: fpg,
            hpg: hpg,
            ua: ua,
            tc: tc,
            tg: tg,
            hdlc: hdlc,
            ldlc: ldlc,
            origin: origin,
            originId: originId
        }, { where: { id: id } });
    }

    async deleteById(id: string) {
        let sql: string = `update user_health_information set is_deleted = 1 where id = $id`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
    }

    async export(startTime: number, endTime: number) {
        let sql: string = `select real_name as realName, sex, age, telephone, id_card as  idCard, health_report_url as healthReportUrl, height, weight, waistline, hipline, sbp, dbp, rbg, fpg, hpg, ua, tc, tg, hdlc, ldlc, origin, created_at as createdAt from user_health_information where is_deleted = 0`;
        if (startTime > 1) {
            sql += ` and   created_at >= $startTime`;
        }
        if (endTime > 1) {
            sql += ` and created_at <= $endTime`;
        }
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { startTime: startTime, endTime: endTime } });
    }
}