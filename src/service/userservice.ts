import { SQL } from "../common/sql";
import { RandomUtils } from  "../util/random-utils";
import { RedisHelper } from "../common/redishelper";
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny: any = global;

export class UserService {
  private data: any;
  private model: any;

  constructor(obj: any = null) {
    if (obj) this.data = _.clone(obj);
    this.model = sequelize.models.user;
  }

  findOne(id: string) {
    return this.model.findOne({
      attributes: ["username", "password"],
      where: { id: id }
    });
  }

  findOneByUserName(username: string) {
    return  this.model.findOne({
      attributes: ["id", "username", "password"],
      where: { username: username }
    });
  }

  async findAll(pageIndex: number, pageSize: number) {
    const sql = new SQL();
    let userHealthInformations = [];
    let count: number = 0;
    let limit: number = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
    let offset: number = limit * (Number(pageIndex) - 1);
    count = (await globalAny.db.query(sql.getUserCount(), { type: sequelize.QueryTypes.SELECT, bind: {
    }}))[0].count;
    userHealthInformations = await globalAny.db.query(sql.getUser(offset, limit), { type: sequelize.QueryTypes.SELECT, bind: {
    }});
    return { count: count, rows: userHealthInformations };
  }

  async setTokenInRedis(userInfo: any) {
    let tokenTime: number = 600;
    // 生成随机token
    let token = RandomUtils.generateToken();
    let redisHelper: RedisHelper = RedisHelper.getInstance();
    await redisHelper.setDataWithKey("USER:" + token, JSON.stringify(userInfo), tokenTime / 60);
    return {
      id: userInfo.id,
      username: userInfo.username,
      token: token
    };
  }

  async logout(token: string) {
    let redisHelper: RedisHelper = RedisHelper.getInstance();
    let redisContent: string = await redisHelper.getDataByKey("USER:" + token);
    if (redisContent) {
      await redisHelper.clearDataWithKey("USER:" + token);
    }
  }
}