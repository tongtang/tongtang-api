import { NOW } from "sequelize";
import { SQL } from "../common/sql";
const sequelize = require("sequelize");
let _ = require("lodash");
import moment = require("moment");
const globalAny: any = global;

export class HtmlPageService {
    private data: any;
    private model: any;

    constructor(obj: any = null) {
        if (obj) this.data = _.clone(obj);
        this.model = sequelize.models.htmlpage;
    }

    findOneById(id: string) {
        return this.model.findOne({
            attributes: ["id", "title", "type", "comment", "pv", ["image_url", "imageUrl"], "author", "content", ["creation_at", "creationAt"], ["html_script_fragment", "htmlScriptFragment"], ["page_number", "pageNumber"], ["is_open", "isOpen"], "weight", ["is_video", "isVideo"]],
            where: { id: id }
        });
    }

    findPCOneById(id: string) {
        return this.model.findOne({
            attributes: ["id", "title", "type", "comment", "pv", ["image_url", "imageUrl"], "author", "content", ["creation_at", "creationAt"], ["html_script_fragment", "htmlScriptFragment"], ["page_number", "pageNumber"], ["is_open", "isOpen"], "weight", ["is_video", "isVideo"]],
            where: { id: id }
        });
    }

    findOneByPageNumber(pageNumer: string) {
        return this.model.findOne({
            attributes: ["id", "title", "type", "comment", "pv", ["image_url", "imageUrl"], "author", "content", ["creation_at", "creationAt"], ["html_script_fragment", "htmlScriptFragment"], ["page_number", "pageNumber"], ["is_open", "isOpen"], "weight", ["is_video", "isVideo"]],
            where: { pageNumer: pageNumer }
        });
    }

    async findAll(type: string) {
        let sql: string = `select id, title, type, comment, pv, image_url as imageUrl, author, content, creation_at as creationAt, html_script_fragment as htmlScriptFragment, page_number as pageNumber, is_open as isOpen, weight, is_video as isVideo from html_page where  is_deleted = 0 and is_open = 1 and  type = '${type}' order by weight desc ;`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: {} });
    }

    async findAllPagePv() {
        let sql: string = `select id, title, pv from html_page where  is_deleted = 0 and is_open = 1;`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: {} });
    }

    async findPCAll(title: string, type: string, isOpen: number, startTime: number, endTime: number, pageIndex: number, pageSize: number) {
        const sql = new SQL();
        let htmlpages = [];
        let count: number = 0;
        let pvCount: number = 0;
        let limit: number = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
        let offset: number = limit * (Number(pageIndex) - 1);
        count = (await globalAny.db.query(sql.getHtmlPageCount(title, type, isOpen, startTime, endTime), {
            type: sequelize.QueryTypes.SELECT, bind: {
                title: `%${title}%`,
                type: type,
                isOpen: isOpen
            }
        }))[0].count;
        htmlpages = await globalAny.db.query(sql.getHtmlPage(title, type, isOpen, startTime, endTime, offset, limit), {
            type: sequelize.QueryTypes.SELECT, bind: {
                title: `%${title}%`,
                type: type,
                isOpen: isOpen
            }
        });
        pvCount = (await globalAny.db.query(sql.getPvCount(), {
            type: sequelize.QueryTypes.SELECT, bind: {
            }
        }))[0].count;
        return { count: count, pvCount: pvCount, rows: htmlpages };
    }

    create(imageUrl: string, title: string, comment: string, type: string, author: string, content: string, creationAt: string, htmlScriptFragment: string, pageNumber: string, weight: number, isVideo: number) {
        return this.model.create({
            title: title,
            comment: comment,
            type: type,
            imageUrl: imageUrl,
            author: author,
            content: content,
            creationAt: creationAt,
            weight: weight ? weight : 0,
            isVideo: isVideo ? isVideo : 0,
            htmlScriptFragment: htmlScriptFragment,
            pageNumber: pageNumber
        });
    }

    updateById(id: string, title: string, comment: string, type: string, imageUrl: string, author: string, content: string, creationAt: number, htmlScriptFragment: string, pageNumber: string, weight: number, isVideo: number) {
        console.log("updateAt", new Date().toLocaleString());
        return this.model.update({
            title: title,
            comment: comment,
            type: type,
            imageUrl: imageUrl,
            author: author,
            content: content,
            creationAt: creationAt,
            updateAt: new Date(),
            weight: weight ? weight : 0,
            isVideo: isVideo ? isVideo : 0,
            htmlScriptFragment: htmlScriptFragment,
            pageNumber: pageNumber
        }, { where: { id: id } });
    }

    async addPV(id: string) {
        let sql: string = `update html_page set pv = pv + 1 where id = '${id}'`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.UPDATE, bind: {} });
    }

    async isOpen(id: string, isOpen: number) {
        return this.model.update({
            isOpen: isOpen
        }, { where: { id: id } });
    }

    async deleteById(id: string) {
        let sql: string = `update html_page set is_deleted = 1 where id = $id`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
    }

    async getHtmlPageCodeList() {
        return [
            { code: "HTMLTYPE001", name: "母婴健康" },
            { code: "HTMLTYPE002", name: "就医问药" },
            { code: "HTMLTYPE003", name: "饮食营养" },
            { code: "HTMLTYPE004", name: "睡眠管理" },
            { code: "HTMLTYPE005", name: "减重健身" },
            { code: "HTMLTYPE006", name: "科技前沿" },
            { code: "HTMLTYPE007", name: "视频专区" }
        ];
    }

}