import { SQL } from "../common/sql";
const sequelize = require("sequelize");
let _ = require("lodash");
const globalAny: any = global;

export class BannerService {
    private data: any;
    private model: any;

    constructor(obj: any = null) {
        if (obj) this.data = _.clone(obj);
        this.model = sequelize.models.banner;
    }

    async findAll(pageIndex: number, pageSize: number) {
        const sql = new SQL();
        let banners = [];
        let count: number = 0;
        let limit: number = (Number(pageSize) === 0 || Number(pageSize) > 50) ? 50 : Number(pageSize);
        let offset: number = limit * (Number(pageIndex) - 1);
        count = (await globalAny.db.query(sql.getBannerCount(), { type: sequelize.QueryTypes.SELECT }))[0].count;
        banners = await globalAny.db.query(sql.getBanner(offset, limit), { type: sequelize.QueryTypes.SELECT });
        return { count: count, rows: banners };
    }

    create(title: string, type: number, author: string, imageUrl: string, skipLink: string, htmlPageNumber: string) {
        return this.model.create({
            title: title,
            type: type,
            author: author,
            imageUrl: imageUrl,
            skipLink: skipLink,
            htmlPageNumber: htmlPageNumber
        });
    }

    updateById(id: string, title: string, type: number, author: string, imageUrl: string, skipLink: string, htmlPageNumber: string) {
        return this.model.update({
            title: title,
            type: type,
            author: author,
            imageUrl: imageUrl,
            skipLink: skipLink,
            htmlPageNumber: htmlPageNumber
        }, { where: { id: id } });
    }

    async deleteById(id: string) {
        let sql: string = `update banner set is_deleted = 1 where id = $id`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: { id: id } });
    }

}