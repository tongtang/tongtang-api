import { NOW } from "sequelize";
import { SQL } from "../common/sql";
const sequelize = require("sequelize");
let _ = require("lodash");
import moment = require("moment");
const globalAny: any = global;

export class HtmlPagePvService {
    private data: any;
    private model: any;

    constructor(obj: any = null) {
        if (obj) this.data = _.clone(obj);
        this.model = sequelize.models.htmlpagepv;
    }

    create(date: number, pv: number, htmlPageId: string) {
        return this.model.create({
            pv: pv,
            date: date,
            htmlPageId: htmlPageId
        });
    }

    async addPV(id: string) {
        const date: number = Number(moment(new Date()).format("YYYYMMDD"));
        let queryHmlPagePv: string = `select html_page_id as htmlPageId from html_page_pv where date = ${date} and html_page_id = '${id}' and is_deleted =0`;
        let result: any = await globalAny.db.query(queryHmlPagePv, { type: sequelize.QueryTypes.SELECT, bind: {} });
        if (result.length > 0) {
            let htmlPageId: number = result[0].htmlPageId;
            let updateSql: string = `update html_page_pv set pv = pv + 1 where html_page_id = $htmlPageId and date = ${date}; `;
            return await globalAny.db.query(updateSql, { type: sequelize.QueryTypes.UPDATE, bind: { htmlPageId: htmlPageId } });
        } else {
            return await this.create(date, 1, id);
        }

    }

    async getHtmlPagePvById(id: string, startTime: number, endTime: number) {
        let sql: string = `select html_page_id as htmlPageId, date, pv from html_page_pv where html_page_id = '${id}' and is_deleted = 0`;
        if (startTime) {
            sql += ` and date >= ${startTime}`;
        }
        if (endTime) {
            sql += ` and date <= ${endTime}`;
        }
        sql += ` order by date;`;
        return await globalAny.db.query(sql, { type: sequelize.QueryTypes.SELECT, bind: {} });
    }
}