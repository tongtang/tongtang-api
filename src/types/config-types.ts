import { ClusterNode, ClusterOptions } from "ioredis";
export interface RedisConfig {
host?: string; // 单实例时的IP
port?: number; // 单实例时的端口
password?: string;  // 单实例时的密码
isClusterMode?: boolean; // 是否为集群模式
clusterNode?: ClusterNode[];  // 集群节点列表
clusterOptions?: ClusterOptions;  // 集群选项
}